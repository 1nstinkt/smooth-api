// run NODE_ENV=development /usr/bin/node db_update.js  to execute
var MongoClient = require('sails-mongo/node_modules/mongodb').MongoClient;
var config = require('../config/env/'+process.env.NODE_ENV+'.js');
var fs = require('fs');

// Build A Mongo Connection String
var connectionString = 'mongodb://';

// If auth is used, append it to the connection string
if(config.connections.mongo.user && config.connections.mongo.password) {

    // Ensure a database was set if auth in enabled
    if(!config.connections.mongo.database) {
        throw new Error('The MongoDB Adapter requires a database config.connections.mongo option if authentication is used.');
    }

    connectionString += config.connections.mongo.user + ':' + config.connections.mongo.password + '@';
}

// Append the host and port
connectionString += config.connections.mongo.host + ':' + config.connections.mongo.port + '/';

if(config.connections.mongo.database) {
    connectionString += config.connections.mongo.database;
}

// Use config.connections.mongo connection string if available
if(config.connections.mongo.url) connectionString = config.connections.mongo.url;


MongoClient.connect(connectionString, function(err, db) {
    var ObjectID = require('sails-mongo/node_modules/mongodb').ObjectID;

    db.collection('transactions').find().toArray(function(err, docs) {
        if(!docs.length) return console.log('Nothing to update!');
        for(var d = 0; d < docs.length; d++) {
            if(docs[d].gateway_type == 'qiwi') {
                db.collection('transactions').update(
                    { "_id": docs[d]._id},
                    {
                        $unset: { payer_email: "" },
                        $set: {
                            payer_id: docs[d].payer_email,
                            user_id: new ObjectID(docs[d].payer_id)
                        }
                    }
                );
            } else if(docs[d].gateway_type == 'paypal' && docs[d].custom) {
                db.collection('transactions').update(
                    { "_id": docs[d]._id},
                    {
                        $set: {
                            user_id: new ObjectID(JSON.parse(docs[d].custom).user_id)
                        }
                    }
                );
            }
        }
    });
});