// run NODE_ENV=development /usr/bin/node db_update.js  to execute
var MongoClient = require('sails-mongo/node_modules/mongodb').MongoClient;
var config = require('../config/env/'+process.env.NODE_ENV+'.js');
var fs = require('fs');

// Build A Mongo Connection String
var connectionString = 'mongodb://';

// If auth is used, append it to the connection string
if(config.connections.mongo.user && config.connections.mongo.password) {

    // Ensure a database was set if auth in enabled
    if(!config.connections.mongo.database) {
        throw new Error('The MongoDB Adapter requires a database config.connections.mongo option if authentication is used.');
    }

    connectionString += config.connections.mongo.user + ':' + config.connections.mongo.password + '@';
}

// Append the host and port
connectionString += config.connections.mongo.host + ':' + config.connections.mongo.port + '/';

if(config.connections.mongo.database) {
    connectionString += config.connections.mongo.database;
}

// Use config.connections.mongo connection string if available
if(config.connections.mongo.url) connectionString = config.connections.mongo.url;


MongoClient.connect(connectionString, function(err, db) {
    db.collection('posts').find({"original_post": {$exists: true}}).toArray(function(err, docs) {
        if(!docs.length) return console.log('Nothing to update!');
        for(var d = 0; d < docs.length; d++) {
            db.collection('post_reposts__post_original_post').insertOne({
                post_original_post: docs[d]._id,
                post_reposts: docs[d].original_post
            }, function(err, inserted) {
                if(err) return console.log(err);
                if(!err) {
                    db.collection('posts').update(
                        { "_id": inserted.ops[0].post_original_post },
                        {
                            $unset: { original_post: "" }
                        }, function(err, result) {
                            if(err) return console.log(err);
                            console.log('+1 updated');
                        }
                    );
                }
            })
        }
    });
});