module.exports = {
    schema: true,
    attributes : {
        title: {
            type: 'string',
            required: true
        },
        content: {
            type: 'text',
            required: true
        },
        city: {
            type: 'string'
        },
        country: {
            type: 'string'
        },
        date_from: {
            required: true
        },
        date_to: {
            required: true
        },
        images: {
            type: 'array'
        },
        shortDesc: {
            type: 'string'
        },
        owner: {
            model: 'eventmanager',
            required: true
        },
        tags: {
            type: 'string'
        }
    },

    beforeCreate(values, cb) {
        if(values.date_from) {
            values.date_from = new Date(values.date_from)
        }

        if(values.date_to) {
            values.date_to = new Date(values.date_to)
        }

        cb();
    },

    beforeUpdate(values, cb) {
        if(values.date_from) {
            values.date_from = new Date(values.date_from)
        }

        if(values.date_to) {
            values.date_to = new Date(values.date_to)
        }

        cb();
    },

    afterDestroy(destroyedRecords, cb) {
        if(destroyedRecords.length) {
            const OR = {or: [{event: destroyedRecords.map(record => { return record.id })}, {casting: destroyedRecords.map(record => { return record.id })}, {news: destroyedRecords.map(record => { return record.id })}]};
            Promise.all([
                Vote.destroy(OR), // destroying all votes
                Like.destroy(OR), // destroying all likes
                Comment.destroy(OR) // destroying all comments
            ]).then(() => {
                cb();
            }).catch(error => {
                cb(error);
            })
        } else {
            cb();
        }
    }
};