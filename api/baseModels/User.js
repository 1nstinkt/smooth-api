var Q = require('q');
var passwordHash = require('password-hash');

module.exports = {
    schema: true,
    adapter: 'mongo',

    attributes: {
        name: {
            type: 'string',
            required: true
        },
        email: {
            type: 'email',
            required: true,
            unique: true
        },
        password: {
            type: 'string',
            required: true
        },
        role: {
            type: 'string',
            in: sails.config.AVAILABLE_USER_ROLES,
            defaultsTo: 'model'
        },
        status: {
            type: 'string',
            in: sails.config.AVAILABLE_USER_STATUSES,
            defaultsTo: 'active'
        }
    },

    validateBeforeSave: function (params) {

        var deferred = Q.defer();

        // validate email unique in all user collections
        Q.all([
            Admin.findOne(params.id ? {email: params.email, id: {"!": params.id}} : {email: params.email}),
            Model.findOne(params.id ? {email: params.email, id: {"!": params.id}} : {email: params.email}),
            EventManager.findOne(params.id ? {email: params.email, id: {"!": params.id}} : {email: params.email}),
            ContentManager.findOne(params.id ? {email: params.email, id: {"!": params.id}} : {email: params.email}),
            UserManager.findOne(params.id ? {email: params.email, id: {"!": params.id}} : {email: params.email})
        ])
        .spread(function(admin, model, event_manager, content_manager, user_manager){
            if(admin || model || event_manager || content_manager || user_manager) {
                throw new Error('User with such E-mail is already exists');
            }
            deferred.resolve();
        })
        .catch(function(err){
            deferred.reject(err.message);
        });

        return deferred.promise;
    },

    beforeCreate: function(values, cb) {
        this.validateBeforeSave(values).then(() => {
            // set default role of user entity
            values.role = this.role;

            var ObjectID = require(sails.config.mongodb_module_path).ObjectID;
            values['_id'] = new ObjectID(values['_id']);

            if (values.password) {
                if (values.password.substring(0, 4) !== 'sha1') { // preventing to hash hashed pwd
                    values.password = passwordHash.generate(values.password);
                }
            } else {
                values.password = passwordHash.generate(Date.now() + 'password')
            }

            if(values.payment_status) values.payment_status = this.attributes.payment_status.defaultsTo; // prevent create payment status manually
            if(values.expiration) values.expiration = this.attributes.expiration.defaultsTo; // prevent create with expiration of account date manually

            if(values.parent) {
                ActionLog.create({
                    type: ActionLog.attributes.type.in[5],
                    owner: values.parent,
                    details: {new_user_id: values['_id']}
                }).then(()=>{}).catch((e)=>{console.error(e)});
            }

            cb();
        }).catch((error) => {
            return cb({ message: error });
        });
    },

    beforeUpdate: function(values, cb) {
        values.role = this.role;

        // reset security values if request comes from user - not API itself
        if(values.payment_status && values.permitCode !== sails.config.custom_config.permit_code) delete values.payment_status; // prevent create payment status manually
        if(values.expiration && values.permitCode !== sails.config.custom_config.permit_code) delete values.expiration; // prevent create with expiration of account date manually

        if(values.friends) {
            if(values.permitCode !== sails.config.custom_config.permit_code) {
                delete values.friends;
            } else {
                if(!values.friends) {
                    ActionLog.create({
                        type: ActionLog.attributes.type.in[3],
                        owner: values.id,
                        details: {message: 'No details'}
                    }).then(()=>{}).catch((e)=>{console.error(e)});
                    delete values.friends; // deny to remove all friends
                }
            }
        }

        if(values.as_friend) {
            if(values.permitCode !== sails.config.custom_config.permit_code) {
                delete values.as_friend;
            } else {
                if(!values.as_friend) {
                    ActionLog.create({
                        type: ActionLog.attributes.type.in[4],
                        owner: values.id,
                        details: {message: 'No details'}
                    }).then(()=>{}).catch((e)=>{console.error(e)});
                    delete values.as_friend; // deny to remove all invitations
                }
            }
        }

        if(values.password && values.password.substring(0,5) !== 'sha1$') { // preventing to hash hashed pwd
            values.password = passwordHash.generate(values.password);
        }

        if(typeof values.email !== 'undefined') {
            this.validateBeforeSave(values).then(() => {
                return cb();
            }).catch((error) => {
                return cb({ message: error });
            });
        } else {
            cb();
        }
    },

    getSubRole: function(user) {
        if(['admin', 'content_manager', 'user_manager'].indexOf(user.role) !== -1) {
            return user.role;
        }
        if(user.role == 'model') {
            if(user.payment_status && user.payment_status == Model.attributes.payment_status.in[1] && new Date(user.expiration) > new Date()) {
                if(user.is_model == true) return 'model';
                else return 'member_pro';
            } else return 'member';
        } else if(user.role == 'business') {
            if(user.payment_status && user.payment_status == Model.attributes.payment_status.in[1] && new Date(user.expiration) > new Date()) return 'business_pro';
            else return user.role;
        } else if(user.role == 'event_manager') {
            if(user.is_agent) return 'event_manager_agent';
            return 'event_manager_pro';
        }
    }
};