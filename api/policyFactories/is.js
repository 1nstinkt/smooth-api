var passport = require('passport');
module.exports = function(userType){

    return function(req, res, next){
        if(userType == 'guest' && !req.headers.uid && !req.headers.Authorization) {
            return next();
        } else {
            passport.authenticate(userType+'-bearer', {session: false}, function (err, user, info) {
                if (err) return next(err);
                if (user && user.id === req.headers.uid) {
                    req.headers.userrole = user.role;
                    return next();
                }
                return res.forbidden({message: "You are not permitted to perform this action."});
            })(req, res);
        }

    };
};