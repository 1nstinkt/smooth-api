/**
 * Attributes, that can be accessible to selected roles
 *
 * @param {Array} roles
 * @param {Array} attributes
 * @returns {Function}
 */
var passport = require('passport');

module.exports = function(rules){
    return function(req, res, next) {
        var roles = JSON.parse(rules).roles,
            attributes = JSON.parse(rules).permitAttr,
            forbiddenMsg = "You are not permitted to perform this action.";

        var Q = require('q'),
            promises = [];

        for(var i in roles) {
            promises.push((function(i) {
                var deferred = Q.defer();

                var userType = roles[i];
                if(userType == 'guest' && !req.headers.uid && !req.headers.Authorization) { // user without authorization
                    req.headers.userrole = 'guest';
                    req.headers.permitAttr = attributes;
                    deferred.resolve(true);
                } else {
                    try {
                        passport.authenticate(userType + '-bearer', {session: false}, function (err, user, info) {
                            if (err) return deferred.reject(err);
                            if (user && user.id === req.headers.uid) {
                                req.headers.userrole = user.role;
                                req.headers.usertype = userType;
                                req.headers.permitAttr = attributes;
                                return deferred.resolve(true);
                            } else {
                                return deferred.reject(new Error(forbiddenMsg));
                            }
                        })(req, res);
                    } catch(err) {
                        deferred.reject(err)
                    }
                }

                return deferred.promise;
            })(i));
        }

        Q
            .allSettled(promises)
            .then(function(data) {
                var has_access = false;
                data.map(function(check_result) {
                    if(check_result.state == 'fulfilled' && check_result.value) has_access = true;
                });

                if(has_access) return next();
                return res.forbidden({message: forbiddenMsg});
            }).catch(function(error) {
                return res.forbidden(error)
            });

    };
};