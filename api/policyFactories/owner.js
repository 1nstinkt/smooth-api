var passport = require('passport');
module.exports = function(userType){

    return function(req, res, done){
        if(!userType) userType = 'all'; // check in all user collections
        passport.authenticate(userType+'-bearer', {session: false}, function (err, user, info) {
            if (err) return done(err);
            if (user && user.id === req.headers.uid && user.id == req.param('id')) {
                req.headers.userrole = user.role;
                return done();
            }

            return res.forbidden({message: "You are not permitted to perform this action."});
        })(req, res);
    };
};

