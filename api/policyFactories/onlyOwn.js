var passport = require('passport');

module.exports = function (userType) {
    return function (req, res, next) {
        passport.authenticate(userType + '-bearer', {session: false}, function (err, user, info) {
            if (err) return next(err);
            var permit_message = "You are not permitted to perform this action";

            if (user && user.id === req.headers.uid) {
// ================================ CONTROLLER ==========================
                switch (req.options.controller) {
                    case 'news':
                        switch (req.options.action) {
                            case 'create':
                            case 'update':
                                if (user.role == 'event_manager') {
                                    req.body.owner = user.id;
                                    return next();
                                }
                                break;
                            case 'destroy':
                            case 'destroyimg':
                                if (user.role == 'event_manager') {
                                    News.findOne({id: req.param('id')}).populate('owner')
                                        .then(function (found) {
                                            if (!found || found.owner.id !== user.id) throw new Error(permit_message);
                                            return next();
                                        })
                                        .catch(function (error) {
                                            console.error(error);
                                            return res.forbidden({message: error.message});
                                        });
                                }
                                break;
                            case 'customfind':
                            case 'customfindone':
                                if (user.role == 'event_manager') {
                                    req.query.where = req.query.where ? JSON.parse(req.query.where) : {};
                                    req.query.where.owner = user.id;
                                    req.query.where = JSON.stringify(req.query.where);
                                    return next();
                                }
                                break;
                        }
                        break;
                    case 'event':
                        switch (req.options.action) {
                            case 'create':
                            case 'update':
                                if (user.role == 'event_manager') {
                                    req.body.owner = user.id;
                                    return next();
                                }
                                break;
                            case 'destroy':
                            case 'destroyimg':
                                if (user.role == 'event_manager') {
                                    Event.findOne({id: req.param('id')}).populate('owner')
                                        .then(function (found) {
                                            if (!found || found.owner.id !== user.id) throw new Error(permit_message);
                                            req.headers.userrole = user.role;
                                            return next();
                                        })
                                        .catch(function (error) {
                                            console.error(error);
                                            return res.forbidden({message: error.message});
                                        });
                                }
                                break;
                            case 'customfind':
                            case 'customfindone':
                                if (user.role == 'event_manager') {
                                    req.query.where = req.query.where ? JSON.parse(req.query.where) : {};
                                    req.query.where.owner = user.id;
                                    req.query.where = JSON.stringify(req.query.where);
                                    return next();
                                }
                                break;
                        }
                        break;
                    case 'casting':
                        switch (req.options.action) {
                            case 'create':
                            case 'update':
                                if (user.role == 'event_manager') {
                                    req.body.owner = user.id;
                                    return next();
                                }
                                break;
                            case 'destroy':
                            case 'destroyimg':
                                if (user.role == 'event_manager') {
                                    Casting.findOne({id: req.param('id')}).populate('owner')
                                        .then(function (found) {
                                            if (!found || found.owner.id !== user.id) throw new Error(permit_message);
                                            req.headers.userrole = user.role;
                                            return next();
                                        })
                                        .catch(function (error) {
                                            console.error(error);
                                            return res.forbidden({message: error.message});
                                        });
                                }
                                break;
                            case 'customfind':
                            case 'customfindone':
                                if (user.role == 'event_manager') {
                                    req.query.where = req.query.where ? JSON.parse(req.query.where) : {};
                                    req.query.where.owner = user.id;
                                    req.query.where = JSON.stringify(req.query.where);
                                    return next();
                                }
                                break;
                        }
                        break;
                    case 'eventinvitation':
                        switch (req.options.action) {
                            case 'findOne':
                                if (user.role == 'event_manager') {
                                    EventInvitation.findOne({id: req.param('id')}).populate('event')
                                        .then(function (found) {
                                            if (!found || found.event.owner !== user.id) throw new Error(permit_message);
                                            return next();
                                        })
                                        .catch(function (error) {
                                            console.error(error);
                                            return res.forbidden({message: error.message});
                                        });
                                } else if (user.role == 'model') {
                                    EventInvitation.findOne({id: req.param('id')}).populate('model')
                                        .then(function (found) {
                                            if (!found || found.model.id !== user.id) throw new Error(permit_message);
                                            return next();
                                        })
                                        .catch(function (error) {
                                            console.error(error);
                                            return res.forbidden({message: error.message});
                                        });
                                }
                                break;
                            case 'customfind':
                                if (user.role == 'event_manager') {
                                    Event.find({owner: user.id}).populate('invitations')
                                        .then(function (found) {
                                            req.query.where = !req.query.where ? {} : JSON.parse(req.query.where);
                                            for (var e in found) {
                                                for (var i in found[e].invitations) {
                                                    if (!req.query.where.id) req.query.where.id = [];
                                                    if (found[e].invitations[i].id) req.query.where.id.push(found[e].invitations[i].id);
                                                }
                                            }
                                            req.query.where = JSON.stringify(req.query.where);
                                            return next();
                                        })
                                        .catch(function (error) {
                                            console.error(error);
                                            return res.forbidden({message: error.message});
                                        });
                                } else if (user.role == 'model') {
                                    if (!req.query.where) req.query.where = {};
                                    req.query.where.model = user.id;
                                    return next();
                                }
                                break;
                            case 'create':
                                if (user.role == 'model') { // when model try to register in event
                                    req.body.model = user.id;
                                    req.body.status = EventInvitation.attributes.status.in[2]; // status of invitation as accepted
                                    return next();
                                } else if (user.role == 'event_manager') {
                                    req.body.status = EventInvitation.attributes.status.in[0]; // invited
                                    req.body.send = false;
                                    Event.findOne({id: req.param('event')}).populate('owner')
                                        .then(function (found) {
                                            if (!found || found.owner.id !== user.id) throw new Error(permit_message);
                                            return next();
                                        })
                                        .catch(function (error) {
                                            console.error(error);
                                            return res.forbidden({message: error.message});
                                        });
                                }
                                break;
                            case 'update':
                            case 'destroy':
                                if (user.role == 'event_manager') {
                                    EventInvitation.findOne({id: req.param('id')}).populate('event')
                                        .then(function (found) {
                                            if (!found || found.event.owner !== user.id) throw new Error(permit_message);
                                            return next();
                                        })
                                        .catch(function (error) {
                                            console.error(error);
                                            return res.forbidden({message: error.message});
                                        });
                                }
                                break;
                        }
                        break;
                    case 'castinginvitation':
                        switch (req.options.action) {
                            case 'findOne':
                                if (user.role == 'event_manager') {
                                    CastingInvitation.findOne({id: req.param('id')}).populate('casting')
                                        .then(function (found) {
                                            if (!found || found.casting.owner !== user.id) throw new Error(permit_message);
                                            return next();
                                        })
                                        .catch(function (error) {
                                            console.error(error);
                                            return res.forbidden({message: error.message});
                                        });
                                } else if (user.role == 'model') {
                                    CastingInvitation.findOne({id: req.param('id')}).populate('model')
                                        .then(function (found) {
                                            if (!found || found.model.id !== user.id) throw new Error(permit_message);
                                            return next();
                                        })
                                        .catch(function (error) {
                                            console.error(error);
                                            return res.forbidden({message: error.message});
                                        });
                                }
                                break;
                            case 'create':
                                if (user.role == 'model') { // when model try to register in casting
                                    req.body.model = user.id;
                                    req.body.status = CastingInvitation.attributes.status.in[2]; // status of invitation as accepted
                                    return next();
                                } else if (user.role == 'event_manager') {
                                    req.body.status = CastingInvitation.attributes.status.in[0]; // invited
                                    req.body.send = false;
                                    Casting.findOne({id: req.param('casting')}).populate('owner')
                                        .then(function (found) {
                                            if (!found || found.owner.id !== user.id) throw new Error(permit_message);
                                            return next();
                                        })
                                        .catch(function (error) {
                                            console.error(error);
                                            return res.forbidden({message: error.message});
                                        });
                                }
                                break;
                            case 'update':
                            case 'destroy':
                                if (user.role == 'event_manager') {
                                    CastingInvitation.findOne({id: req.param('id')}).populate('casting')
                                        .then(function (found) {
                                            if (!found || found.casting.owner !== user.id) throw new Error(permit_message);
                                            return next();
                                        })
                                        .catch(function (error) {
                                            console.error(error);
                                            return res.forbidden({message: error.message});
                                        });
                                }
                                break;
                            case 'customfind':
                                if (user.role == 'event_manager') {
                                    Casting.find({owner: user.id}).populate('invitations')
                                        .then(function (found) {
                                            req.query.where = !req.query.where ? {} : JSON.parse(req.query.where);
                                            for (var e in found) {
                                                for (var i in found[e].invitations) {
                                                    if (!req.query.where.id) req.query.where.id = [];
                                                    if (found[e].invitations[i].id) req.query.where.id.push(found[e].invitations[i].id);
                                                }
                                            }
                                            req.query.where = JSON.stringify(req.query.where);
                                            return next();
                                        })
                                        .catch(function (error) {
                                            console.error(error);
                                            return res.forbidden({message: error.message});
                                        });
                                } else if (user.role == 'model') {
                                    if (!req.query.where) req.query.where = {};
                                    req.query.where.model = user.id;
                                    return next();
                                }
                                break;
                        }
                        break;
                    case 'album':
                        switch (req.options.action) {
                            case 'create':
                                if (user.role == 'model') {
                                    req.body.owner = user.id;
                                    return next();
                                } else if (user.role == 'event_manager') {
                                    Casting.find({owner: user.id}).populate('invitations').then(function (found) {
                                        if (found.length) {
                                            for (var i in found) {
                                                if (found[i].invitations && found[i].invitations.length) {
                                                    for (var a in found[i].invitations) {
                                                        if (found[i].invitations[a].model == req.body.owner && found[i].invitations[a].status == CastingInvitation.attributes.status.in[4]) {
                                                            return next();
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        return res.forbidden({message: permit_message});
                                    });
                                } else {
                                    return res.forbidden({message: permit_message});
                                }
                                break;
                            case 'update':
                            case 'destroy':
                                if (user.role == 'model') {
                                    Album.findOne({id: req.param('id')})
                                        .then(function (found) {
                                            if (!found || found.owner !== user.id) throw new Error(permit_message);
                                            return next();
                                        })
                                        .catch(function (error) {
                                            return res.forbidden({message: error.message});
                                        });
                                } else if (user.role == 'event_manager') {
                                    var Promise = require('q');

                                    Promise.all([
                                        Casting.find({owner: user.id}).populate('invitations'),
                                        Album.findOne({id: req.param('id')})
                                    ])
                                        .spread(function(castings, album){
                                            if(!castings.length || !album) return res.forbidden({message: permit_message});
                                            for (var i in castings) {
                                                if (castings[i].invitations && castings[i].invitations.length) {
                                                    for (var a in castings[i].invitations) {
                                                        if (castings[i].invitations[a].model == album.owner && castings[i].invitations[a].status == CastingInvitation.attributes.status.in[4]) {
                                                            return next();
                                                        }
                                                    }
                                                }
                                            }
                                            return res.forbidden({message: permit_message});
                                        });
                                } else {
                                    return res.forbidden({message: permit_message});
                                }
                                break;
                        }
                        break;
                    case 'image':
                        switch (req.options.action) {
                            case 'create':
                                if (user.role == 'model') {
                                    if (req.param('album')) {
                                        // verify album permissions
                                        Album.findOne({id: req.param('album')})
                                            .then(function (found) {
                                                if (!found || found.owner !== user.id) throw new Error(permit_message);
                                                req.body.user_id = user.id;
                                                return next();
                                            })
                                            .catch(function (error) {
                                                return res.forbidden({message: error.message});
                                            });
                                    } else {
                                        // add image to default album
                                        req.body.user_id = user.id;
                                        return next();
                                    }
                                }
                                break;
                            case 'destroy':
                                if (user.role == 'model') {
                                    Album.findOne({images: req.param('name')})
                                        .then(function (found) {
                                            if (!found || found.owner !== user.id) throw new Error(permit_message);
                                            return next();
                                        })
                                        .catch(function (error) {
                                            return res.forbidden({message: error.message});
                                        });
                                }
                                break;
                            case 'clearall':
                                if (user.role == 'model') {
                                    Album.findOne({id: req.param('id')})
                                        .then(function (found) {
                                            if (!found || found.owner !== user.id) throw new Error(permit_message);
                                            return next();
                                        })
                                        .catch(function (error) {
                                            return res.forbidden({message: error.message});
                                        });
                                }
                                break;
                            case 'update':
                                if (user.role == 'model') {
                                    var found = undefined;
                                    Album.findOne({images: req.param('name')})
                                        .then(function (result) {
                                            found = result;
                                            return require('photo_cropper').prepare(req, res);
                                        }).then(function () {
                                            if (!found || found.owner !== user.id) throw new Error(permit_message);
                                            req.body.user_id = user.id;
                                            return next();
                                        })
                                        .catch(function (error) {
                                            return res.forbidden({message: error.message});
                                        });
                                }
                                break;
                        }
                        break;
                    case 'post':
                        switch (req.options.action) {
                            case 'create':
                                if (user.role == 'model') {
                                    // TODO: add friend verification
                                    req.body.sender_id = user.id;
                                    next();
                                }
                                break;
                            case 'update':
                                if (user.role == 'model') {
                                    Post.findOne({ id: req.param('id') })
                                        .then(function (found) {
                                            if (!found || found.sender_id !== user.id) throw new Error(permit_message);
                                            return next();
                                        })
                                        .catch(function (error) {
                                            return res.forbidden({message: error.message});
                                        });
                                }
                                break;
                            case 'destroy':
                                if (user.role == 'model') {
                                    Post.findOne({ id: req.param('id') })
                                        .then(function (found) {
                                            if (found && (found.sender_id == user.id || found.user_id == user.id)) return next();
                                            throw new Error(permit_message);
                                        })
                                        .catch(function (error) {
                                            return res.forbidden({message: error.message});
                                        });
                                }
                                break;
                        }
                        break;
                    case 'chat':
                        switch (req.options.action) {
                            case 'getchatusers':
                            case 'getunreadmsgs':
                                if (user.role == 'model') {
                                    req.query.where = req.query.where ? JSON.parse(req.query.where) : {};
                                    req.query.where.to = user.id;
                                    req.query.where = JSON.stringify(req.query.where);
                                }
                                return next();
                                break;
                        }
                        break;
                    case 'account':
                        switch (req.options.action) {
                            case 'qw_activate':
                                if (user.role == 'model') {
                                    req.body.user_id = user.id;
                                }
                                return next();
                                break;
                        }
                        break;
                    case 'statistic':
                        switch (req.options.action) {
                            case 'summary':
                                if (user.role == 'event_manager') {
                                    req.query.where = req.query.where ? JSON.parse(req.query.where) : {};
                                    req.query.where.owner = user.id;
                                    req.query.where = JSON.stringify(req.query.where);
                                }
                                return next();
                                break;
                        }
                        break;
                    case 'comment':
                        switch (req.options.action) {
                            case 'create':
                            case 'update':
                            case 'destroy':
                                if (user.role == 'model') {
                                    req.body.user = user.id;
                                    return next();
                                }
                                return next();
                                break;
                        }
                        break;
                    case 'worksheet':
                        switch (req.options.action) {
                            case 'createorupdate':
                            case 'destroy':
                                if (user.role == 'model') {
                                    req.body.user = user.id;
                                    return next();
                                }
                                return next();
                                break;
                        }
                        break;
                    default:
                        return res.forbidden({message: permit_message});
                        break;
                }
            } else {
                return res.forbidden({message: permit_message});
            }
        })(req, res);
    };
};