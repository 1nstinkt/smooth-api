var passwordHash = require('password-hash'),
    passport = require('passport'),
    BearerStrategy = require('passport-http-bearer').Strategy,
    LocalStrategy = require('passport-local').Strategy,
    Promise = require('q');

// for admin authorization
passport.use('admin-bearer', new BearerStrategy(
    function (accessToken, done) {
        jwtToken.verifyToken(accessToken, function (err, user) {
            if (err) return done(null, false);
            Promise.all([
                Admin.findOne({id: user.id})
            ])
            .spread(function(admin){
                var info = {scope: '*'};
                if(admin) {
                    return done(null, admin, info);
                }

                return done(null, false);
            })
            .catch(function(error){
                done(error);
            });
        });
    })
);
// for all event_manager authorization
passport.use('event_manager-bearer', new BearerStrategy(
        function (accessToken, done) {
            jwtToken.verifyToken(accessToken, function (err, user) {
                if (err) return done(null, false);
                Promise.all([
                    EventManager.findOne({id: user.id })
                ])
                    .spread(function(event_manager){
                        var info = {scope: '*'};
                        if(event_manager) {
                            return done(null, event_manager, info);
                        }

                        return done(null, false);
                    })
                    .catch(function(error){
                        done(error);
                    });
            });
        })
);

passport.use('event_manager_pro-bearer', new BearerStrategy(
    function (accessToken, done) {
        jwtToken.verifyToken(accessToken, function (err, user) {
            if (err) return done(null, false);
            Promise.all([
                EventManager.findOne({id: user.id, is_agent: false })
            ])
                .spread(function(event_manager){
                    var info = {scope: '*'};
                    if(event_manager) {
                        return done(null, event_manager, info);
                    }

                    return done(null, false);
                })
                .catch(function(error){
                    done(error);
                });
        });
    })
);
// for model user authorization
passport.use('model-bearer', new BearerStrategy(
        function (accessToken, done) {
            jwtToken.verifyToken(accessToken, function (err, user) {
                if (err) return done(null, false);
                Promise.all([
                    Model.findOne({id: user.id, payment_status: Model.attributes.payment_status.in[1], expiration: {'>': new Date()}, is_model: true})
                ])
                    .spread(function(model){
                        var info = {scope: '*'};
                        if(model) {
                            return done(null, model, info);
                        }

                        return done(null, false);
                    })
                    .catch(function(error){
                        done(error);
                    });
            });
        })
);
// for model user with any payment status
passport.use('model_any-bearer', new BearerStrategy(
        function (accessToken, done) {
            jwtToken.verifyToken(accessToken, function (err, user) {
                if (err) return done(null, false);
                Promise.all([
                    Model.findOne({id: user.id})
                ])
                    .spread(function(model){
                        var info = {scope: '*'};
                        if(model) {
                            return done(null, model, info);
                        }

                        return done(null, false);
                    })
                    .catch(function(error){
                        done(error);
                    });
            });
        })
);
// for member user authorization
passport.use('member-bearer', new BearerStrategy(
    function (accessToken, done) {
        jwtToken.verifyToken(accessToken, function (err, user) {
            if (err) return done(null, false);
            Promise.all([
                Model.findOne({id: user.id, "or": [
                    {payment_status: Model.attributes.payment_status.in[0]},
                    {expiration: null},
                    {payment_status: Model.attributes.payment_status.in[1], expiration: {'<': new Date()}}
                ]}) // any model user which has expired account
            ])
                .spread(function(member){
                    var info = {scope: '*'};
                    if(member) {
                        return done(null, member, info);
                    }

                    return done(null, false);
                })
                .catch(function(error){
                    done(error);
                });
        });
    })
);
// for activated member user authorization
passport.use('member_pro-bearer', new BearerStrategy(
    function (accessToken, done) {
        jwtToken.verifyToken(accessToken, function (err, user) {
            if (err) return done(null, false);
            Promise.all([
                Model.findOne({id: user.id, payment_status: Model.attributes.payment_status.in[1], expiration: {'>': new Date()}, is_model: false})
            ])
                .spread(function(member){
                    var info = {scope: '*'};
                    if(member) {
                        return done(null, member, info);
                    }

                    return done(null, false);
                })
                .catch(function(error){
                    done(error);
                });
        });
    })
);
// for news maker user authorization
passport.use('content_manager-bearer', new BearerStrategy(
    function (accessToken, done) {
        jwtToken.verifyToken(accessToken, function (err, user) {
            if (err) return done(null, false);
            Promise.all([
                ContentManager.findOne({id: user.id})
            ])
                .spread(function(content_manager){
                    var info = {scope: '*'};
                    if(content_manager) {
                        return done(null, content_manager, info);
                    }

                    return done(null, false);
                })
                .catch(function(error){
                    done(error);
                });
        });
    })
);
// for user manager authorization
passport.use('user_manager-bearer', new BearerStrategy(
    function (accessToken, done) {
        jwtToken.verifyToken(accessToken, function (err, user) {
            if (err) return done(null, false);
            Promise.all([
                UserManager.findOne({id: user.id})
            ])
                .spread(function(user_manager){
                    var info = {scope: '*'};
                    if(user_manager) {
                        return done(null, user_manager, info);
                    }

                    return done(null, false);
                })
                .catch(function(error){
                    done(error);
                });
        });
    })
);
// for user authentication
passport.use(new LocalStrategy(
    {
        usernameField: 'email',
        passwordField: 'password'
    },
    function (username, password, done) {
        Promise.all([
            Admin.findOne({email: username}),
            Model.findOne({email: username}),
            EventManager.findOne({email: username}),
            ContentManager.findOne({email: username}),
            UserManager.findOne({email: username})
        ])
        .spread(function(admin, model, event_manager, content_manager, user_manager){
            if (admin !== undefined && admin.password !== undefined && admin.status == 'active' && passwordHash.verify(password, admin.password)) {
                return done(null, admin);
            } else if (model !== undefined && model.password !== undefined && model.status == 'active' && passwordHash.verify(password, model.password)) {
                return done(null, model);
            } else if (event_manager !== undefined && event_manager.password !== undefined && event_manager.status == 'active' && passwordHash.verify(password, event_manager.password)) {
                return done(null, event_manager);
            } else if (content_manager !== undefined && content_manager.password !== undefined && content_manager.status == 'active' && passwordHash.verify(password, content_manager.password)) {
                return done(null, content_manager);
            } else if (user_manager !== undefined && user_manager.password !== undefined && user_manager.status == 'active' && passwordHash.verify(password, user_manager.password)) {
                return done(null, user_manager);
            }

            return done(null, false);
        })
        .catch(function(error){
            done(error);
        });
    })
);
// for any user authorization
passport.use('any_user-bearer', new BearerStrategy(
    function (accessToken, done) {
        jwtToken.verifyToken(accessToken, function (err, user) {
            if (err) return done(null, false);
            Promise.all([
                Admin.findOne({id: user.id}),
                Model.findOne({id: user.id}),
                EventManager.findOne({id: user.id}),
                ContentManager.findOne({id: user.id}),
                UserManager.findOne({id: user.id})
            ])
                .spread(function(admin, model, event_manager, content_manager, user_manager){
                    var info = {scope: '*'};
                    if(admin) {
                        return done(null, admin, info);
                    } else if(model) {
                        return done(null, model, info);
                    } else if(event_manager) {
                        return done(null, event_manager, info);
                    } else if(content_manager) {
                        return done(null, content_manager, info);
                    } else if(user_manager) {
                        return done(null, user_manager, info);
                    }

                    return done(null, false);
                })
                .catch(function(error){
                    done(error);
                });
        });
    })
);
