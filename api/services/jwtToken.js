var jwt = require('jsonwebtoken');

// Generates a token from supplied payload
module.exports.issueToken = function (payload) {
    var token = jwt.sign(payload, sails.config.session.jwtKey);
    return token;
};

// Verifies token on a request
module.exports.verifyToken = function (token, verified) {
    return jwt.verify(token, sails.config.session.jwtKey, {}, verified);
};