module.exports = {
    /**
     * Custom search method
     *
     * @param req
     * @param Model - entity for searching
     */
    find: function(req, Model) {
        var populateWhere = (req.param('populateWhere')) ? JSON.parse(req.param('populateWhere')) : [],
            toPopulate = (req.param('populate')) ? req.param('populate').replace(/\[|\]/g, '').split(',') : [],
            permitToSelect = [],
            permitToPopulate = {},
            actionUtil = require('sails/lib/hooks/blueprints/actionUtil'),
            Q = require('q');

        var deferred = Q.defer();

        for(var s in req.headers.permitAttr) {
            var splitted = req.headers.permitAttr[s].split('.');
            if(permitToSelect.indexOf(splitted[0]) === -1) permitToSelect.push(splitted[0]); // initialize fields that are permitted to be accessed
            if(splitted[1]) {
                if(!permitToPopulate[splitted[0]]) permitToPopulate[splitted[0]] = [];
                permitToPopulate[splitted[0]].push(splitted[1]); // initialize fields that are permitted to be populated
            }
        }

        // deny to add populateWhere to search criteria
        req.options.criteria = {
            blacklist: ['limit', 'skip', 'sort', 'populate', 'populateWhere', 'populateNested', 'selectFields']
        };

        var where = actionUtil.parseCriteria(req);

        /*
        * Search for ISODate search criterias and transform them to object Date
        * */
        for(var criteria in where) {
            for(var h in where[criteria]) {
                if(typeof where[criteria][h] == 'string') {
                    if(/^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}\.\d{3})Z$/.test(where[criteria][h])) {
                        where[criteria][h] = new Date(where[criteria][h]);
                    }
                } else if(where[criteria][h] instanceof Object) {
                    for(var f in where[criteria][h]) {
                        if(where[criteria][h][f] instanceof Object) {
                            for(var t in where[criteria][h][f]) {
                                if(/^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}\.\d{3})Z$/.test(where[criteria][h][f][t])) {
                                    where[criteria][h][f][t] = new Date(where[criteria][h][f][t]);
                                }
                            }
                        }
                    }
                }
            }
        }

        // build query
        var query = Model.find(req.param('selectFields') ? { select: JSON.parse(req.param('selectFields'))} : null)
            .where(where)
            .limit(actionUtil.parseLimit(req))
            .skip(actionUtil.parseSkip(req))
            .sort(actionUtil.parseSort(req));

        // custom populate
        for(var p in toPopulate) {
            // check if it is permit to get field and as result - to populate field
            if(permitToSelect && permitToSelect.length && permitToSelect.indexOf(toPopulate[p]) === -1) {
                continue;
            }
            populateWhere[toPopulate[p]] ? query.populate(toPopulate[p], populateWhere[toPopulate[p]]) : query.populate(toPopulate[p]);
        }

        query.exec(function (error, response) {
            if (error) return deferred.reject(error);

            if(req.param('populateNested')) { // only when need to populate nested fields
                response = JSON.parse(JSON.stringify(response)); // convert result from module class instance to flat object
            }

            // restricted area
            if(req.headers.permitAttr) {
                // remove fields that are forbidden to view
                for(var i = 0; i < response.length; i++) {
                    for(var f in response[i]) {
                        if(permitToSelect.indexOf(f) === -1) {
                            // remove fields that are not permitted to be selected
                            delete response[i][f];
                        } else if(permitToPopulate[f] && typeof response[i][f] === 'object') { // remove fields that are not permitted to be populated
                            if(response[i][f] instanceof Array) {
                                // array of objects
                                for(var i2 in response[i][f]) {
                                    for(var f2 in response[i][f][i2]) {
                                        if(permitToPopulate[f].indexOf(f2) === -1) delete response[i][f][i2][f2];
                                    }
                                }
                            } else {
                                // object
                                for(var f2 in response[i][f]) {
                                    if(permitToPopulate[f].indexOf(f2) === -1) delete response[i][f][f2];
                                }

                            }
                        }

                    }
                }
            }

            deferred.resolve(response);
        });

        return deferred.promise;
    },

    /**
     * Populate fields that are nested in populated fields of the same model,
     * e.g. Model->populate Likes (made by other Models) -> populate User_id in like (which user was made like)
     *
     * @param req
     * @param response
     * @param Model
     * @returns {Array}
     * @private
     */
    populateNestedFields: function(req, response, Model) {
        var Q = require('q'),
            populateNestedPromises = [],
            toPopulateNested = (req.param('populateNested')) ? JSON.parse(req.param('populateNested')) : {};

        for(var f in toPopulateNested) {
            for(var r in response) {
                if(typeof response[r][f] !== 'undefined') { // search for field in result that should be populated
                    if(response[r][f] instanceof Array) {
                        // array of objects
                        for(var i = 0; i < response[r][f].length; i++) {
                            for(var f2 in toPopulateNested[f]) {
                                populateNestedPromises.push((function(response, r, f, i, f2) {
                                    var deferred2 = Q.defer();

                                    // try to calculate related model - if it absent - do not populate it
                                    try {
                                        var relatedModel = Model.associations.filter((association) => {
                                            if(association.alias == f) return true;
                                            return false;
                                        })[0];
                                        relatedModel = relatedModel.type == 'collection' ? relatedModel.collection : relatedModel.model;
                                    } catch(e) {
                                        return deferred2.resolve(true);
                                    }

                                    eval(relatedModel.charAt(0).toUpperCase() + relatedModel.slice(1)).findOne({ id: response[r][f][i]['id'] }).populate(f2, {select: toPopulateNested[f][f2] ? toPopulateNested[f][f2] : []}).then(function(result) {
                                        response[r][f][i][f2] = [];
                                        // get only required fields for populated nested field
                                        if(result) {
                                            response[r][f][i][f2] = result[f2];
                                        }

                                        deferred2.resolve(true);
                                    });
                                    return deferred2.promise;
                                })(response, r, f, i, f2));
                            }
                        }
                    } else {
                        // object
                        for(var f2 in toPopulateNested[f]) {
                            populateNestedPromises.push((function(response, r, f, f2) {
                                var deferred2 = Q.defer();

                                // try to calculate related model - if it absent - do not populate it
                                try {
                                    var relatedModel = Model.associations.filter((association) => {
                                        if(association.alias == f) return true;
                                        return false;
                                    })[0];
                                    relatedModel = relatedModel.type == 'collection' ? relatedModel.collection : relatedModel.model;
                                } catch(e) {
                                    return deferred2.resolve(true);
                                }

                                eval(relatedModel.charAt(0).toUpperCase() + relatedModel.slice(1)).findOne({ id: response[r][f]['id'] }).populate(f2, {select: toPopulateNested[f][f2] ? toPopulateNested[f][f2] : []}).then(function(result) {
                                    response[r][f][f2] = [];
                                    // get only required fields for populated nested field
                                    if(result) {
                                        response[r][f][f2] = result[f2];
                                    }

                                    deferred2.resolve(true);
                                });
                                return deferred2.promise;
                            })(response, r, f, f2));
                        }

                    }
                }
            }
        }

        return populateNestedPromises;
    },

    /**
     * Remove secure fields before responding to frontend
     *
     * @param data
     * @returns {*}
     * @private
     */
    _clearSecureFields: function(data) {
        for(var a = 0; a < sails.config.SECURE_FIELDS.length; a++) {
            if(typeof data[sails.config.SECURE_FIELDS[a]] !== 'undefined') delete data[sails.config.SECURE_FIELDS[a]];
        }

        return data;
    },

    findOne: function(req, Model) {
        var actionUtil = require('sails/lib/hooks/blueprints/actionUtil'),
            pk = actionUtil.requirePk(req),
            Q = require('q'),
            toPopulate = (req.param('populate')) ? req.param('populate').replace(/\[|\]/g, '').split(',') : [],
            populateWhere = (req.param('populateWhere')) ? JSON.parse(req.param('populateWhere')) : [],
            where = actionUtil.parseCriteria(req),
            THIS = this;

        var deferred = Q.defer(),
            query = Model.findOne(pk);

        // custom populate
        for(var p in toPopulate) {
            populateWhere[toPopulate[p]] ? query.populate(toPopulate[p], populateWhere[toPopulate[p]]) : query.populate(toPopulate[p]);
        }

        query = actionUtil.populateRequest(query, req);
        query
            .where(where)
            .exec(function found(err, matchingRecord) {
            if (err) return deferred.reject(err);
            if(!matchingRecord) return deferred.reject('No record found with the specified `id`.');

            if (req._sails.hooks.pubsub && req.isSocket) {
                Model.subscribe(req, matchingRecord);
                actionUtil.subscribeDeep(req, matchingRecord);
            }

            matchingRecord = THIS._clearSecureFields(matchingRecord.toJSON());
            deferred.resolve(matchingRecord);
        });

        return deferred.promise;
    }
};
