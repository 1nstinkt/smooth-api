/**
 * CityController
 *
 * @description :: Server-side logic for managing Cities
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    customFind: function (req, res) {
        return customFind.find(req, City).then(function(result) {
            res.ok(result);
        }).catch(function(error) {
            res.serverError(error);
        });
    }
};

