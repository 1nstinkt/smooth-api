/**
 * ContentManagerController
 *
 * @description :: Server-side logic for managing ContentManagers
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    create: function(req, res) {
        ContentManager.create(req.allParams()).then(function(user) {
            req.file('avatar').upload(sails.config.custom_config.POST_LIMIT_MB ? {maxBytes: sails.config.custom_config.POST_LIMIT_MB*1000000} : {}, function (err, uploadedFiles) {
                if (err) return res.serverError(err.message);
                if(uploadedFiles.length > 0) {
                    var request = require("request");
                    var path = require('path');
                    var fs = require('fs');

                    var r = request.post({
                        url: sails.config.custom_config.CDN_URL + "/avatar/" + user.id,
                        timeout: 60 * 1000
                    }, function (err, response, body) {
                        if (fs.existsSync(sails.config.custom_config.tmp_file_storage + path.basename(uploadedFiles[0].fd))) {
                            fs.unlinkSync(sails.config.custom_config.tmp_file_storage + path.basename(uploadedFiles[0].fd));
                        }
                        if (err) return res.badRequest(err.message);

                        if(response.statusCode == 200) {
                            // update image to ContentManager user
                            ContentManager.update({id: user.id}, {avatar: JSON.parse(body).filepath}).exec(function(err, updated_user) {
                                if(err) res.badRequest(err.message);
                                res.ok(updated_user[0]);
                            });
                        } else {
                            res.badRequest(JSON.parse(body).errors[0]);
                        }
                    });

                    var form = r.form();
                    form.append(uploadedFiles[0].filename, fs.createReadStream(sails.config.custom_config.tmp_file_storage + path.basename(uploadedFiles[0].fd)), {filename: path.basename(uploadedFiles[0].fd)});
                } else {
                    res.ok(user);
                }
            });
        }).catch(function(err) {
            res.badRequest(err);
        });
    },

    update: function(req, res) {
        var params = req.allParams();

        ContentManager.update({id: req.param('id')}, params).then(function(user) {
            if(!user.length) throw 'No such Content Manager user.';
            user = user[0];
            req.file('avatar').upload(sails.config.custom_config.POST_LIMIT_MB ? {maxBytes: sails.config.custom_config.POST_LIMIT_MB*1000000} : {}, function (err, uploadedFiles) {
                if (err) return res.serverError(err.message);
                if(uploadedFiles.length > 0) {
                    var request = require("request");
                    var path = require('path');
                    var fs = require('fs');

                    var r = request.post({
                        url: sails.config.custom_config.CDN_URL + "/avatar/" + user.id,
                        timeout: 60 * 1000
                    }, function (err, response, body) {
                        if (fs.existsSync(sails.config.custom_config.tmp_file_storage + path.basename(uploadedFiles[0].fd))) {
                            fs.unlinkSync(sails.config.custom_config.tmp_file_storage + path.basename(uploadedFiles[0].fd));
                        }
                        if (err) return res.badRequest(err.message);

                        if(response.statusCode == 200) {
                            // update image to ContentManager user
                            ContentManager.update({id: user.id}, {avatar: JSON.parse(body).filepath}).exec(function(err, updated_user) {
                                if(err) return res.badRequest(err.message);
                                ContentManager.findOne({id: updated_user[0].id}).then(function(user) {
                                    res.ok(user);
                                });
                            });
                        } else {
                            res.badRequest(JSON.parse(body).errors[0]);
                        }
                    });

                    var form = r.form();
                    form.append(uploadedFiles[0].filename, fs.createReadStream(sails.config.custom_config.tmp_file_storage + path.basename(uploadedFiles[0].fd)), {filename: path.basename(uploadedFiles[0].fd)});
                } else {
                    ContentManager.findOne({id: user.id}).then(function(user) {
                        res.ok(user);
                    });
                }
            });
        }).catch(function(err) {
            res.badRequest(err);
        });
    }
};

