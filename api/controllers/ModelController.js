/**
 * ModelController
 *
 * @description :: Server-side logic for managing Models
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    /**
     * Find profiles of users with subrole as model
     *
     * @param req
     * @param res
     */
    customFind: function (req, res) {
        var _ = require('lodash'),
            Q = require('q'),
            User = require('../baseModels/User.js');

        var originalPermitAttr = _.cloneDeep(req.headers.permitAttr);

        req.query.where = this._buildWhere(req, res);

        // if user is member - permit to view Portfolio album
        if (req.headers.usertype == 'member' && req.param('populate') && req.param('populate').split(',').indexOf('albums') !== -1) {
            var tmp = req.param('populateWhere') ? JSON.parse(req.param('populateWhere')) : {};
            !tmp['albums'] ? tmp['albums'] = {} : null;
            if (tmp['albums']['name']) {
                if (!tmp['albums']['name']["!"]) {
                    tmp['albums']['name'] = {"!": [sails.config.custom_config.PORTFOLIO_ALBUM_NAME]};
                } else if (typeof tmp['albums']['name']["!"] === 'string') {
                    tmp['albums']['name']["!"] = [tmp['albums']['name']["!"], sails.config.custom_config.PORTFOLIO_ALBUM_NAME]
                } else {
                    tmp['albums']['name']["!"].push(sails.config.custom_config.PORTFOLIO_ALBUM_NAME);
                }
            } else {
                tmp['albums']['name'] = {"!": [sails.config.custom_config.PORTFOLIO_ALBUM_NAME]}
            }
            req.query.populateWhere = JSON.stringify(tmp);
        }

        // Start. Processing subrole
        if (req.headers.permitAttr) {
            for (var f in sails.config.SUB_ROLE_REQUIRED_FIELDS) {
                if (req.headers.permitAttr.indexOf(sails.config.SUB_ROLE_REQUIRED_FIELDS[f]) === -1)
                    req.headers.permitAttr.push(sails.config.SUB_ROLE_REQUIRED_FIELDS[f]);
            }
        }

        return customFind.find(req, Model).then(function (result) {
            // Continue. Processing subrole
            for (var i = 0; i < result.length; i++) {
                result[i].sub_role = User.getSubRole(result[i]);
                if (typeof originalPermitAttr !== 'undefined') {
                    // remove denied fields from response
                    for (var f in sails.config.SUB_ROLE_REQUIRED_FIELDS) {
                        if (originalPermitAttr.indexOf(sails.config.SUB_ROLE_REQUIRED_FIELDS[f]) === -1
                            && result[i][sails.config.SUB_ROLE_REQUIRED_FIELDS[f]]
                        )
                            delete result[i][sails.config.SUB_ROLE_REQUIRED_FIELDS[f]];
                    }
                }
            }
            // End. Processing subrole

            var populateNestedPromises = customFind.populateNestedFields(req, result, Model);

            Q.allSettled(populateNestedPromises).then(function (data) {
                res.ok(result);
            });
        }).catch(function (error) {
            res.serverError(error);
        });
    },

    /**
     * Redirect to WEB part with profile page
     *
     * @param req
     * @param res
     */
    getProfileURL: function (req, res) {
        Model.findOne({ id: req.param('id') }, { select:  JSON.stringify(sails.config.SUB_ROLE_REQUIRED_FIELDS)}).then(function(found) {
            if(!found) return res.notFound();
            var subRole = require('../baseModels/User.js').getSubRole(found);

            res.writeHead(302, {
                'Location': sails.config.custom_config.site_url + '/' + (subRole == 'model' ? 'models/' : 'members/') + req.param('id')
            });
            return res.end();
        });
    },

    /**
     * Searching for user with role model and any sub role
     *
     * @param req
     * @param res
     */
    anyUserCustomFind: function (req, res) {
        var _ = require('lodash'),
            User = require('../baseModels/User.js');

        var originalPermitAttr = _.cloneDeep(req.headers.permitAttr);

        req.query.where = JSON.stringify(_.merge({},
            req.param('where') ? JSON.parse(req.param('where')) : {},
            req.headers.userrole == Admin.role ?
            {} :
            {status: sails.config.AVAILABLE_USER_STATUSES[1]}
        ));

        // if user is member - permit to view Portfolio album
        if (req.headers.usertype == 'member' && req.param('populate') && req.param('populate').split(',').indexOf('albums') !== -1) {
            var tmp = JSON.parse(req.param('populateWhere'));
            !tmp['albums'] ? tmp['albums'] = {} : null;
            if (tmp['albums']['name']) {
                if (!tmp['albums']['name']["!"]) {
                    tmp['albums']['name'] = {"!": [sails.config.custom_config.PORTFOLIO_ALBUM_NAME]};
                } else if (typeof tmp['albums']['name']["!"] === 'string') {
                    tmp['albums']['name']["!"] = [tmp['albums']['name']["!"], sails.config.custom_config.PORTFOLIO_ALBUM_NAME]
                } else {
                    tmp['albums']['name']["!"].push(sails.config.custom_config.PORTFOLIO_ALBUM_NAME);
                }
            } else {
                tmp['albums']['name'] = {"!": [sails.config.custom_config.PORTFOLIO_ALBUM_NAME]}
            }
            req.query.populateWhere = JSON.stringify(tmp);
        }

        // Start. Processing subrole
        if (req.headers.permitAttr) {
            for (var f in sails.config.SUB_ROLE_REQUIRED_FIELDS) {
                if (req.headers.permitAttr.indexOf(sails.config.SUB_ROLE_REQUIRED_FIELDS[f]) === -1)
                    req.headers.permitAttr.push(sails.config.SUB_ROLE_REQUIRED_FIELDS[f]);
            }
        }

        return customFind.find(req, Model).then(function (result) {
            // Continue. Processing subrole
            for (var i = 0; i < result.length; i++) {
                result[i].sub_role = User.getSubRole(result[i]);
                if (typeof originalPermitAttr !== 'undefined') {
                    // remove denied fields from response
                    for (var f in sails.config.SUB_ROLE_REQUIRED_FIELDS) {
                        if (originalPermitAttr.indexOf(sails.config.SUB_ROLE_REQUIRED_FIELDS[f]) === -1
                            && result[i][sails.config.SUB_ROLE_REQUIRED_FIELDS[f]]
                        )
                            delete result[i][sails.config.SUB_ROLE_REQUIRED_FIELDS[f]];
                    }
                }
            }
            // End. Processing subrole

            if(req.param('to_csv') == 'true') {
                var json2csv = require('json2csv');

                var fields = ['name', 'surname', 'email'],
                    fieldNames = ['Имя', 'Фамилия', 'email'];

                res.writeHead(200, {
                    'Content-type': 'text/csv',
                    'Content-Disposition': 'attachment; filename=file.csv',
                    'Pragma': 'no-cache',
                    'Expires': '0'
                });
                return res.end(json2csv({ data: result, fields: fields, fieldNames: fieldNames }));
            } else {
                return res.ok(result);
            }

        }).catch(function (error) {
            res.serverError(error);
        });
    },

    /**
     * Create profile
     *
     * @param req
     * @param res
     */
    create: function (req, res) {
        // deny to update friends while updating user data - only native methods
        delete req.body.friends;
        delete req.body.as_friend;
        delete req.body.avatar; // avatar field will fullfilled earlier

        Model.create(req.allParams()).then(function (user) {
            req.file('avatar').upload(sails.config.custom_config.POST_LIMIT_MB ? {maxBytes: sails.config.custom_config.POST_LIMIT_MB*1000000} : {}, function (err, uploadedFiles) {
                if (err) return res.serverError(err.message);
                if (uploadedFiles.length > 0) {
                    var request = require("request");
                    var path = require('path');
                    var fs = require('fs');

                    var r = request.post({
                        url: sails.config.custom_config.CDN_URL + "/avatar/" + user.id,
                        timeout: 60 * 1000
                    }, function (err, response, body) {
                        if (fs.existsSync(sails.config.custom_config.tmp_file_storage + path.basename(uploadedFiles[0].fd))) {
                            fs.unlinkSync(sails.config.custom_config.tmp_file_storage + path.basename(uploadedFiles[0].fd));
                        }
                        if (err) return res.badRequest(err.message);

                        if (response.statusCode == 200) {
                            // update image to model user
                            Model.update({id: user.id}, {avatar: JSON.parse(body).filepath}).exec(function (err, updated_user) {
                                if (err) return res.badRequest(err.message);
                                res.ok(updated_user[0]);
                            });
                        } else {
                            res.badRequest(JSON.parse(body).errors[0]);
                        }
                    });

                    var form = r.form();
                    form.append(uploadedFiles[0].filename, fs.createReadStream(sails.config.custom_config.tmp_file_storage + path.basename(uploadedFiles[0].fd)), {filename: path.basename(uploadedFiles[0].fd)});
                } else {
                    res.ok(user);
                }
            });
        }).catch(function (err) {
            res.badRequest(err);
        });
    },

    /**
     *  Update profile's data
     * @param req
     * @param res
     */
    update: function (req, res) {
        // deny to update friends while updating user data - only native methods
        delete req.body.friends;
        delete req.body.as_friend;
        // ability to remove avatar link from profile - CDN file will be replaced by another avatar image
        if(req.body.avatar !== "") delete req.body.avatar; // avatar field will fullfilled earlier

        // remove field if it is empty
        if(req.body.nickname == "") {
            delete req.body.nickname;
            var ObjectID = require(sails.config.mongodb_module_path).ObjectID;

            Model.native((err, collection) => {
                collection.update(
                    {"_id": new ObjectID(req.param('id'))},
                    {$unset: {nickname: true}}
                );
            });
        }

        Model.update({id: req.param('id')}, req.allParams()).then(function (user) {
            if (!user.length) throw 'No such Model user.';
            user = user[0];
            req.file('avatar').upload(sails.config.custom_config.POST_LIMIT_MB ? {maxBytes: sails.config.custom_config.POST_LIMIT_MB*1000000} : {}, function (err, uploadedFiles) {
                if (err) return res.serverError(err.message);
                if (uploadedFiles.length > 0) {
                    var request = require("request");
                    var path = require('path');
                    var fs = require('fs');

                    var r = request.post({
                        url: sails.config.custom_config.CDN_URL + "/avatar/" + user.id,
                        timeout: 60 * 1000
                    }, function (err, response, body) {
                        if (fs.existsSync(sails.config.custom_config.tmp_file_storage + path.basename(uploadedFiles[0].fd))) {
                            fs.unlinkSync(sails.config.custom_config.tmp_file_storage + path.basename(uploadedFiles[0].fd));
                        }
                        if (err) return res.badRequest(err.message);

                        if (response.statusCode == 200) {
                            // update image to model user
                            Model.update({id: user.id}, {avatar: JSON.parse(body).filepath}).exec(function (err, updated_user) {
                                if (err) return res.badRequest(err.message);
                                return res.ok({message: 'Success'});
                            });
                        } else {
                            res.badRequest(JSON.parse(body).errors[0]);
                        }
                    });

                    var form = r.form();
                    form.append(uploadedFiles[0].filename, fs.createReadStream(sails.config.custom_config.tmp_file_storage + path.basename(uploadedFiles[0].fd)), {filename: path.basename(uploadedFiles[0].fd)});
                } else {
                    return res.ok({message: 'Success'});
                }
            });
        }).catch(function (err) {
            res.badRequest(err);
        });
    },

    /**
     * Add or remove if added user to friends
     *
     * @param req
     * @param res
     */
    add_removeFriend: function (req, res) {
        Model.findOne({id: req.headers.uid}).populate('friends', {select: ['id']}).populate('as_friend', {select: ['id']}).then(function (user) {
            const friend_id = req.params.friend_id,
                ObjectID = require(sails.config.mongodb_module_path).ObjectID;

            let friends = user.friends.map((friend) => {
                return friend.id;
            });

            let friendOf = user.as_friend.map((friendOf) => {
                return friendOf.id;
            });

            let oldFriends = friends,
                oldInvitations = friendOf;

            if (friends.length === 0 || friends.indexOf(friend_id) === -1) {
                ActionLog.create({type: ActionLog.attributes.type.in[0], owner: req.headers.uid, details: {friend: new ObjectID(friend_id)}}).then(()=>{}).catch((e)=>{console.error(e)});
                friends.push(friend_id);
                model_as_friend__model_friends.native((err, collection) => {
                    if (err) return res.badRequest(err);

                    collection.insertOne({
                        "model_as_friend": new ObjectID(friend_id),
                        "model_friends": new ObjectID(req.headers.uid)
                    }, (err, result) => {
                        if(err) return res.badRequest(err);

                        res.ok({message: 'Success'});
                    });
                });
            } else {
                ActionLog.create({type: ActionLog.attributes.type.in[1], owner: req.headers.uid, details: {friend: new ObjectID(friend_id)}}).then(()=>{}).catch((e)=>{console.error(e)});
                // remove friend from friends list
                if(friends.indexOf(friend_id) !== -1) friends.splice(friends.indexOf(friend_id), 1);
                // remove from invitation list
                if(friendOf.indexOf(friend_id) !== -1) friendOf.splice(friendOf.indexOf(friend_id), 1);

                model_as_friend__model_friends.native((err, collection) => {
                    if (err) return res.badRequest(err);

                    Promise.all([
                        // remove friend from friends list
                        new Promise((resolve, reject) => {
                            collection.deleteOne({
                                "model_as_friend": new ObjectID(friend_id),
                                "model_friends": new ObjectID(req.headers.uid)
                            }, (err, result) => {
                                if(err) return reject(err);

                                resolve();
                            });
                        }),
                        // remove from invitation list
                        new Promise((resolve, reject) => {
                            collection.deleteOne({
                                "model_as_friend": new ObjectID(req.headers.uid),
                                "model_friends": new ObjectID(friend_id)
                            }, (err, result) => {
                                if(err) return reject(err);

                                resolve();
                            });
                        })
                    ]).then(() => {
                        res.ok({message: 'Success'});
                    }).catch((err) => {
                        res.badRequest(err);
                    });
                });
            }

            if(oldFriends.length > 1 && friends.length == 0) {
                ActionLog.create({
                    type: ActionLog.attributes.type.in[3],
                    owner: req.headers.uid,
                    details: {
                        before: oldFriends,
                        after: friends,
                        user_agent: req.headers['user-agent']
                    }
                }).then(()=>{}).catch((e)=>{console.error(e)});
            }

            if(oldInvitations.length > 1 && friendOf == 0) {
                ActionLog.create({
                    type: ActionLog.attributes.type.in[4],
                    owner: req.headers.uid,
                    details: {
                        before: oldInvitations,
                        after: friendOf,
                        user_agent: req.headers['user-agent']
                    }
                }).then(()=>{}).catch((e)=>{console.error(e)});
            }
        }).catch(function (error) {
            return res.badRequest(error);
        });
    },

    /**
     * Remove friendship invitation from other user
     *
     * @param req
     * @param res
     */
    removeInvitation: function (req, res) {
        Model.findOne({id: req.headers.uid}).populate('as_friend', {select: ['id']}).then(function (user) {
            const friend_id = req.params.friend_id,
                ObjectID = require(sails.config.mongodb_module_path).ObjectID;

            let friendOf = user.as_friend.map((friendOf) => {
                return friendOf.id;
            });

            let oldInvitations = friendOf;

            if(friendOf.indexOf(friend_id) !== -1) {
                ActionLog.create({type: ActionLog.attributes.type.in[2], owner: req.headers.uid, details: {friend: new ObjectID(friend_id)}}).then(()=>{}).catch((e)=>{console.error(e)});
                friendOf.splice(friendOf.indexOf(friend_id), 1);
            }

            if(oldInvitations.length > 1 && friendOf == 0) {
                ActionLog.create({
                    type: ActionLog.attributes.type.in[4],
                    owner: req.headers.uid,
                    details: {
                        before: oldInvitations,
                        after: friendOf,
                        user_agent: req.headers['user-agent']
                    }
                }).then(()=>{}).catch((e)=>{console.error(e)});
            }

            model_as_friend__model_friends.native((err, collection) => {
                if (err) return res.badRequest(err);

                Promise.all([
                    // remove from invitation list
                    new Promise((resolve, reject) => {
                        collection.deleteOne({
                            "model_as_friend": new ObjectID(user.id),
                            "model_friends": new ObjectID(friend_id)
                        }, (err, result) => {
                            if(err) return reject(err);

                            resolve();
                        });
                    })
                ]).then(() => {
                    res.ok({message: 'Success'});
                }).catch((err) => {
                    res.badRequest(err);
                });
            });
        }).catch(function (error) {
            return res.badRequest(error);
        });
    },

    /**
     * Make user account as paid
     * @param req
     * @param res
     */
    prepay: function (req, res) {
        var UID = req.param('id');

        Model.findOne({id: UID}).then(function(found) {
            if(!found) return res.badRequest({message: 'No user found.'});

            if(found.subscr_id) { // active Paypal subscription ID
                if(new Date(found.expiration) > new Date()) {
                    // deny to extend subscription manually because ipn request from Paypal will reject it when current subscription will end
                    return res.badRequest({ message: 'Paypal subscription can be extend manually only after it\'s expiration.' });
                } else {
                    // cancel delayed PAYPAL subscription
                    var request = require("request");
                    request.post({
                        url: sails.config.custom_config.PAYPAL.NVP_URL,
                        form: {
                            METHOD: 'ManageRecurringPaymentsProfileStatus',
                            PROFILEID: found.subscr_id,
                            ACTION: 'cancel',
                            USER: sails.config.custom_config.PAYPAL.NVP_USER,
                            PWD: sails.config.custom_config.PAYPAL.NVP_PWD,
                            SIGNATURE: sails.config.custom_config.PAYPAL.NVP_SIGNATURE,
                            NOTE: 'Cancelling due to manual payment by manager.',
                            VERSION: "54.0"
                        }
                    }, function (err, response, body) {
                        if(err) return res.badRequest(err);
                        /**
                         * Make paid
                         */
                        var expiration = req.param('expiration') && new Date(req.param('expiration')) > new Date()
                            ? new Date(req.param('expiration'))
                            : new Date(new Date().setFullYear(new Date().getFullYear() + 1));

                        Model.native(function(err, collection) {
                            if (err) console.error(err);
                            var ObjectID = require(sails.config.mongodb_module_path).ObjectID;

                            collection.update(
                                { "_id": new ObjectID(UID), subscr_id: found.subscr_id },
                                {
                                    $unset: { subscr_id: "" },
                                    $set: {
                                        expiration: expiration,
                                        payment_status: Model.attributes.payment_status.in[1]
                                    }
                                },
                                function(error, result) {
                                    if (error) return res.badRequest(error);
                                    res.ok({message: 'Success'});
                                });
                        });
                    });
                }
            } else {
                if(found.payment_status == Model.attributes.payment_status.in[1]) {
                    /**
                     * Make unpaid
                     */
                    Model.update({id: UID}, {
                        payment_status: Model.attributes.payment_status.in[0],
                        permitCode: sails.config.custom_config.permit_code  // for security update
                    }).then(function () {
                        res.ok({message: 'Success'});
                    }).catch(function (error) {
                        return res.badRequest(error);
                    });
                } else {
                    /**
                     * Make paid
                     */
                    var expiration = req.param('expiration') && new Date(req.param('expiration')) > new Date()
                        ? new Date(req.param('expiration'))
                        : new Date(new Date().setFullYear(new Date().getFullYear() + 1));

                    Model.update({id: UID}, {
                        expiration: expiration,
                        payment_status: Model.attributes.payment_status.in[1],
                        permitCode: sails.config.custom_config.permit_code  // for security update
                    }).then(function () {
                        res.ok({message: 'Success'});
                    }).catch(function (error) {
                        return res.badRequest(error);
                    });
                }
            }
        }).catch(function (error) {
            return res.badRequest(error);
        });
    },

    /**
     * Searching for one model's profile data
     *
     * @param req
     * @param res
     */
    customFindOne: function (req, res) {
        var User = require('../baseModels/User.js');

        var originalPermitAttr = _.cloneDeep(req.headers.permitAttr);

        req.query.where = this._buildWhere(req, res);

        req.query.populateWhere = req.query.populateWhere ? JSON.parse(req.query.populateWhere) : {};
        req.query.populateWhere.friends = _.merge({}, req.query.populateWhere.friends, {"select": req.query.populateWhere.friends && req.query.populateWhere.friends.select ? req.query.populateWhere.friends.select.concat(["id", "name", "avatar"].concat(sails.config.SUB_ROLE_REQUIRED_FIELDS)) : ["id", "name", "avatar"].concat(sails.config.SUB_ROLE_REQUIRED_FIELDS)});
        req.query.populateWhere.as_friend = _.merge({}, req.query.populateWhere.as_friend, {"select": req.query.populateWhere.as_friend && req.query.populateWhere.as_friend.select ? req.query.populateWhere.as_friend.select.concat(["id", "name", "avatar"].concat(sails.config.SUB_ROLE_REQUIRED_FIELDS)) : ["id", "name", "avatar"].concat(sails.config.SUB_ROLE_REQUIRED_FIELDS)});

        req.query.populateWhere = JSON.stringify(req.query.populateWhere);

        // Start. Processing subrole
        if (req.headers.permitAttr) {
            for (var f in sails.config.SUB_ROLE_REQUIRED_FIELDS) {
                if (req.headers.permitAttr.indexOf(sails.config.SUB_ROLE_REQUIRED_FIELDS[f]) === -1)
                    req.headers.permitAttr.push(sails.config.SUB_ROLE_REQUIRED_FIELDS[f]);
            }
        }

        return customFind.findOne(req, Model).then(function (result) {
            // Continue. Processing subrole
            result.sub_role = User.getSubRole(result);
            if (typeof originalPermitAttr !== 'undefined') {
                // remove denied fields from response
                for (var f in sails.config.SUB_ROLE_REQUIRED_FIELDS) {
                    if (originalPermitAttr.indexOf(sails.config.SUB_ROLE_REQUIRED_FIELDS[f]) === -1
                        && result[i][sails.config.SUB_ROLE_REQUIRED_FIELDS[f]]
                    )
                        delete result[sails.config.SUB_ROLE_REQUIRED_FIELDS[f]];
                }
            }
            // End. Processing subrole

            if(result.friends) {
                // Processing subrole for all friends
                for(var f = 0; f < result.friends.length; f++) {
                    result.friends[f].sub_role = User.getSubRole(result.friends[f]);
                    for (var i in sails.config.SUB_ROLE_REQUIRED_FIELDS) {
                        if ((typeof originalPermitAttr === 'undefined' ||
                            originalPermitAttr.indexOf(sails.config.SUB_ROLE_REQUIRED_FIELDS[i]) === -1)
                            && result.friends[f][sails.config.SUB_ROLE_REQUIRED_FIELDS[i]]
                        )
                            delete result.friends[f][sails.config.SUB_ROLE_REQUIRED_FIELDS[i]];
                    }
                }
            }

            if(result.as_friend) {
                // Processing subrole for all invitations for friendship
                for(var f = 0; f < result.as_friend.length; f++) {
                    result.as_friend[f].sub_role = User.getSubRole(result.as_friend[f]);
                    for (var i in sails.config.SUB_ROLE_REQUIRED_FIELDS) {
                        if ((typeof originalPermitAttr === 'undefined' ||
                            originalPermitAttr.indexOf(sails.config.SUB_ROLE_REQUIRED_FIELDS[i]) === -1)
                            && result.as_friend[f][sails.config.SUB_ROLE_REQUIRED_FIELDS[i]]
                        )
                            delete result.as_friend[f][sails.config.SUB_ROLE_REQUIRED_FIELDS[i]];
                    }
                }
            }

            res.ok(result);
        }).catch(function (error) {
            res.serverError(error);
        });
    },

    /**
     * Building request for searching only model users
     *
     * @param req Request's params
     * @param res
     * @returns {*}
     * @private
     */
    _buildWhere: function(req, res) {
        var _ = require('lodash');

        return JSON.stringify(_.merge({},
            req.param('where') ? JSON.parse(req.param('where')) : {},
            req.headers.userrole == Admin.role ? Model.buildSearchCriteriaFor('model') : _.merge({}, Model.buildSearchCriteriaFor('model'), {status: sails.config.AVAILABLE_USER_STATUSES[1]})
        ));
    }
};