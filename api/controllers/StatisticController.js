module.exports = {
    /**
     * Get statistic by owner, by dates, total
     *
     * @param req
     * @param res
     */
    summary: function (req, res) {
        var Promise = require('q'),
            where = req.query.where ? JSON.parse(req.query.where) : {};
        var owner = where.owner ? where.owner : false;
        _ = require('lodash');

        Promise.all([
            // all registered
            Model.count(_.merge(
                {},
                where.from ? {createdAt: {">": new Date(where.from)}} : {},
                where.to ? {createdAt: {"<=": new Date(where.to)}} : {},
                owner ? {parent: owner} : {}
            )),
            // only activated users
            Model.count(_.merge(
                {},
                where.from ? {expiration: {">": new Date(new Date(where.from).setFullYear(new Date(where.from).getFullYear() + 1))}} : {expiration:{"$ne": null}},
                where.to ? {expiration: {"<=": new Date(new Date(where.to).setFullYear(new Date(where.to).getFullYear() + 1))}} : {expiration:{"$ne": null}},
                owner ? {parent: owner} : {}
            )),
            // only model users
            Model.count(_.merge(
                {},
                Model.buildSearchCriteriaFor('model'),
                where.from ? {expiration: {">": new Date(new Date(where.from).setFullYear(new Date(where.from).getFullYear() + 1))}} : {},
                where.to ? {expiration: {"<=": new Date(new Date(where.to).setFullYear(new Date(where.to).getFullYear() + 1))}} : {},
                owner ? {parent: owner} : {}
            )),
            // paypal transactions
            /*owner ?
             Transaction.find(_.merge(
             {},
             where.from ? {createdAt: {">": new Date(where.from)}} : {},
             where.to ? {createdAt: {"<=": new Date(where.to)}} : {},
             {gateway_type: Transaction.attributes.gateway_type.in[0], payment_status: 'Completed', user_id: {"$exists": true}}
             )).populate('user_id') :
             Transaction.count(_.merge(
             {},
             where.from ? {createdAt: {">": new Date(where.from)}} : {},
             where.to ? {createdAt: {"<=": new Date(where.to)}} : {},
             {gateway_type: Transaction.attributes.gateway_type.in[0], payment_status: 'Completed'}
             )),*/
            // qiwi transactions
            owner ?
                Transaction.find(_.merge(
                    {},
                    where.from ? {createdAt: {">": new Date(where.from)}} : {},
                    where.to ? {createdAt: {"<=": new Date(where.to)}} : {},
                    {gateway_type: Transaction.attributes.gateway_type.in[1], payment_status: 'paid', user_id: {"$exists": true}}
                )).populate('user_id') :
                Transaction.count(_.merge(
                    {},
                    where.from ? {createdAt: {">": new Date(where.from)}} : {},
                    where.to ? {createdAt: {"<=": new Date(where.to)}} : {},
                    {gateway_type: Transaction.attributes.gateway_type.in[1], payment_status: 'paid'}
                )),
            owner ?
                EventInvitation.find(_.merge(
                    {},
                    where.from ? {createdAt: {">": new Date(where.from)}} : {},
                    where.to ? {createdAt: {"<=": new Date(where.to)}} : {},
                    {status: EventInvitation.attributes.status.in[4]}
                )).populate('model') :
                EventInvitation.count(_.merge(
                    {},
                    where.from ? {createdAt: {">": new Date(where.from)}} : {},
                    where.to ? {createdAt: {"<=": new Date(where.to)}} : {},
                    {status: EventInvitation.attributes.status.in[4]}
                )),
            owner ?
                CastingInvitation.find(_.merge(
                    {},
                    where.from ? {createdAt: {">": new Date(where.from)}} : {},
                    where.to ? {createdAt: {"<=": new Date(where.to)}} : {},
                    {status: CastingInvitation.attributes.status.in[4]}
                )).populate('model') :
                CastingInvitation.count(_.merge(
                    {},
                    where.from ? {createdAt: {">": new Date(where.from)}} : {},
                    where.to ? {createdAt: {"<=": new Date(where.to)}} : {},
                    {status: CastingInvitation.attributes.status.in[4]}
                ))
        ])
            .spread(function(registered, activated, models, qiwi_transactions, event_invitations, casting_invitations) {
                var qiwi = 0,
                    events = 0,
                    castings = 0;

                /*if(paypal_transactions instanceof Array) {
                 // populated data
                 for(var i = 0; i < paypal_transactions.length; i++) {
                 if(paypal_transactions[i].user_id && paypal_transactions[i].user_id.parent == owner) {
                 paypal++;
                 }
                 }
                 } else {
                 paypal = paypal_transactions;
                 }*/

                if(qiwi_transactions instanceof Array) {
                    // populated data
                    for(var i = 0; i < qiwi_transactions.length; i++) {
                        if(qiwi_transactions[i].user_id && qiwi_transactions[i].user_id.parent == owner) {
                            qiwi++;
                        }
                    }
                } else {
                    qiwi = qiwi_transactions;
                }

                if(event_invitations instanceof Array) {
                    // populated data
                    for(var i = 0; i < event_invitations.length; i++) {
                        if(event_invitations[i].model && event_invitations[i].model.parent == owner) {
                            events++;
                        }
                    }
                } else {
                    events = event_invitations;
                }

                if(casting_invitations instanceof Array) {
                    // populated data
                    for(var i = 0; i < casting_invitations.length; i++) {
                        if(casting_invitations[i].model && casting_invitations[i].model.parent == owner) {
                            castings++;
                        }
                    }
                } else {
                    castings = casting_invitations;
                }

                return res.ok({ registered: registered, activated: activated, models: models, paypal: activated - qiwi, qiwi: qiwi, events: events, castings: castings });
            })
            .catch(function(error){
                return res.badRequest(error);
            });
    },

    /**
     * Get statistic grouped by event managers
     *
     * @param req
     * @param res
     * @returns {*}
     */
    by_managers: function (req, res) {
        var where = req.query.where ? JSON.parse(req.query.where) : {};
        if(!where || !where.from || !where.to) return res.badRequest({message: 'Period of search not defined'});

        var Promise = require('q');

        EventManager.find().limit(5000).skip(0).populate('children').populate('related_cities', {select: ['name']}).then(function(found) {
            var waitFor = [];
            for(var i = 0; i < found.length; i++) {
                waitFor.push((function(i) {
                    var deferred = Promise.defer();

                    Promise.all([
                        CastingInvitation.count(_.merge(
                            {},
                            where.from ? {createdAt: {">": new Date(where.from)}} : {},
                            where.to ? {createdAt: {"<=": new Date(where.to)}} : {},
                            {status: CastingInvitation.attributes.status.in[4]},
                            {model: found[i].children.map(function(child) { return child.id; })}
                        )),
                        EventInvitation.count(_.merge(
                            {},
                            where.from ? {createdAt: {">": new Date(where.from)}} : {},
                            where.to ? {createdAt: {"<=": new Date(where.to)}} : {},
                            {status: EventInvitation.attributes.status.in[4]},
                            {model: found[i].children.map(function(child) { return child.id; })}
                        ))
                    ]).spread(function(castings, events) {
                        var tmp = {name: found[i].name, is_agent: found[i].is_agent, related_cities: found[i].related_cities, registered: 0, activated: 0, castings: castings, events: events};

                        for(var c = 0; c < found[i].children.length; c++) {
                            if(new Date(where.from) < new Date(found[i].children[c].createdAt) && new Date(where.to) >= new Date(found[i].children[c].createdAt)) tmp.registered++;
                            if(new Date(new Date(where.from).setFullYear(new Date(where.from).getFullYear() + 1)) < new Date(found[i].children[c].expiration) && new Date(new Date(where.to).setFullYear(new Date(where.to).getFullYear() + 1)) >= new Date(found[i].children[c].expiration)) tmp.activated++;
                        }

                        deferred.resolve(tmp);
                    }).catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                })(i))
            }


            Promise.allSettled(waitFor).then(function(data) {
                return res.ok(data.map(function(result) { return result.value }));
            });
        });
    }
};
