/**
 * ChatController
 *
 * @description :: Server-side logic for managing Chats
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    /**
     *  Get list of users with whom I had chat
     *
     * @param req
     * @param res
     * @returns {*}
     */
    getChatUsers: function (req, res) {
        req.query.where = JSON.parse(req.query.where);
        if(!req.query.where.to) return res.badRequest({message: 'Interlocutor ID as \"to\" param is not defined.'});

        Chat.native(function(err, collection) {
            if (err) return res.badRequest(err);

            var ObjectID = require(sails.config.mongodb_module_path).ObjectID;
            // search for send to me messages
            collection.aggregate([
                { $match : {$or: [
                    {
                        to: new ObjectID(req.query.where.to)
                    },
                    {
                        from: new ObjectID(req.query.where.to)
                    }
                ]}},
                { $group : {
                    _id : "$chat_id",
                    lastMessage : { $max : "$createdAt" }
                }},
                {$sort: {
                    lastMessage: -1
                }}
            ], function(err, result) {
                if(err) return res.badRequest(err);

                var Q = require('q'),
                    toPopulate = [],
                    User = require('../baseModels/User.js');

                for(var i = 0; i < result.length; i++) {
                    toPopulate.push((function(i) {
                        var deferred = Q.defer();

                        Model.find({id: result[i]._id.split('_')[0] === req.query.where.to ? result[i]._id.split('_')[1] : result[i]._id.split('_')[0]}, {select: ['id', 'name', 'surname', 'avatar', 'role', 'is_model', 'payment_status', 'expiration']}).then(function(found) {
                            if(found && found.length > 0) {
                                // get subrole using secure data
                                found[0].sub_role = User.getSubRole(found[0]);
                                // remove secure data
                                delete found[0].role;
                                delete found[0].is_model;
                                delete found[0].payment_status;
                                delete found[0].expiration;
                                result[i] = found[0];
                            }
                            deferred.resolve(true);
                        });

                        return deferred.promise;
                    })(i));
                }

                Q.allSettled(toPopulate).then(function(data) {
                    res.ok(result);
                });
            });
        });
    },

    /**
     * Get unread messages with all interlocutors for one user
     *
     * @param req
     * @param res
     */
    getUnreadMsgs: function(req, res) {
        Chat.native(function(err, collection) {
            if (err) return res.badRequest(err);
            req.query.where = JSON.parse(req.query.where);

            var ObjectID = require(sails.config.mongodb_module_path).ObjectID;
            // search for send to me messages
            collection.aggregate([
                {
                    $match: {
                        to: new ObjectID(req.query.where.to),
                        status: Chat.attributes.status.in[0]
                    }
                },
                {
                    $group: {
                        _id: "$from",
                        count: {$sum: 1}
                    }
                }
            ], function (err, result) {
                if(err) return res.badRequest(err);

                var Q = require('q'),
                    toPopulate = [],
                    User = require('../baseModels/User.js');

                for(var i = 0; i < result.length; i++) {
                    toPopulate.push((function(i) {
                        var deferred = Q.defer();

                        Model.find({id: result[i]['_id'].toString()}, {select: ['id', 'name', 'avatar', 'role', 'is_model', 'payment_status', 'expiration']}).then(function(found) {
                            var resultUserData = {};
                            if(found && found.length > 0) {
                                resultUserData.id = found[0].id;
                                resultUserData.name = found[0].name;
                                resultUserData.avatar = found[0].avatar;
                                // get subrole using secure data
                                resultUserData.sub_role = User.getSubRole(found[0]);
                                resultUserData.unread_count = result[i]['count'];
                                result[i] = resultUserData;
                            }
                            deferred.resolve(true);
                        });

                        return deferred.promise;
                    })(i));
                }

                Q.allSettled(toPopulate).then(function(data) {
                    res.ok(result);
                });
            });
        });
    },

    /**
     * Mark message as read
     *
     * @param req
     * @param res
     */
    setAsRead: function (req, res) {
        Chat.update({id: req.param('id')}, {status: Chat.attributes.status.in[1], readAt: new Date()}).then(function(data) {
            res.ok(data[0]);
        }).catch(function(error) {
            res.badRequest(error);
        });
    },

    customFind: function (req, res) {
        // if user - get only own messages
        if(req.headers.userrole == 'model') {
            req.query.where = req.query.where ? JSON.parse(req.query.where) : {};
            if(req.query.where.or) {
                if((req.query.where.or[0].to !== req.headers.uid && req.query.where.or[1].to !== req.headers.uid) || (req.query.where.or[0].from !== req.headers.uid && req.query.where.or[1].from !== req.headers.uid)) {
                    req.query.where.or = [{"to": req.headers.uid}, {"from": req.headers.uid}];
                }
            } else {
                req.query.where.or = [{"to": req.headers.uid}, {"from": req.headers.uid}];
            }

            req.query.where = JSON.stringify(req.query.where);

            return customFind.find(req, Chat).then(function(result) {
                res.ok(result);
            }).catch(function(error) {
                res.serverError(error);
            });
        } else {
            return customFind.find(req, Chat).then(function(result) {
                res.ok(result);
            }).catch(function(error) {
                res.serverError(error);
            });
        }
    },

    create: function(req, res) {
        var params = req.allParams();
        params.chat_id = params.from > params.to ? params.to+'_'+params.from : params.from+'_'+params.to;

        Chat.create(params).then(function (message) {
            Chat.find({id: message.id}).populate('from', {select: ['id', 'name', 'avatar']}).populate('to', {select: ['id', 'name', 'avatar']}).then(function(result) {
                res.ok(result[0]);
            }).catch(function(error) {
                res.serverError(error);
            });
        }).catch(function(error) {
            res.serverError(error);
        });
    }
};

