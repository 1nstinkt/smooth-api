/**
 * WallController
 *
 * @description :: Server-side logic for managing Walls
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    customFind: function (req, res) {
        var Q = require('q');

        req.query.where = JSON.stringify(_.merge({},
            req.param('where') ? JSON.parse(req.param('where')) : {},
            {
                status: Post.attributes.status.in[0], // search only active comments
                type: [Post.attributes.type.in[0], Post.attributes.type.in[1]] // search only posts and reposts
            }
        ));

        return customFind.find(req, Post).then((function (result) {
            var populateNestedPromises = customFind.populateNestedFields(req, result, Post);

            Q.allSettled(populateNestedPromises).then(function (data) {
                res.ok(result);
            });
        }).bind(this)).catch(function (error) {
            res.serverError(error);
        });
    },

    /**
     * Timeline for selected user
     *
     * @param req
     * @param res
     */
    timeline(req, res) {
        let owner = req.param('owner_id'),
            User = require('../baseModels/User.js');

        var originalPermitAttr = _.cloneDeep(req.headers.permitAttr);

        req.query.where = _.merge({},
            req.param('where') ? JSON.parse(req.param('where')) : {},
            {
                status: Post.attributes.status.in[0], // search only active posts
            }
        );

        // search for post that made by:
        req.query.where.or = [
            {"user_id": owner, "sender_id": owner, type: Post.attributes.type.in[0]}, // own post in own timeline
            {"user_id": owner, "sender_id": owner, type: Post.attributes.type.in[1]}, // own reposts in own timeline
            {"sender_id": owner, type: Post.attributes.type.in[0]}, // own posts in timeline of friends
            {"user_id": owner, type: Post.attributes.type.in[0]} // all other posts in own timeline
        ];

        // Start. Processing subrole

        // additional fields for sender_id subrole in comment and original_post of post
        if (req.query.populateNested) {
            req.query.populateNested = JSON.parse(req.query.populateNested);
            for (var f in sails.config.SUB_ROLE_REQUIRED_FIELDS) {
                if (req.query.populateNested.comments.sender_id.indexOf(sails.config.SUB_ROLE_REQUIRED_FIELDS[f]) === -1)
                    req.query.populateNested.comments.sender_id.push(sails.config.SUB_ROLE_REQUIRED_FIELDS[f]);
                if (req.query.populateNested.original_post.sender_id.indexOf(sails.config.SUB_ROLE_REQUIRED_FIELDS[f]) === -1)
                    req.query.populateNested.original_post.sender_id.push(sails.config.SUB_ROLE_REQUIRED_FIELDS[f]);
            }
            req.query.populateNested = JSON.stringify(req.query.populateNested);
        }

        if (req.headers.permitAttr) {
            for (var f in sails.config.SUB_ROLE_REQUIRED_FIELDS) {
                if (req.headers.permitAttr.indexOf('sender_id.' + sails.config.SUB_ROLE_REQUIRED_FIELDS[f]) === -1)
                    req.headers.permitAttr.push('sender_id.' + sails.config.SUB_ROLE_REQUIRED_FIELDS[f]);
            }
        }

        Model.findOne({id: owner}).populate('friends').then(found => {
            // search for own friend's posts, that he made by himself in his timeline
            found.friends.map( friend => {
                req.query.where.or.push({"user_id": friend.id, "sender_id": friend.id, type: Post.attributes.type.in[0]}); // view only posts by friends
            });
            // auto subscription
            sails.config.AUTO_SUBCRIBE_ON_USERS.map(user_id => {
                req.query.where.or.push({"user_id": user_id, "sender_id": user_id});
            });

            req.query.where = JSON.stringify(req.query.where);

            return customFind.find(req, Post).then(function (result) {
                // Continue. Processing subrole
                result.map( res => {
                    res.sender_id.sub_role = User.getSubRole(res.sender_id);
                    if (typeof originalPermitAttr !== 'undefined') {
                        // remove denied fields from response
                        sails.config.SUB_ROLE_REQUIRED_FIELDS.map( field => {
                            if (originalPermitAttr.indexOf(`sender_id.${field}`) === -1 && res['sender_id'][field])
                                delete res['sender_id'][field];
                        });
                    }
                });
                // End. Processing subrole

                var populateNestedPromises = customFind.populateNestedFields(req, result, Post);

                Promise.all(populateNestedPromises).then(function (data) {
                    // generating subrole for sender_id in comments
                    result.map( res => {
                        for(var k = 0; k < res.comments.length; k++) {
                            res.comments[k].sender_id.sub_role = User.getSubRole(res.comments[k].sender_id);
                            for (var f in sails.config.SUB_ROLE_REQUIRED_FIELDS) {
                                if (typeof res.comments[k].sender_id[sails.config.SUB_ROLE_REQUIRED_FIELDS[f]] !== 'undefined')
                                    delete res.comments[k].sender_id[sails.config.SUB_ROLE_REQUIRED_FIELDS[f]]; // remove secure fields
                            }
                        }
                        // get subrole of sender in original post of repost
                        if(res.original_post && res.original_post.length > 0) {
                            res.original_post[0].sender_id.sub_role = User.getSubRole(res.original_post[0].sender_id);
                            // remove secure fields
                            for (var f in sails.config.SUB_ROLE_REQUIRED_FIELDS) {
                                if (typeof res.original_post[0].sender_id[sails.config.SUB_ROLE_REQUIRED_FIELDS[f]] !== 'undefined')
                                    delete res.original_post[0].sender_id[sails.config.SUB_ROLE_REQUIRED_FIELDS[f]]; // remove secure fields
                            }
                        }
                    });

                    return res.ok(result);
                });
            }).catch(function (error) {
                res.serverError(error);
            });
        }).catch(function (error) {
            res.serverError(error);
        });
    },

    _formatMapCoordinates: function (req, res) {
        if (req.body.map) {
            req.body.map = req.body.map.split(',');
            if (typeof req.body.map[0] !== 'undefined' && typeof req.body.map[1] !== 'undefined') {
                if (parseFloat(req.body.map[0]) == 0 && parseFloat(req.body.map[1]) == 0) {
                    delete req.body.map;
                } else {
                    req.body.map = [parseFloat(req.body.map[0]), parseFloat(req.body.map[1])];
                }
            } else {
                return res.badRequest(new Error('Coordinates are invalid'));
            }
        }
    },

    /**
     * Populate all related to post fields
     *
     * @param post
     * @param res
     * @private
     */
    _populateResult: function (req, res, post) {
        let Q = require('q'),
            User = require('../baseModels/User.js');

        // Start. Processing subrole
        if (req.headers.permitAttr) {
            for (var f in sails.config.SUB_ROLE_REQUIRED_FIELDS) {
                if (req.headers.permitAttr.indexOf('sender_id.' + sails.config.SUB_ROLE_REQUIRED_FIELDS[f]) === -1)
                    req.headers.permitAttr.push('sender_id.' + sails.config.SUB_ROLE_REQUIRED_FIELDS[f]);
            }
        }

        req.query.where = JSON.stringify({"id": post.id});
        req.query.populate = "likes,comments,original_post,user_id,sender_id,parent_post,reposts";
        req.query.populateNested = JSON.stringify({
            "original_post": {
                "sender_id": ["id", "name", "surname", "avatar"].concat(sails.config.SUB_ROLE_REQUIRED_FIELDS),
                "reposts": ["id"]
            },
            "comments": {
                "likes": ["id", "name", "createdAt", "avatar"],
                "user_id": ["id", "name", "avatar"],
                "sender_id": ["id", "name", "surname", "avatar"].concat(sails.config.SUB_ROLE_REQUIRED_FIELDS)
            },
            "parent_post": {
                "sender_id": ["id", "name", "avatar"]
            }
        });

        customFind.find(req, Post).then(function (result) {
            // Continue. Processing subrole
            for (var i = 0; i < result.length; i++) {
                result[i].sender_id.sub_role = User.getSubRole(result[i].sender_id);
                if (typeof originalPermitAttr !== 'undefined') {
                    // remove denied fields from response
                    for (var f in sails.config.SUB_ROLE_REQUIRED_FIELDS) {
                        if (originalPermitAttr.indexOf('sender_id.' + sails.config.SUB_ROLE_REQUIRED_FIELDS[f]) === -1
                            && result[i]['sender_id'][sails.config.SUB_ROLE_REQUIRED_FIELDS[f]]
                        )
                            delete result[i]['sender_id'][sails.config.SUB_ROLE_REQUIRED_FIELDS[f]];
                    }
                }
            }
            // End. Processing subrole

            var populateNestedPromises = customFind.populateNestedFields(req, result, Post);

            Q.allSettled(populateNestedPromises).then(function (data) {
                for(var r = 0; r < result.length; r++) {
                    for(var k = 0; k < result[r].comments.length; k++) {
                        // generating subrole for sender_id in comment
                        result[r].comments[k].sender_id.sub_role = User.getSubRole(result[r].comments[k].sender_id);
                        for (var f in sails.config.SUB_ROLE_REQUIRED_FIELDS) {
                            if (typeof result[r].comments[k].sender_id[sails.config.SUB_ROLE_REQUIRED_FIELDS[f]] !== 'undefined')
                                delete result[r].comments[k].sender_id[sails.config.SUB_ROLE_REQUIRED_FIELDS[f]]; // remove secure fields
                        }
                    }
                    if(result[r].original_post && result[r].original_post.length > 0) {
                        // get subrole of sender in original post of repost
                        result[r].original_post[0].sender_id.sub_role = User.getSubRole(result[r].original_post[0].sender_id);
                        // remove secure fields
                        for (var f in sails.config.SUB_ROLE_REQUIRED_FIELDS) {
                            if (typeof result[r].original_post[0].sender_id[sails.config.SUB_ROLE_REQUIRED_FIELDS[f]] !== 'undefined')
                                delete result[r].original_post[0].sender_id[sails.config.SUB_ROLE_REQUIRED_FIELDS[f]]; // remove secure fields
                        }
                    }
                }

                return res.ok(result);
            });
        }).catch(function (error) {
            res.serverError(error);
        });
    },

    create: function (req, res) {
        var THIS = this;
        Post.findOne({id: req.param('parent_post')}).then(function (found) {
            if (!found) delete req.body.parent_post;

            THIS._formatMapCoordinates(req, res);

            Post.create(req.allParams()).then(function (post) {
                req.file('images').upload(sails.config.custom_config.POST_LIMIT_MB ? {maxBytes: sails.config.custom_config.POST_LIMIT_MB*1000000} : {}, function (err, uploadedFiles) {
                    if (err) return res.serverError(err.message);
                    if (uploadedFiles.length > 0) {
                        var request = require("request"),
                            Q = require('q'),
                            path = require('path'),
                            _ = require('lodash'),
                            fs = require('fs'),
                            imagesToStore = [];

                        for (var i in uploadedFiles) {
                            imagesToStore.push((function () {
                                var deferred = Q.defer();

                                var r = request.post({
                                    url: sails.config.custom_config.CDN_URL + "/gallery/" + post.id,
                                    timeout: 60 * 1000
                                }, function (err, response, body) {
                                    if (fs.existsSync(sails.config.custom_config.tmp_file_storage + path.basename(uploadedFiles[i].fd))) {
                                        fs.unlinkSync(sails.config.custom_config.tmp_file_storage + path.basename(uploadedFiles[i].fd));
                                    }

                                    if (err) {
                                        console.log(err);
                                        return deferred.reject('Error while uploading image to CDN.');
                                    } else {
                                        if (response.statusCode == 200) {
                                            deferred.resolve({image: JSON.parse(body).filepath});
                                        } else {
                                            deferred.reject(JSON.parse(body).errors[0]);
                                        }
                                    }
                                });

                                var form = r.form();
                                form.append(uploadedFiles[i].filename, fs.createReadStream(sails.config.custom_config.tmp_file_storage + path.basename(uploadedFiles[i].fd)), {filename: path.basename(uploadedFiles[i].fd)});

                                return deferred.promise;
                            })())
                        }

                        Q.allSettled(imagesToStore).then(function (data) {
                            var addedImages = [];
                            for (var i in data) {
                                if (data[i].state == 'rejected') return res.badRequest(data[i].reason);
                                if (!post.images) post.images = [];
                                post.images.push(data[i].value.image);
                                addedImages.push(data[i].value.image);
                            }

                            Post.update({id: post.id}, {images: post.images}).then(function (updated) {
                                THIS._populateResult(req, res, updated[0]);
                            }).catch(function (err) {
                                res.badRequest(err);
                            });
                        })
                    } else {
                        THIS._populateResult(req, res, post);
                    }
                });
            }).catch(function (err) {
                res.badRequest(err);
            });
        });
    },

    update: function (req, res) {
        delete req.body.type; // can not be updated
        delete req.body.parent_post;  // can not be updated

        this._formatMapCoordinates(req, res);

        Post.update({id: req.param('id')}, req.allParams()).then(function (post) {
            if (!post.length) throw 'No such Post.';
            post = post[0];
            req.file('image').upload(sails.config.custom_config.POST_LIMIT_MB ? {maxBytes: sails.config.custom_config.POST_LIMIT_MB*1000000} : {}, function (err, uploadedFiles) {
                if (err) return res.serverError(err.message);
                if (uploadedFiles.length > 0) {
                    var request = require("request"),
                        Q = require('q'),
                        path = require('path'),
                        _ = require('lodash'),
                        fs = require('fs'),
                        imagesToStore = [];

                    // add new images
                    for (var i in uploadedFiles) {
                        imagesToStore.push((function () {
                            var deferred = Q.defer();

                            var r = request.post({
                                url: sails.config.custom_config.CDN_URL + "/gallery/" + post.id,
                                timeout: 60 * 1000
                            }, function (err, response, body) {
                                if (fs.existsSync(sails.config.custom_config.tmp_file_storage + path.basename(uploadedFiles[i].fd))) {
                                    fs.unlinkSync(sails.config.custom_config.tmp_file_storage + path.basename(uploadedFiles[i].fd));
                                }

                                if (err) {
                                    console.log(err);
                                    return deferred.reject('Error while uploading image to CDN.');
                                } else {
                                    if (response.statusCode == 200) {
                                        deferred.resolve({image: JSON.parse(body).filepath});
                                    } else {
                                        deferred.reject(JSON.parse(body).errors[0]);
                                    }
                                }
                            });

                            var form = r.form();
                            form.append(uploadedFiles[i].filename, fs.createReadStream(sails.config.custom_config.tmp_file_storage + path.basename(uploadedFiles[i].fd)), {filename: path.basename(uploadedFiles[i].fd)});

                            return deferred.promise;
                        })())
                    }

                    Q.allSettled(imagesToStore).then(function (data) {
                        var addedImages = [],
                            imagesToRemove = [];

                        for (var i in data) {
                            if (data[i].value && data[i].value.image) addedImages.push(data[i].value.image);
                        }

                        var path = require('path');
                        // remove earlier images
                        for (var i in post.images) {
                            imagesToRemove.push((function () {
                                var deferred = Q.defer();

                                request.delete({
                                    url: sails.config.custom_config.CDN_URL + "/" + post.id + "/gallery/" + path.basename(post.images[i]),
                                    timeout: 60 * 1000
                                }, function (err, response, body) {
                                    if (err) {
                                        console.log(err);
                                        return deferred.reject('Error while removing image from CDN.');
                                    } else {
                                        if (response.statusCode == 200) {
                                            return deferred.resolve({message: 'ok'});
                                        } else {
                                            return deferred.reject(JSON.parse(body).errors[0]);
                                        }
                                    }
                                });

                                return deferred.promise;
                            })());
                        }

                        Q.allSettled(imagesToRemove).then(function (data) {
                            Post.update({id: post.id}, {images: addedImages}).then(function (updated) {
                                res.ok({message: {addedImages: addedImages, removedImages: post.images}});
                            }).catch(function (err) {
                                res.badRequest(err);
                            });
                        });
                    });
                } else {
                    res.ok(post);
                }
            });
        }).catch(function (err) {
            res.badRequest(err);
        });
    },

    destroy: function (req, res) {
        var Promise = require('q'),
            _ = require('lodash');

        Promise.all([
            Post.find({parent_post: req.param('id'), type: Post.attributes.type.in[2]}), // find all comments
            Post.findOne({id: req.param('id')}).populate('reposts').populate('original_post', {select: ["id"]}) // find original post
        ])
            .spread((function (comments, post) {
                if (!post) return res.badRequest({message: 'No such original post.'});
                var entitiesToRemove = [],
                    request = require("request"),
                    path = require('path');


                // remove images of original
                if (post.images && post.images.length > 0) {
                    for (var i in post.images) {
                        entitiesToRemove.push((function () {
                            var deferred = Promise.defer();

                            request.delete({
                                url: sails.config.custom_config.CDN_URL + "/" + post.id + "/gallery/" + path.basename(post.images[i]),
                                timeout: 60 * 1000
                            }, function (err, response, body) {
                                if (err) {
                                    console.log(err);
                                    return deferred.reject('Error while removing image from CDN.');
                                } else {
                                    if (response.statusCode == 200) {
                                        return deferred.resolve({message: 'ok'});
                                    } else {
                                        return deferred.reject(JSON.parse(body).errors[0]);
                                    }
                                }
                            });

                            return deferred.promise;
                        })());
                    }
                }

                for(var i = 0; i < post.reposts.length; i++) {
                    // remove images of all child reposts
                    if(post.reposts[i].images && post.reposts[i].images.length > 0) {
                        for (var k in post.reposts[i].images) {
                            entitiesToRemove.push((function () {
                                var deferred = Promise.defer();

                                request.delete({
                                    url: sails.config.custom_config.CDN_URL + "/" + post.reposts[i].id + "/gallery/" + path.basename(post.reposts[i].images[k]),
                                    timeout: 60 * 1000
                                }, function (err, response, body) {
                                    if (err) {
                                        console.log(err);
                                        return deferred.reject('Error while removing image from CDN.');
                                    } else {
                                        if (response.statusCode == 200) {
                                            return deferred.resolve({message: 'ok'});
                                        } else {
                                            return deferred.reject(JSON.parse(body).errors[0]);
                                        }
                                    }
                                });

                                return deferred.promise;
                            })());
                        }
                    }
                    // remove comments of child reposts
                    entitiesToRemove.push((function () {
                        var deferred = Promise.defer(),
                            repostIDs = _.map(post.reposts, function(repost) {
                                return repost.id;
                            });

                        Post.destroy({parent_post: repostIDs, type: Post.attributes.type.in[2]}).then(function(removed) {
                            return deferred.resolve({message: 'ok'});
                        });

                        return deferred.promise;
                    })());
                }

                Promise.allSettled(entitiesToRemove).then((function (data) {
                    Promise.allSettled([
                        Post.destroy({id: req.param('id')}),
                        this._removeAllChildren(post.reposts),
                        this._removeAllChildren(comments)
                    ]).spread(function (original, children, comments) {
                        var removedReposts = [],
                            removedComments = [];

                        for (var i in children.value) {
                            if (children.value[i].value && children.value[i].value) removedReposts.push(children.value[i].value);
                        }

                        for (var i in comments.value) {
                            if (comments.value[i].value && comments.value[i].value) removedComments.push(comments.value[i].value);
                        }

                        res.ok(post);
                    });
                }).bind(this));
            }).bind(this));
    },

    _removeAllChildren: function (children) {
        var Promise = require('q');
        if (children.length) {
            var childrenToRemove = [];

            for (var i = 0; i < children.length; i++) {
                childrenToRemove.push((function () {
                    var deferred = Promise.defer();

                    Post.destroy({id: children[i].id}).then(function (removed) {
                        deferred.resolve(removed[0].id);
                    });

                    return deferred.promise;
                })());
            }

            return Promise.allSettled(childrenToRemove)
        } else {
            return Promise.defer().resolve(true);
        }
    },

    like: function (req, res) {
        let THIS = this;

        Post.findOne({id: req.param('id')}).populate('likes').then(function (found) {
            for (var i = 0; i < found.likes.length; i++) {
                found.likes[i] = found.likes[i].id;
            }

            if (!found.likes || !found.likes.length) {
                found.likes = [req.headers.uid];
            } else if (found.likes.indexOf(req.headers.uid) === -1) {
                found.likes.push(req.headers.uid)
            } else {
                found.likes.splice(found.likes.indexOf(req.headers.uid), 1);
            }

            Post.update({id: found.id}, {likes: found.likes}).then(function (updated) {
                return THIS._populateResult(req, res, updated[0]);
            }).catch(function (error) {
                return res.badRequest(error);
            });
        }).catch(function (error) {
            return res.badRequest(error);
        });
    }
};

