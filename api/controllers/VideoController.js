/**
 * VideoController
 *
 * @description :: Server-side logic for managing Videos
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    /**
     * Update video
     *
     * @param req
     * @param res
     * @returns {*}
     */
    update: (req, res) => {
        if(!req.body.user_id) return res.badRequest('Manager should pass \"user_id\" parameter');
        req.file('video').upload(sails.config.custom_config.POST_LIMIT_MB ? {maxBytes: sails.config.custom_config.POST_LIMIT_MB*1000000} : {}, (err, uploadedFiles) => {
            if (err) return res.serverError(err.message);
            if(uploadedFiles.length > 0) {
                var request = require("request"),
                    path = require('path'),
                    fs = require('fs');

                var r = request.post({
                    url: sails.config.custom_config.CDN_URL + "/portfolio_video/" + req.body.user_id,
                    timeout: 60 * 1000
                }, (err, response, body) => {
                    if (fs.existsSync(sails.config.custom_config.tmp_file_storage + path.basename(uploadedFiles[0].fd))) {
                        fs.unlinkSync(sails.config.custom_config.tmp_file_storage + path.basename(uploadedFiles[0].fd));
                    }
                    if (err) return res.badRequest(err.message);

                    if(response.statusCode == 200) {
                        res.ok({ message: path.basename(JSON.parse(body).filepath)});
                    } else {
                        res.badRequest(JSON.parse(body).errors[0]);
                    }
                });

                var form = r.form();
                form.append(uploadedFiles[0].filename, fs.createReadStream(sails.config.custom_config.tmp_file_storage + path.basename(uploadedFiles[0].fd)), {filename: req.params.name});
            } else {
                res.badRequest({ message: 'Video parameter is empty.' });
            }
        });
    },

    // remove video from album
    destroy: (req, res) => {
        Album.findOne({ videos: req.params.name }).then( album => {
            if(!album) return res.badRequest({ message: 'Video not found.' });
            var request = require("request");

            request.delete({
                url: sails.config.custom_config.CDN_URL + "/" + album.owner + "/portfolio_video/" + req.params.name,
                timeout: 60 * 1000
            }, (err, response, body) => {
                try {
                    if(err || JSON.parse(body).status == 'Error') return res.badRequest({ message: err ? err.message : JSON.parse(body).errors});

                    let videos = album.videos;
                    if(videos.indexOf(req.params.name) !== -1) videos.splice(videos.indexOf(req.params.name), 1);
                    Album.update({ id: album.id }, { videos: videos }).then( updated => {
                        res.ok({ message: 'Video was completely removed.' });
                    }).catch( err => {
                        res.badRequest(err);
                    });
                } catch (e) {
                    res.badRequest({message: body});
                }
            });
        });
    },

    // remove all videos
    clearAll: function(req, res) {
        Album.findOne({ id: req.param('id') }).then(function(album) {
            if(!album) return res.badRequest({ message: 'Album not found.' });
            if(album.videos && album.videos.length) {
                var request = require("request");

                request.delete({
                    url: `${sails.config.custom_config.CDN_URL}/clear_portfolio_videos/${album.owner}`,
                    timeout: 60 * 1000
                }, (err, response, body) => {
                    if (err) return deferred.reject(err.message);

                    if(response.statusCode == 200) {
                        Album.update({ id: req.param('id') }, { videos: [] }).exec((err, updated_album) => {
                            if (err) return res.badRequest(err.message);
                            res.ok(updated_album[0]);
                        });
                    } else {
                        try {
                            return res.badRequest(JSON.parse(body).errors[0]);
                        } catch (e) {
                            return res.badRequest(response.statusMessage);
                        }
                    }
                });
            } else {
                res.ok(album);
            }
        });
    }
};

