/**
 * CastingInvitationController
 *
 * @description :: Server-side logic for managing Castinginvitations
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    customFind: function (req, res) {
        return customFind.find(req, CastingInvitation).then(result => {
            result.map((invitation) => {
                if(typeof invitation.model !== 'undefined') invitation.model.sub_role = require('../baseModels/User.js').getSubRole(invitation.model)
            });

            res.ok(result);
        }).catch(function(error) {
            res.serverError(error);
        });
    },

    create: function(req, res) {
        var params = req.allParams();

        CastingInvitation.create(params).then(function (castinginvitation) {
            return CastingInvitation.find({id: castinginvitation.id}).populate('casting');
        }).then(function(result) {
            res.ok(result[0]);
        }).catch(function(error) {
            res.serverError(error);
        });
    }
};

