module.exports = {
    restore_friends: (req, res) => {
        const userId = req.params.id;
        const ObjectID = require(sails.config.mongodb_module_path).ObjectID;
        // Build A Mongo Connection String
        let connectionString = 'mongodb://',
            config = sails.config,
            MongoClient = require(sails.config.mongodb_module_path).MongoClient;

        // If auth is used, append it to the connection string
        if (config.connections.mongo.user && config.connections.mongo.password) {

            // Ensure a database was set if auth in enabled
            if (!config.connections.mongo.database) {
                return res.badRequest({message: 'The MongoDB Adapter requires a database config.connections.mongo option if authentication is used.'});
            }

            connectionString += config.connections.mongo.user + ':' + config.connections.mongo.password + '@';
        }

        // Append the host and port
        connectionString += config.connections.mongo.host + ':' + config.connections.mongo.port + '/';

        if (config.connections.mongo.database) {
            connectionString += config.connections.mongo.database;
        }

        // Use config.connections.mongo connection string if available
        if (config.connections.mongo.url) connectionString = config.connections.mongo.url;

        ActionLog.find({details: {friend: new ObjectID(userId)}}).then((data) => {
            MongoClient.connect(connectionString, (err, db) => {
                let col = db.collection('model_as_friend__model_friends');

                let batch = col.initializeOrderedBulkOp();

                data = JSON.parse(JSON.stringify(data));
                data.map((event) => {
                    batch.find({model_friends: new ObjectID(event.owner), model_as_friend: new ObjectID(userId)}).upsert().updateOne({model_friends: new ObjectID(event.owner), model_as_friend: new ObjectID(userId)});
                    batch.find({model_as_friend: new ObjectID(event.owner), model_friends: new ObjectID(userId)}).upsert().updateOne({model_as_friend: new ObjectID(event.owner), model_friends: new ObjectID(userId)});
                });
                if(data.length > 0) {
                    batch.execute(function (err, result) {
                        db.close();
                        if (err) return res.badRequest({message: err.message});
                        res.ok({message: 'Success'});
                    });
                } else {
                    res.ok({message: 'Nothing to restore'});
                }

            });

        });
    }
};