var passport = require('passport');
var jwtToken = require('./../services/jwtToken');

module.exports = {
    login: function (req, res) {
        passport.authenticate('local', {session: false}, function (err, user) {
            if(err) {
                res.badRequest({"message": err.message});
            }
            if (!user) res.forbidden({"message": "Invalid user/password"});
            else {
                var User = require('../baseModels/User.js');
                user.sub_role = User.getSubRole(user);
                res.ok({
                    uid: user.id,
                    token: jwtToken.issueToken({id: user.id}), // generate token
                    profile: user
                });
            }
        })(req, res);
    },

    logout: function (req, res) {
        req.logOut();
        return res.ok({message: 'Ok'});
    }

};
