/**
 * WorksheetController
 *
 * @description :: Server-side logic for managing worksheets
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    createOrUpdate(req, res) {
        Worksheet.findOne({user: req.body.user}).then((found) => {
            if (!found) {
                Worksheet.create({user: req.body.user, data: req.body.data}).then(() => {
                    res.ok({message: 'Success'});
                }).catch(function (error) {
                    return res.badRequest(error);
                });
            } else {
                Worksheet.update({user: req.body.user}, {data: req.body.data}).then(() => {
                    res.ok({message: 'Success'});
                }).catch(function (error) {
                    return res.badRequest(error);
                });
            }
        }).catch(function (error) {
            return res.badRequest(error);
        });
    },

    customFindOne(req, res) {
        Worksheet.findOne({user: req.params.uid}).then(function (found) {
            return res.ok(found);
        }).catch(function (error) {
            return res.badRequest(error);
        });
    }
};