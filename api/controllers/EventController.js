/**
 * EventsController
 *
 * @description :: Server-side logic for managing Events
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    customFind: function (req, res) {
        return require('../baseControllers/News.js').customFind(req, res, Event);
    },

    customFindOne(req, res) {
        return require('../baseControllers/News.js').customFindOne(req, res, Event);
    },

    create: function (req, res) {
        return require('../baseControllers/News.js').create(req, res, Event);
    },

    update: function (req, res) {
        return require('../baseControllers/News.js').update(req, res, Event);
    },

    destroy: function (req, res) {
        return require('../baseControllers/News.js').destroy(req, res, Event);
    },

    getCities: function(req, res) {
        Event.native(function(err, collection) {
            if (err) {
                console.error(err);
                return res.badRequest({message: err.message});
            }

            collection.aggregate([
                { $match: {
                    country: JSON.parse(req.query.where).country
                }},
                { $group : {
                    _id : "$city"
                }},
                {$sort: {
                    _id: req.query.sort && req.query.sort.split(' ')[1] == 'DESC' ? 1 : -1
                }}
            ], function(err, result) {
                return res.ok(result);
            });
        });
    },

    getCountries: function(req, res) {
        Event.native(function(err, collection) {
            if (err) {
                console.error(err);
                return res.badRequest({message: err.message});
            }

            collection.aggregate([
                { $group : {
                    _id : "$country"
                }},
                {$sort: {
                    count: -1
                }}
            ], function(err, result) {
                Country.find({ code: result.map(function(res) { return res._id; }) }, { select: ['name', 'code']}).sort(req.query.sort ? req.query.sort : '').then(function(countries) {
                    return res.ok(countries);
                });
            });
        });
    },

    vote: function(req, res) {
        req.body.type = 'event';
        return require('../baseControllers/News.js').vote(req, res, Event);
    },

    like: function(req, res) {
        req.body.type = 'event';
        return require('../baseControllers/News.js').like(req, res, Event);
    },

    destroyImg: function(req, res) {
        req.body.type = 'event';
        return require('../baseControllers/News.js').destroyImg(req, res, Event);
    }
};

