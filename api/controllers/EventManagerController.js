/**
 * EventManagerController
 *
 * @description :: Server-side logic for managing Eventmanagers
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    create: function(req, res) {
        var params = req.allParams();
        if(params.related_cities) params.related_cities = JSON.parse(params['related_cities']);

        EventManager.create(params).then(function(user) {
            req.file('avatar').upload(sails.config.custom_config.POST_LIMIT_MB ? {maxBytes: sails.config.custom_config.POST_LIMIT_MB*1000000} : {}, function (err, uploadedFiles) {
                if (err) return res.serverError(err.message);
                if(uploadedFiles.length > 0) {
                    var request = require("request");
                    var path = require('path');
                    var fs = require('fs');

                    var r = request.post({
                        url: sails.config.custom_config.CDN_URL + "/avatar/" + user.id,
                        timeout: 60 * 1000
                    }, function (err, response, body) {
                        if (fs.existsSync(sails.config.custom_config.tmp_file_storage + path.basename(uploadedFiles[0].fd))) {
                            fs.unlinkSync(sails.config.custom_config.tmp_file_storage + path.basename(uploadedFiles[0].fd));
                        }
                        if (err) return res.badRequest(err.message);

                        if(response.statusCode == 200) {
                            // update image to EventManager user
                            EventManager.update({id: user.id}, {avatar: JSON.parse(body).filepath}).exec(function(err, updated_user) {
                                if(err) res.badRequest(err.message);
                                res.ok(updated_user[0]);
                            });
                        } else {
                            res.badRequest(JSON.parse(body).errors[0]);
                        }
                    });

                    var form = r.form();
                    form.append(uploadedFiles[0].filename, fs.createReadStream(sails.config.custom_config.tmp_file_storage + path.basename(uploadedFiles[0].fd)), {filename: path.basename(uploadedFiles[0].fd)});
                } else {
                    res.ok(user);
                }
            });
        }).catch(function(err) {
            res.badRequest(err);
        });
    },

    customFind: function (req, res) {
        var _ = require('lodash');

        req.query.where = JSON.stringify(_.merge({},
            req.query.where ? JSON.parse(req.query.where) : {},
            req.headers.userrole != Admin.role ?
            {status: sails.config.AVAILABLE_USER_STATUSES[1] } : {}
        ));

        return customFind.find(req, EventManager).then(function(result) {
            res.ok(result);
        }).catch(function(error) {
            res.serverError(error);
        });
    },

    update: function(req, res) {
        delete req.body.is_agent; //prevent updating is_agent field
        // remove field if it is empty
        if(req.body.notification_email == "") {
            delete req.body.notification_email;
            var ObjectID = require(sails.config.mongodb_module_path).ObjectID;

            EventManager.native((err, collection) => {
                collection.update(
                    {"_id": new ObjectID(req.param('id'))},
                    {$unset: {notification_email: true}}
                );
            });
        }

        var params = req.allParams();
        if(params.related_cities) params.related_cities = JSON.parse(params['related_cities']);

        EventManager.update({id: req.param('id')}, params).then(function(user) {
            if(!user.length) throw 'No such Event Manager user.';
            user = user[0];
            req.file('avatar').upload(sails.config.custom_config.POST_LIMIT_MB ? {maxBytes: sails.config.custom_config.POST_LIMIT_MB*1000000} : {}, function (err, uploadedFiles) {
                if (err) return res.serverError(err.message);
                if(uploadedFiles.length > 0) {
                    var request = require("request");
                    var path = require('path');
                    var fs = require('fs');

                    var r = request.post({
                        url: sails.config.custom_config.CDN_URL + "/avatar/" + user.id,
                        timeout: 60 * 1000
                    }, function (err, response, body) {
                        if (fs.existsSync(sails.config.custom_config.tmp_file_storage + path.basename(uploadedFiles[0].fd))) {
                            fs.unlinkSync(sails.config.custom_config.tmp_file_storage + path.basename(uploadedFiles[0].fd));
                        }
                        if (err) return res.badRequest(err.message);

                        if(response.statusCode == 200) {
                            // update image to EventManager user
                            EventManager.update({id: user.id}, {avatar: JSON.parse(body).filepath}).exec(function(err, updated_user) {
                                if(err) return res.badRequest(err.message);
                                EventManager.findOne({id: updated_user[0].id}).then(function(user) {
                                    res.ok(user);
                                });
                            });
                        } else {
                            res.badRequest(JSON.parse(body).errors[0]);
                        }
                    });

                    var form = r.form();
                    form.append(uploadedFiles[0].filename, fs.createReadStream(sails.config.custom_config.tmp_file_storage + path.basename(uploadedFiles[0].fd)), {filename: path.basename(uploadedFiles[0].fd)});
                } else {
                    EventManager.findOne({id: user.id}).then(function(user) {
                        res.ok(user);
                    });
                }
            });
        }).catch(function(err) {
            res.badRequest(err);
        });
    }
};

