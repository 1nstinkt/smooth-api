/**
 * CommentController
 *
 * @description :: Server-side logic for managing Comments
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    create(req, res) {
        let {news, event, casting} = req.allParams();
        if(!news && !event && !casting) return res.badRequest('Related entity not defined.');
        Comment.create(req.allParams()).then(created => {
            res.ok(created);
        }).catch(error => {
            res.badRequest(error);
        })
    },

    like: function(req, res) {
        req.body.type = 'comment';
        return require('../baseControllers/News.js').like(req, res, Comment);
    },
};

