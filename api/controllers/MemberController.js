module.exports = {
    /**
     * Find profiles of users with subrole as member or member_pro
     * @param req
     * @param res
     */
    customFind: function (req, res) {
        var User = require('../baseModels/User.js');

        var originalPermitAttr = _.cloneDeep(req.headers.permitAttr);

        req.query.where = this._buildWhere(req, res);

        // Start. Processing subrole
        if (req.headers.permitAttr) {
            for (var f in sails.config.SUB_ROLE_REQUIRED_FIELDS) {
                if (req.headers.permitAttr.indexOf(sails.config.SUB_ROLE_REQUIRED_FIELDS[f]) === -1)
                    req.headers.permitAttr.push(sails.config.SUB_ROLE_REQUIRED_FIELDS[f]);
            }
        }

        return customFind.find(req, Model).then(function (result) {
            // Continue. Processing subrole
            for (var i = 0; i < result.length; i++) {
                result[i].sub_role = User.getSubRole(result[i]);
                if (typeof originalPermitAttr !== 'undefined') {
                    // remove denied fields from response
                    for (var f in sails.config.SUB_ROLE_REQUIRED_FIELDS) {
                        if (originalPermitAttr.indexOf(sails.config.SUB_ROLE_REQUIRED_FIELDS[f]) === -1
                            && result[i][sails.config.SUB_ROLE_REQUIRED_FIELDS[f]]
                        )
                            delete result[i][sails.config.SUB_ROLE_REQUIRED_FIELDS[f]];
                    }
                }
            }
            // End. Processing subrole

            res.ok(result);
        }).catch(function (error) {
            res.serverError(error);
        });
    },

    /**
     * Searching for one member's or member_pro's profile data
     *
     * @param req
     * @param res
     */
    customFindOne: function (req, res) {
        var User = require('../baseModels/User.js');

        var originalPermitAttr = _.cloneDeep(req.headers.permitAttr);

        req.query.where = this._buildWhere(req, res);

        req.query.populateWhere = req.query.populateWhere ? JSON.parse(req.query.populateWhere) : {};
        req.query.populateWhere.friends = _.merge({}, req.query.populateWhere.friends, {"select": req.query.populateWhere.friends && req.query.populateWhere.friends.select ? req.query.populateWhere.friends.select.concat(["id", "name", "avatar"].concat(sails.config.SUB_ROLE_REQUIRED_FIELDS)) : ["id", "name", "avatar"].concat(sails.config.SUB_ROLE_REQUIRED_FIELDS)});
        req.query.populateWhere.as_friend = _.merge({}, req.query.populateWhere.as_friend, {"select": req.query.populateWhere.as_friend && req.query.populateWhere.as_friend.select ? req.query.populateWhere.as_friend.select.concat(["id", "name", "avatar"].concat(sails.config.SUB_ROLE_REQUIRED_FIELDS)) : ["id", "name", "avatar"].concat(sails.config.SUB_ROLE_REQUIRED_FIELDS)});

        req.query.populateWhere = JSON.stringify(req.query.populateWhere);

        // Start. Processing subrole
        if (req.headers.permitAttr) {
            for (var f in sails.config.SUB_ROLE_REQUIRED_FIELDS) {
                if (req.headers.permitAttr.indexOf(sails.config.SUB_ROLE_REQUIRED_FIELDS[f]) === -1)
                    req.headers.permitAttr.push(sails.config.SUB_ROLE_REQUIRED_FIELDS[f]);
            }
        }

        return customFind.findOne(req, Model).then(function (result) {
            // Continue. Processing subrole
            result.sub_role = User.getSubRole(result);
            if (typeof originalPermitAttr !== 'undefined') {
                // remove denied fields from response
                for (var f in sails.config.SUB_ROLE_REQUIRED_FIELDS) {
                    if (originalPermitAttr.indexOf(sails.config.SUB_ROLE_REQUIRED_FIELDS[f]) === -1
                        && result[i][sails.config.SUB_ROLE_REQUIRED_FIELDS[f]]
                    )
                        delete result[sails.config.SUB_ROLE_REQUIRED_FIELDS[f]];
                }
            }
            // End. Processing subrole

            if(result.friends) {
                // Processing subrole for all friends
                for(var f = 0; f < result.friends.length; f++) {
                    result.friends[f].sub_role = User.getSubRole(result.friends[f]);
                    for (var i in sails.config.SUB_ROLE_REQUIRED_FIELDS) {
                        if ((typeof originalPermitAttr === 'undefined' ||
                            originalPermitAttr.indexOf(sails.config.SUB_ROLE_REQUIRED_FIELDS[i]) === -1)
                            && result.friends[f][sails.config.SUB_ROLE_REQUIRED_FIELDS[i]]
                        )
                            delete result.friends[f][sails.config.SUB_ROLE_REQUIRED_FIELDS[i]];
                    }
                }
            }

            if(result.as_friend) {
                // Processing subrole for all invitations for friendship
                for(var f = 0; f < result.as_friend.length; f++) {
                    result.as_friend[f].sub_role = User.getSubRole(result.as_friend[f]);
                    for (var i in sails.config.SUB_ROLE_REQUIRED_FIELDS) {
                        if ((typeof originalPermitAttr === 'undefined' ||
                            originalPermitAttr.indexOf(sails.config.SUB_ROLE_REQUIRED_FIELDS[i]) === -1)
                            && result.as_friend[f][sails.config.SUB_ROLE_REQUIRED_FIELDS[i]]
                        )
                            delete result.as_friend[f][sails.config.SUB_ROLE_REQUIRED_FIELDS[i]];
                    }
                }
            }

            res.ok(result);
        }).catch(function (error) {
            res.serverError(error);
        });
    },
    /**
     * Building request for searching only member and member_pro users
     *
     * @param req
     * @param res
     * @returns {*}
     * @private
     */
    _buildWhere: function(req, res) {
        var _ = require('lodash'),
            requiredOr = Model.buildSearchCriteriaFor('any_member');

        req.query.where = req.query.where ? JSON.parse(req.query.where) : {};

        if(typeof req.query.where.or !== 'undefined') {
            var tmp = [];

            for(var k = 0; k < requiredOr['or'].length; k++) {
                for(var i = 0; i < req.query.where.or.length; i++) {
                    tmp.push(_.merge({}, requiredOr['or'][k], req.query.where.or[i]));
                }
            }
            req.query.where.or = tmp;
        } else {
            req.query.where = _.merge({},
                req.query.where,
                requiredOr
            );
        }

        req.query.where = JSON.stringify(_.merge({}, req.query.where, req.headers.userrole == Admin.role ? {} : {status: sails.config.AVAILABLE_USER_STATUSES[1]}));

        return req.query.where;
    }
};