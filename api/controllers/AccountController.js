module.exports = {
    qw_activate: function (req, res) {
        Model.update({ id: req.param('user_id') }, req.param('update_phone') == 'true' ? { phone: req.param('phone') } : {}).then(function(user) {
            if(!user[0]) return res.badRequest({ message: 'User not found' });
            user = user[0];
            if(!req.param('phone') || !/^\+\d{1,2}\(?\d{3}\)?\d{7}$/.test(req.param('phone'))) return res.badRequest({ message: 'Phone num' });

            var user_id = user.id;
            var bill_id = user_id +'_'+ Date.now(),
                order_lifetime_days = 1,
                successUrl = req.param('success_return_url'),
                failUrl = req.param('fail_return_url');

            var url = sails.config.custom_config.QIWI.API_URL+sails.config.custom_config.QIWI.SHOP_ID+'/bills/'+bill_id,
                request = require('request'),
                querystring = require('querystring');

            var request_data = {headers: {
                "Accept": "text/json",
                "Authorization": 'Basic '+new Buffer( sails.config.custom_config.QIWI.API_ID +':'+ sails.config.custom_config.QIWI.API_PWD ).toString('base64'),
                "Content-Type": "application/x-www-form-urlencoded; charset=utf-8"
            }};

            request_data.url = url;

            var lifetime = new Date();
            lifetime.setHours(lifetime.getHours() + 24 * order_lifetime_days);

            request_data.body = querystring.stringify({
                user: 'tel:'+req.param('phone').replace(/[\(\)]/g, ""),
                amount: sails.config.custom_config.QIWI.member_pro_membership_cost,
                ccy: sails.config.custom_config.QIWI.CURRENCY,
                comment: "Activated Member "+user.email,
                lifetime: lifetime.toISOString(),
                pay_source: 'qw', // 'mobile'
                prv_name: 'payments@smooth.style'
            });

            request.put(request_data, function (err, data) {
                if(err) return res.badRequest(err);

                if(JSON.parse(data.body).response.result_code == 0) {
                    return res.ok({ url: 'https://qiwi.com/order/external/main.action?shop='+sails.config.custom_config.QIWI.SHOP_ID+'&transaction='+bill_id+'&successUrl='+successUrl+'&failUrl='+failUrl+'&iframe=false' });
                }

                res.badRequest({ message: JSON.parse(data.body).response.description });
            });
        }).catch(function(error) {
            res.badRequest(error);
        });
    },
    // {"txn_type":"subscr_signup",
    // "subscr_id":"I-XLBJ1MA9W4T3",
    // "last_name":"buyer",
    // "residence_country":"US",
    // "mc_currency":"USD",
    // "item_name":"Model membership",
    // "business":"payments-facilitator@smooth.style",
    // "amount3":"1.00",
    // "recurring":"1",
    // "verify_sign":"A0asK6oiIZmtBFrRCq-Iiqf8lrk.AiCl5BJL8zlM6LWB1wStplPzPa.D",
    // "payer_status":"verified",
    // "test_ipn":"1",
    // "payer_email":"payments-buyer@smooth.style",
    // "first_name":"test",
    // "receiver_email":"payments-facilitator@smooth.style",
    // "payer_id":"2NJ3XFDRKJHHL",
    // "reattempt":"1",
    // "subscr_date":"02:04:19 Sep 12, 2016 PDT",
    // "custom":"{",
    // "charset":"windows-1252","notify_version":"3.8","period3":"1 Y","mc_amount3":"1.00","ipn_track_id":"a29a3c3630599"}


    // {"transaction_subject":"Model membership",
    // "payment_date":"04:34:04 Sep 12, 2016 PDT",
    // "txn_type":"subscr_payment",
    // "subscr_id":"I-GSXAJ0486UK2",
    // "last_name":"buyer",
    // "residence_country":"US",
    // "item_name":"Model membership",
    // "payment_gross":"1.00",
    // "mc_currency":"USD",
    // "business":"payments-facilitator@smooth.style",
    // "payment_type":"instant",
    // "protection_eligibility":"Ineligible",
    // "verify_sign":"AOtwBs7CKWt6Fc7cseONJwQf7yffAz5yA-AWH0Zp5pR9ArqZDoghlUqD",
    // "payer_status":"verified",
    // "test_ipn":"1",
    // "payer_email":"payments-buyer@smooth.style",
    // "txn_id":"2H6709827H945331B",
    // "receiver_email":"payments-facilitator@smooth.style",
    // "first_name":"test",
    // "payer_id":"2NJ3XFDRKJHHL",
    // "receiver_id":"SMPSZES6ZVUYU",
    // "payment_status":"Completed",
    // "payment_fee":"0.33",
    // "mc_fee":"0.33",
    // "mc_gross":"1.00",
    // "custom":"{\"user_id\":\"5756c0b0a341aca73542f103\",\"service_id\":1}",
    // "charset":"windows-1252",
    // "notify_version":"3.8",
    // "ipn_track_id":"25c6acf7ee956",
    // "cmd":"_notify-validate"}
    ipn: function (req, res) {
        var params = req.allParams();
        if (params.subscr_id && params.receiver_email == sails.config.custom_config.receiver_email) { // subscription actions
            //IPN authentication protocol
            params.cmd = '_notify-validate';

            var request = require("request");
            request.post({
                url: sails.config.custom_config.PAYPAL.API_URL,
                formData: params
            }, function (err, response, body) {
                if (body == 'VERIFIED') {
                    // parse the IPN message
                    var UID = JSON.parse(params.custom).user_id;
                    var SID = JSON.parse(params.custom).service_id;
                    if (UID && SID) {
                        switch (params.txn_type) {
                            case 'subscr_signup':
                                // store subscription data
                                if (SID === 1) {
                                    // membership subscription
                                    Model.findOne({subscr_id: params.subscr_id}).exec(function (err, found) {
                                        if(err) return console.error('Invalid subscr_signup' + err);
                                        if (!found) { // subscription does't exist
                                            Model.update({id: UID}, {
                                                subscr_id: params.subscr_id,
                                                permitCode: sails.config.custom_config.permit_code // for security update
                                            }).exec(function (err, updated) {
                                                if (err) console.error('Invalid updating expiration date. Error: ' + err);
                                            })
                                        }
                                    });
                                } else if (SID == 2) {
                                    // for business subscription
                                    EventManager.findOne({subscr_id: params.subscr_id}).exec(function (err, found) {
                                        if(err) return console.error('Invalid subscr_signup' + err);
                                        if (!found) { // subscription does't exist
                                            EventManager.update({id: UID}, {
                                                subscr_id: params.subscr_id,
                                                permitCode: sails.config.custom_config.permit_code // for security update
                                            }).exec(function (err, updated) {
                                                if (err) console.error('Invalid updating expiration date. Error: ' + err);
                                            })
                                        }
                                    });
                                }
                                break;
                            case 'subscr_payment': // subscription payment
                                if (params.payment_status == 'Completed' && params.mc_currency == 'USD') {
                                    if (SID === 1 || SID === 2) {
                                        // check if transaction is already exists
                                        Transaction.findOne({txn_id: params.txn_id}).exec(function (err, found) {
                                            if (!found) {
                                                params.gateway_type = Transaction.attributes.gateway_type.in[0]; // paypal gateway
                                                var _ = require('lodash');
                                                Transaction.create(_.merge({}, params, {user_id: UID})).then(function (created) {
                                                    if (created) {
                                                        // calculate date for the next payment
                                                        var expiration = new Date(new Date(params.payment_date).setFullYear(new Date(params.payment_date).getFullYear() + 1));
                                                        if (parseFloat(params.mc_gross) == sails.config.custom_config.PAYPAL.member_pro_membership_cost) {
                                                            Model.update({id: UID}, {
                                                                expiration: expiration,
                                                                payment_status: Model.attributes.payment_status.in[1],
                                                                permitCode: sails.config.custom_config.permit_code  // for security update
                                                            }).exec(function (err, updated) {
                                                                if (err) console.error('Invalid updating payment status. Error: ' + err);
                                                            })
                                                        } else if (parseFloat(params.mc_gross) == sails.config.custom_config.business_pro_membership_cost) {
                                                            EventManager.update({id: UID}, {
                                                                expiration: expiration,
                                                                payment_status: Model.attributes.payment_status.in[1],
                                                                permitCode: sails.config.custom_config.permit_code  // for security update
                                                            }).exec(function (err, updated) {
                                                                if (err) console.error('Invalid updating payment status. Error: ' + err);
                                                            })
                                                        }
                                                    }
                                                });
                                            }
                                        })
                                    }
                                }
                                break;
                            case 'subscr_cancel': // cancel subscription
                                Model.native(function(err, collection) {
                                    if (err) console.error(err);
                                    var ObjectID = require(sails.config.mongodb_module_path).ObjectID;

                                    collection.update(
                                        { "_id": new ObjectID(UID), subscr_id: params.subscr_id },
                                        {
                                            $unset: { subscr_id: "" }
                                        },
                                        function(error, result) {
                                            if (error) console.error(error);
                                        });
                                });
                                break;
                            case 'subscr_eot': // subscription is ended
                                if (SID === 1) {
                                    Model.update({id: UID}, {
                                        payment_status: Model.attributes.payment_status.in[0],
                                        permitCode: sails.config.custom_config.permit_code  // for security update
                                    }).exec(function (err, updated) {
                                        if (err) console.error('Invalid updating payment status. Error: ' + err);
                                        else console.log('Subscription for Model user ' + updated.id + ' is ended!')
                                    });
                                } else if (SID == 2) {
                                    EventManager.update({id: UID}, {
                                        payment_status: Model.attributes.payment_status.in[0],
                                        permitCode: sails.config.custom_config.permit_code  // for security update
                                    }).exec(function (err, updated) {
                                        if (err) console.error('Invalid updating payment status. Error: ' + err);
                                        else console.log('Subscription for Event_Manager user ' + updated.id + ' is ended!')
                                    });
                                }
                                break;
                            case 'subscr_modify': // change subscription
                                break;
                            case 'subscr_failed':
                                break;
                        }
                    }
                } else {
                    // incorrect request to mark payment as success in smooth api DB
                    console.error('Invalid IPN: ' + JSON.stringify(params))
                }
            });


        }

        res.ok({});
    },

    /**
     * Convert UTF16 string to UTF8 and then to bytes
     * @param str
     * @returns {string}
     * @private
     */
    _convertUTF16ToUTF8ToByteStr: function (str) {
        var utf8 = unescape(encodeURIComponent(str));

        var byteString = "";
        for (var i = 0; i < utf8.length; i++) {
            byteString += utf8.charCodeAt(i);
        }

        return byteString;
    },
    // paying
    // response
    // {"command":"bill","bill_id":"5756c0b0a341aca73542f103_1487345881875","status":"paid","error":"0","amount":"1.50","user":"tel:+380989659990","prv_name":"payments@smooth.style","ccy":"USD","comment":"test smooth"}
    // headers
    // {"user-agent":"QWClient 1.0 (Java)","accept":"text/xml","content-type":"application/x-www-form-urlencoded","authorization":"Basic NDkzNjEwOlY2ekZxeTJRU3dFRjhjRVpGc1lY","cache-control":"no-cache","pragma":"no-cache","host":"82.117.232.61","connection":"keep-alive","content-length":"179"}

    // rejection
    // response
    // {"command":"bill","bill_id":"5756c0b0a341aca73542f103_1487345356776","status":"rejected","error":"0","amount":"1.50","user":"tel:+380989659990","prv_name":"payments@smooth.style","ccy":"USD","comment":"test smooth"}
    // headers
    // {"user-agent":"QWClient 1.0 (Java)","accept":"text/xml","content-type":"application/x-www-form-urlencoded","authorization":"Basic NDkzNjEwOlY2ekZxeTJRU3dFRjhjRVpGc1lY","cache-control":"no-cache","pragma":"no-cache","host":"82.117.232.61","connection":"keep-alive","content-length":"183"}

    // unpaid
    // {"command":"bill","bill_id":"582476bf76dbff40058b4567_1487662353221","status":"unpaid","error":"0","amount":"1.00","user":"tel:+380989659990","prv_name":"payments@smooth.style","ccy":"RUB","comment":"Smooth membership payment"}

    /**
     * Income payment notifications from Visa QIWI Wallet server
     *
     * @param req
     * @param res
     * @returns {*}
     */
    qw_ipn: function (req, res) {
        var Q = require('q'),
            THIS = this;

        (function (req, res) {
            var deferred = Q.defer();

            var reqParams = req.allParams();

            var UID = reqParams.bill_id ? reqParams.bill_id.split('_')[0] : null,
                payment_date = reqParams.bill_id ? new Date(parseInt(reqParams.bill_id.split('_')[1])) : null,
                txn_id = "txn_" + reqParams.bill_id,
                txn_status = reqParams.status;

            (function() {
                var deferred2 = Q.defer();

                if (typeof req.headers.authorization !== 'undefined') { // Basic authorization
                    if (req.headers.authorization == 'Basic ' + new Buffer(sails.config.custom_config.QIWI.SHOP_ID + ':' + sails.config.custom_config.QIWI.NOTIFICATION_PWD).toString('base64')) {
                        deferred2.resolve();
                    } else {
                        deferred2.reject(150); // Error in password verification
                    }
                } else if (typeof req.headers['x-api-signature'] !== 'undefined') { // digital sign
                    // TODO: code not verified
                    var crypto = require('crypto'),
                        hexHash,
                        signature = req.headers['x-api-signature'],
                        encoded_signature,
                        reqString = "";

                    var sortedIndexes = Object.keys(reqParams).sort(); // sort keys
                    // generate string from values of sorted request
                    for (var i in sortedIndexes) {
                        reqString += "|" + reqParams[sortedIndexes[i]];
                    }

                    reqString = THIS._convertUTF16ToUTF8ToByteStr(reqString.substring(1)); // convert UTF16 string to UTF8 and then to string of bytes

                    hexHash = crypto.createHmac('sha1', THIS._convertUTF16ToUTF8ToByteStr(sails.config.custom_config.QIWI.SHOP_ID)).update(reqString).digest('hex'); // hashed string hexadecimal
                    encoded_signature = new Buffer(THIS._convertUTF16ToUTF8ToByteStr(hexHash)).toString('base64'); // base64 encoded

                    if (encoded_signature == signature) { // compare encoded signature with signature from header
                        deferred2.resolve();
                    } else {
                        deferred2.reject(151); // Error in sign verification
                    }
                }

                return deferred2.promise;
            })().then(function() {
                if(parseFloat(reqParams.amount) !== sails.config.custom_config.QIWI.member_pro_membership_cost) return deferred.resolve(0); // ignore creating transactions for commission

                Transaction.findOne({txn_id: txn_id, payment_status: txn_status}).exec(function (err, found) {
                    if (err) return deferred.reject('Invalid updating payment status. Error: ' + err);

                    (function() {
                        var deferred3 = Q.defer();

                        if (!found) {
                            var params = {
                                txn_id: txn_id,
                                txn_type: reqParams.command, // "bill"
                                mc_gross: reqParams.amount,
                                mc_currency: reqParams.ccy,
                                payment_date: payment_date,
                                payment_status: reqParams.status,
                                business: reqParams.prv_name,
                                receiver_email: reqParams.prv_name,
                                payer_id: reqParams.user,
                                custom: JSON.stringify({error: reqParams.error}),
                                gateway_type: Transaction.attributes.gateway_type.in[1], // qiwi gateway
                                user_id: UID
                            };

                            // first payment
                            Transaction.create(params).then(function (created) {
                                if (created) {
                                    deferred3.resolve();
                                }
                            }).catch(function (err) {
                                if (err) deferred3.reject('Invalid transaction creation. Error: ' + err);
                            });
                        } else {
                            // already exists
                            deferred.resolve(0);
                        }

                        return deferred3.promise;
                    })().then(function() {
                        if (parseFloat(reqParams.amount) == sails.config.custom_config.QIWI.member_pro_membership_cost && reqParams.ccy == sails.config.custom_config.QIWI.CURRENCY) {
                            Model.findOne({id: UID}).then(function (found_user) {
                                if(found_user) {
                                    switch(reqParams.status) {
                                        case 'paid':
                                            var expiration = found_user.expiration && found_user.expiration > new Date() ? new Date(new Date(found_user.expiration).setFullYear(new Date(found_user.expiration).getFullYear() + 1)) : new Date(new Date(payment_date).setFullYear(new Date(payment_date).getFullYear() + 1));
                                            Model.update({id: UID}, {
                                                expiration: expiration,
                                                payment_status: Model.attributes.payment_status.in[1],
                                                permitCode: sails.config.custom_config.permit_code  // for security update
                                            }).exec(function (err, updated) {
                                                if (err) return deferred.reject('Error while updating user payment status. Error: ' + err);
                                                deferred.resolve(0);
                                            });

                                            break;
                                    }
                                } else {
                                    if (err) return deferred.reject('User not found. Error: ' + err);
                                }
                            }).catch(function (err) {
                                if (err) return deferred.reject('Error while searching user. Error: ' + err);
                            });
                        } else {
                            deferred.reject('Not valid currency or payment amount.');
                        }
                    }, function(err) {
                        deferred.reject('Error while transaction creation. Error: ' + err);
                    });
                });
            }, function(err) {
                deferred.reject(err);
            });


            return deferred.promise;
        })(req, res).then(function (result_code) {
            res.setHeader("Content-type", "text/xml");
            var xml = '<?xml version="1.0"?>\
                    <result>\
                    <result_code>' + result_code + '</result_code>\
                    </result>';
            return res.send(xml);
        }, function (error) {
            console.error(error);
            res.setHeader("Content-type", "text/xml");
            var errNum = typeof error == 'number' ? error : 13;
            var xml = '<?xml version="1.0"?>\
                    <result>\
                    <result_code>' + errNum + '</result_code>\
                    </result>';
            return res.send(xml);
        });
    },
    /**
     * Reset password
     *
     * @param req
     * @param res
     * @returns {*}
     */
    reset_pwd: function (req, res) {
        var crypto = require('crypto');
        var randomPwd = crypto.randomBytes(Math.ceil(sails.config.custom_config.pwd_length / 2)).toString('hex').substring(0, sails.config.custom_config.pwd_length);
        randomPwd = randomPwd.charAt(0).toUpperCase() + randomPwd.slice(1);

        Model.findOne({ email: req.param("email") }).then(function(found) {
            if(!found) return res.badRequest({ message: 'Email not found' });
            sails.hooks.email.send("resetPwdEmail_" + req.param("lang"),
                {
                    new_password: randomPwd,
                    user: found
                },
                {
                    to: found.email,
                    subject: req.param('lang') == 'ru' ? "Smooth.style - Ваш новый пароль" : "Smooth.style - your new password"
                },
                function (error, result) {
                    if (error) {
                        console.error(error.message);
                        if (error) return res.badRequest(error);
                    }
                    Model.update({id: found.id}, {
                        password: randomPwd
                    }).exec(function (error, updated) {
                        if (error) return res.badRequest(error);

                        res.ok({message: 'Success'});
                    });
                }
            );
        });
    },
    /**
     * Get payment information about available membership
     *
     * @param req
     * @param res
     */
    get_costs: function(req, res) {
        res.ok({
            costs: {
                member_pro: {
                    paypal: {
                        cost: parseFloat(sails.config.custom_config.PAYPAL.member_pro_membership_cost).toFixed(2),
                        currency: 'USD'
                    },
                    qiwi: {
                        cost: parseFloat(sails.config.custom_config.QIWI.member_pro_membership_cost).toFixed(2),
                        currency: sails.config.custom_config.QIWI.CURRENCY
                    }
                }
            }
        });
    }
};