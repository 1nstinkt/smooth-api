/**
 * CastingController
 *
 * @description :: Server-side logic for managing Castings
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    customFind: function (req, res) {
        return require('../baseControllers/News.js').customFind(req, res, Casting);
    },

    customFindOne(req, res) {
        return require('../baseControllers/News.js').customFindOne(req, res, Casting);
    },
    
    create: function (req, res) {
        return require('../baseControllers/News.js').create(req, res, Casting);
    },

    update: function (req, res) {
        return require('../baseControllers/News.js').update(req, res, Casting);
    },

    destroy: function (req, res) {
        return require('../baseControllers/News.js').destroy(req, res, Casting);
    },

    getCities: function(req, res) {
        Casting.native(function(err, collection) {
            if (err) {
                console.error(err);
                return res.badRequest({message: err.message});
            }

            collection.aggregate([
                { $match: {
                    country: JSON.parse(req.query.where).country
                }},
                { $group : {
                    _id : "$city"
                }},
                {$sort: {
                    _id: req.query.sort && req.query.sort.split(' ')[1] == 'DESC' ? 1 : -1
                }}
            ], function(err, result) {
                return res.ok(result);
            });
        });
    },

    getCountries: function(req, res) {
        Casting.native(function(err, collection) {
            if (err) {
                console.error(err);
                return res.badRequest({message: err.message});
            }

            collection.aggregate([
                { $group : {
                    _id : "$country"
                }},
                {$sort: {
                    count: -1
                }}
            ], function(err, result) {
                Country.find({ code: result.map(function(res) { return res._id; }) }, { select: ['name', 'code']}).sort(req.query.sort ? req.query.sort : '').then(function(countries) {
                    return res.ok(countries);
                });
            });
        });
    },

    vote: function(req, res) {
        req.body.type = 'casting';
        return require('../baseControllers/News.js').vote(req, res, Casting);
    },

    like: function(req, res) {
        req.body.type = 'casting';
        return require('../baseControllers/News.js').like(req, res, Casting);
    },

    destroyImg: function(req, res) {
        req.body.type = 'casting';
        return require('../baseControllers/News.js').destroyImg(req, res, Casting);
    }
};

