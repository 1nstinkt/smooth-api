/**
 * ImageController
 *
 * @description :: Server-side logic for managing Images
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    /**
     * Update image
     *
     * @param req
     * @param res
     * @returns {*}
     */
    update: function(req, res) {
        if(!req.param('user_id')) return res.badRequest('Manager should pass \"user_id\" parameter');
        req.file('image').upload(sails.config.custom_config.POST_LIMIT_MB ? {maxBytes: sails.config.custom_config.POST_LIMIT_MB*1000000} : {}, function (err, uploadedFiles) {
            if (err) return res.serverError(err.message);
            if(uploadedFiles.length > 0) {
                var request = require("request"),
                    path = require('path'),
                    fs = require('fs');

                var r = request.post({
                    url: sails.config.custom_config.CDN_URL + "/gallery/" + req.param('user_id'),
                    timeout: 60 * 1000
                }, function (err, response, body) {
                    if (fs.existsSync(sails.config.custom_config.tmp_file_storage + path.basename(uploadedFiles[0].fd))) {
                        fs.unlinkSync(sails.config.custom_config.tmp_file_storage + path.basename(uploadedFiles[0].fd));
                    }
                    if (err) return res.badRequest(err.message);

                    if(response.statusCode == 200) {
                        res.ok({ message: path.basename(JSON.parse(body).filepath)});
                    } else {
                        res.badRequest(JSON.parse(body).errors[0]);
                    }
                });

                var form = r.form();
                form.append(uploadedFiles[0].filename, fs.createReadStream(sails.config.custom_config.tmp_file_storage + path.basename(uploadedFiles[0].fd)), {filename: req.param('name')});
            } else {
                res.badRequest({ message: 'Image parameter is empty.' });
            }
        });
    },

    /**
     * Create album
     *
     * @param req
     * @param res
     * @returns {*}
     */
    create: function(req, res) {
        if(!req.param('user_id')) return res.badRequest('Manager should pass user_id parameter');
        var Q = require('q');

        Q.all([
            Model.findOne({ id: req.param('user_id') }),
            Album.findOne({ id: req.param('album') })
        ])
            .spread(function(model, album){
                if(!model) return res.badRequest({ message: 'User not found.' });
                if(!album) {
                    // create default album
                    Album.findOne({ owner: model.id, default: true }).then(function(album) {
                        if(!album) {
                            // create default main album
                            Album.create({ name: 'Main', description: 'Main album for gallery images', owner: model.id, default: true, images: [], permitCode: sails.config.custom_config.permit_code }).then(function(album) {
                                req.file('image').upload(sails.config.custom_config.POST_LIMIT_MB ? {maxBytes: sails.config.custom_config.POST_LIMIT_MB*1000000} : {}, function (err, uploadedFiles) {
                                    if (err) return res.serverError(err.message);
                                    if(uploadedFiles.length > 0) {
                                        var request = require("request"),
                                            Q = require('q'),
                                            path = require('path'),
                                            _ = require('lodash'),
                                            fs = require('fs'),
                                            imagesToStore = [];

                                        for(var i in uploadedFiles) {
                                            imagesToStore.push((function() {
                                                var deferred = Q.defer();

                                                var r = request.post({
                                                    url: sails.config.custom_config.CDN_URL + "/gallery/" + model.id,
                                                    timeout: 60 * 1000
                                                }, function (err, response, body) {
                                                    if (fs.existsSync(sails.config.custom_config.tmp_file_storage + path.basename(uploadedFiles[0].fd))) {
                                                        fs.unlinkSync(sails.config.custom_config.tmp_file_storage + path.basename(uploadedFiles[0].fd));
                                                    }
                                                    if(err) return deferred.reject(err);

                                                    if(response.statusCode == 200) {
                                                        // update image to casting
                                                        deferred.resolve({ image: path.basename(JSON.parse(body).filepath) });
                                                    } else {
                                                        deferred.reject(JSON.parse(body).errors[0]);
                                                    }
                                                });

                                                var form = r.form();
                                                form.append(uploadedFiles[i].filename, fs.createReadStream(sails.config.custom_config.tmp_file_storage + path.basename(uploadedFiles[i].fd)), {filename: path.basename(uploadedFiles[i].fd)});

                                                return deferred.promise;
                                            })())
                                        }

                                        Q.allSettled(imagesToStore).then(function(data) {
                                            var addedImages = [];
                                            for(var i in data) {
                                                if(data[i].state == 'rejected') return res.badRequest({message: data[i].reason});
                                                if(!album.images) album.images = [];
                                                album.images.push(data[i].value.image);
                                                addedImages.push(data[i].value.image);
                                            }

                                            Album.update({ id: album.id }, { images: album.images }).then(function(updated) {
                                                res.ok({ message: addedImages.join(',')});
                                            }).catch(function (err) {
                                                res.badRequest(err);
                                            });
                                        })
                                    } else {
                                        res.badRequest({ message: 'Image parameter is empty.' });
                                    }
                                });
                            })
                        } else {
                            // use already created default main album
                            req.file('image').upload(sails.config.custom_config.POST_LIMIT_MB ? {maxBytes: sails.config.custom_config.POST_LIMIT_MB*1000000} : {}, function (err, uploadedFiles) {
                                if (err) return res.serverError(err.message);
                                if(uploadedFiles.length > 0) {
                                    var request = require("request"),
                                        Q = require('q'),
                                        path = require('path'),
                                        _ = require('lodash'),
                                        fs = require('fs'),
                                        imagesToStore = [];

                                    for(var i in uploadedFiles) {
                                        imagesToStore.push((function() {
                                            var deferred = Q.defer();

                                            var r = request.post({
                                                url: sails.config.custom_config.CDN_URL + "/gallery/" + model.id,
                                                timeout: 60 * 1000
                                            }, function (err, response, body) {
                                                if (fs.existsSync(sails.config.custom_config.tmp_file_storage + path.basename(uploadedFiles[0].fd))) {
                                                    fs.unlinkSync(sails.config.custom_config.tmp_file_storage + path.basename(uploadedFiles[0].fd));
                                                }
                                                if(err) return deferred.reject(err);

                                                if(response.statusCode == 200) {
                                                    deferred.resolve({ image: path.basename(JSON.parse(body).filepath) });
                                                } else {
                                                    deferred.reject(JSON.parse(body).errors[0]);
                                                }
                                            });

                                            var form = r.form();
                                            form.append(uploadedFiles[i].filename, fs.createReadStream(sails.config.custom_config.tmp_file_storage + path.basename(uploadedFiles[i].fd)), {filename: path.basename(uploadedFiles[i].fd)});

                                            return deferred.promise;
                                        })())
                                    }

                                    Q.allSettled(imagesToStore).then(function(data) {
                                        var addedImages = [];
                                        for(var i in data) {
                                            if(data[i].state == 'rejected') return res.badRequest({message: data[i].reason});
                                            if(!album.images) album.images = [];
                                            album.images.push(data[i].value.image);
                                            addedImages.push(data[i].value.image);
                                        }

                                        Album.update({ id: album.id }, { images: album.images }).then(function(updated) {
                                            res.ok({ message: addedImages.join(',')});
                                        }).catch(function (err) {
                                            res.badRequest(err);
                                        });
                                    })
                                } else {
                                    res.badRequest({ message: 'Image parameter is empty.' });
                                }
                            });
                        }
                    })

                } else {
                    if(album.name == sails.config.custom_config.PORTFOLIO_ALBUM_NAME) {
                        Promise.all([
                            new Promise((response, reject) => {
                                req.file('image').upload(sails.config.custom_config.POST_LIMIT_MB ? {maxBytes: sails.config.custom_config.POST_LIMIT_MB*1000000} : {}, (err, uploaded) => {
                                    if(err) return reject(err);
                                    response(uploaded);
                                })
                            }),
                            new Promise((response, reject) => {
                                req.file('videos').upload(sails.config.custom_config.POST_LIMIT_MB ? {maxBytes: sails.config.custom_config.POST_LIMIT_MB*1000000} : {}, (err, uploaded) => {
                                    if(err) return reject(err);
                                    response(uploaded);
                                })
                            })
                        ]).then((result) => {
                            let uploadedImages = result[0],
                                uploadedVideos = result[1];

                            let request = require("request"),
                                path = require('path'),
                                fs = require('fs'),
                                storeToCDN = [];

                            uploadedVideos.map((video) => {
                                storeToCDN.push(((video) => {
                                    return new Promise((resolve, reject) => {
                                        let r = request.post({
                                            url: sails.config.custom_config.CDN_URL + "/portfolio_video/" + model.id,
                                            timeout: 60 * 1000
                                        }, (err, response, body) => {
                                            if (fs.existsSync(sails.config.custom_config.tmp_file_storage + path.basename(video.fd))) {
                                                fs.unlinkSync(sails.config.custom_config.tmp_file_storage + path.basename(video.fd));
                                            }
                                            if(err) return reject(err);

                                            if(response.statusCode == 200) {
                                                return resolve({ video: path.basename(JSON.parse(body).filepath) });
                                            } else {
                                                return reject(JSON.parse(body).errors[0]);
                                            }
                                        });

                                        var form = r.form();
                                        form.append(video.filename, fs.createReadStream(sails.config.custom_config.tmp_file_storage + path.basename(video.fd)), {filename: path.basename(video.fd)});
                                    });
                                })(video));
                            });

                            uploadedImages.map((image) => {
                                storeToCDN.push(((image) => {
                                    return new Promise((resolve, reject) => {
                                        let r = request.post({
                                            url: sails.config.custom_config.CDN_URL + "/gallery/" + model.id,
                                            timeout: 60 * 1000
                                        }, (err, response, body) => {
                                            if (fs.existsSync(sails.config.custom_config.tmp_file_storage + path.basename(image.fd))) {
                                                fs.unlinkSync(sails.config.custom_config.tmp_file_storage + path.basename(image.fd));
                                            }
                                            if(err) return reject(err);

                                            if(response.statusCode == 200) {
                                                return resolve({ image: path.basename(JSON.parse(body).filepath) });
                                            } else {
                                                return reject(JSON.parse(body).errors[0]);
                                            }
                                        });

                                        var form = r.form();
                                        form.append(image.filename, fs.createReadStream(sails.config.custom_config.tmp_file_storage + path.basename(image.fd)), {filename: path.basename(image.fd)});
                                    });
                                })(image));
                            });

                            Promise.all(storeToCDN).then(data => {
                                let addedVideos = [],
                                    addedImages = [];

                                data.map((file) => {
                                    if(file.video) {
                                        if(!album.videos) album.videos = [];
                                        album.videos.push(file.video);
                                        addedVideos.push(file.video);
                                    } else if(file.image) {
                                        if(!album.images) album.images = [];
                                        album.images.push(file.image);
                                        addedImages.push(file.image);
                                    }
                                });

                                Album.update({ id: album.id }, { videos: album.videos, images: album.images }).then((updated) => {
                                    res.ok({ message: {videos: addedVideos.join(','), images: addedImages.join(',')}});
                                }).catch( err => {
                                    res.badRequest(err);
                                });
                            }, err => {
                                res.badRequest(err);
                            });

                        }, (error) => {
                            return res.serverError(error.message);
                        });
                    } else {
                        req.file('image').upload(sails.config.custom_config.POST_LIMIT_MB ? {maxBytes: sails.config.custom_config.POST_LIMIT_MB*1000000} : {}, (err, uploadedImages) => {
                            if (err) return res.serverError(err.message);
                            if(uploadedImages.length > 0) {
                                var request = require("request"),
                                    Q = require('q'),
                                    path = require('path'),
                                    _ = require('lodash'),
                                    fs = require('fs'),
                                    imagesToStore = [];

                                for(var i in uploadedImages) {
                                    imagesToStore.push((function(i) {
                                        var deferred = Q.defer();

                                        var r = request.post({
                                            url: sails.config.custom_config.CDN_URL + "/gallery/" + model.id,
                                            timeout: 60 * 1000
                                        }, function (err, response, body) {
                                            if (fs.existsSync(sails.config.custom_config.tmp_file_storage + path.basename(uploadedImages[i].fd))) {
                                                fs.unlinkSync(sails.config.custom_config.tmp_file_storage + path.basename(uploadedImages[i].fd));
                                            }
                                            if(err) return deferred.reject(err);

                                            if(response.statusCode == 200) {
                                                deferred.resolve({ image: path.basename(JSON.parse(body).filepath) });
                                            } else {
                                                return deferred.reject(JSON.parse(body).errors[0]);
                                            }
                                        });

                                        var form = r.form();
                                        form.append(uploadedImages[i].filename, fs.createReadStream(sails.config.custom_config.tmp_file_storage + path.basename(uploadedImages[i].fd)), {filename: path.basename(uploadedImages[i].fd)});

                                        return deferred.promise;
                                    })(i))
                                }

                                Q.allSettled(imagesToStore).then(function(data) {
                                    var addedImages = [];
                                    for(var i in data) {
                                        if(data[i].state == 'rejected') return res.badRequest({message: data[i].reason});
                                        if(!album.images) album.images = [];
                                        album.images.push(data[i].value.image);
                                        addedImages.push(data[i].value.image);
                                    }

                                    Album.update({ id: album.id }, { images: album.images }).then(function(updated) {
                                        res.ok({ message: addedImages.join(',')});
                                    }).catch(function (err) {
                                        res.badRequest(err);
                                    });
                                })
                            }
                        });
                    }
                }
            })
            .catch(function(err){
                res.badRequest(err);
            });
    },

    // remove image from album
    destroy: function(req, res) {
        Album.findOne({ images: req.param('name') }).then(function(album) {
            if(!album) return res.badRequest({ message: 'Image not found.' });
            var request = require("request");

            request.delete({
                url: sails.config.custom_config.CDN_URL + "/" + album.owner + "/gallery/" + req.param('name'),
                timeout: 60 * 1000
            }, function (err, response, body) {
                if(err) return res.badRequest({ message: err.message });
                let images = album.images;
                if(images.indexOf(req.param('name')) !== -1) images.splice(images.indexOf(req.param('name')), 1);
                Album.update({ id: album.id }, { images: images }).then(function(updated) {
                    res.ok({ message: 'Image was completely removed.' });
                }).catch(function (err) {
                    res.badRequest(err);
                });
            });
        });
    },

    // remove all images in album
    clearAll: function(req, res) {
        Album.findOne({ id: req.param('id') }).then(function(album) {
            if(!album) return res.badRequest({ message: 'Album not found.' });
            if(album.images && album.images.length) {
                var Q = require('q'),
                    request = require("request");

                var imagesToRemove = [];

                for(var i in album.images) {
                    imagesToRemove.push((function() {
                        var deferred = Q.defer();

                        request.delete({
                            url: sails.config.custom_config.CDN_URL + "/" + album.owner + "/gallery/" + album.images[i],
                            timeout: 60 * 1000
                        }, function (err, response, body) {
                            if(err) return deferred.reject(err.message);
                            deferred.resolve({ message: 'ok' });
                        });

                        return deferred.promise;
                    })());
                }

                Q.allSettled(imagesToRemove).then(function(data) {
                    // ignore errors if image is already absent
                    Album.update({ id: req.param('id') }, { images: [] }).exec(function (err, updated_album) {
                        if (err) return res.badRequest(err.message);
                        res.ok(updated_album[0]);
                    });
                })
            } else {
                res.ok(album);
            }
        });
    }
};

