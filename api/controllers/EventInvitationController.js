/**
 * EventInvitationController
 *
 * @description :: Server-side logic for managing Eventinvitations
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    customFind: function (req, res) {
        return customFind.find(req, EventInvitation).then(result => {
            result.map((invitation) => {
                if(typeof invitation.model !== 'undefined') invitation.model.sub_role = require('../baseModels/User.js').getSubRole(invitation.model)
            });

            res.ok(result);
        }).catch(function(error) {
            res.serverError(error);
        });
    },

    create: function(req, res) {
        var params = req.allParams();

        EventInvitation.create(params).then(function (eventinvitation) {
            return EventInvitation.find({id: eventinvitation.id}).populate('event');
        }).then(function(result) {
            res.ok(result[0]);
        }).catch(function(error) {
            res.serverError(error);
        });
    }
};

