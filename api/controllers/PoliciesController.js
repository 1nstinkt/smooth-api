/**
 * PoliciesController
 *
 * @description :: Server-side logic for managing Policies
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    _getPolicyArray: function(callback) {
        Policies.native((function(err, collection) {
            collection.find({"defaultSchema": true}).toArray((function(err, found) {
                if(!found.length) return callback(null, 'Default policy scheme is absent.');
                var current_policies = this._clean(found[0]);
                for(var i in current_policies) {
                    for(var k in current_policies[i]) {
                        if(typeof current_policies[i][k] === 'string') {
                            var is_r = [],
                                onlyOwn_r = [],
                                owner_r = [],
                                accessAttr_r = [];

                            var is = current_policies[i][k].match(/is\(\"[a-zA-Z_]+/g);
                            if(is) {
                                for(var a in is) {
                                    is_r.push(is[a].split("(\"")[1])
                                }
                            }

                            var onlyOwn = current_policies[i][k].match(/onlyOwn\(\"[a-zA-Z_]+/g);
                            if(onlyOwn) {
                                for(var a in onlyOwn) {
                                    onlyOwn_r.push(onlyOwn[a].split("(\"")[1])
                                }
                            }

                            var owner = current_policies[i][k].match(/owner\(\"[a-zA-Z_]+/g);
                            if(owner) {
                                for(var a in owner) {
                                    owner_r.push(owner[a].split("(\"")[1])
                                }
                            }

                            var accessAttr = current_policies[i][k].match(/accessAttr\(\'[^\']*/g);
                            if(accessAttr) {
                                for(var a in accessAttr) {
                                    accessAttr_r.push(JSON.parse(accessAttr[a].split("(\'")[1]));
                                }
                            }

                            var tmp = {};
                            if(current_policies[i][k].match(/^or/)) {
                                tmp = {"or": {}};
                                if(is_r && is_r.length) tmp['or']['is'] = is_r;
                                if(onlyOwn_r && onlyOwn_r.length) tmp['or']["onlyOwn"] = onlyOwn_r;
                                if(owner_r && owner_r.length) tmp['or']["owner"] = owner_r;
                                if(accessAttr_r && accessAttr_r.length) tmp['or']["accessAttr"] = accessAttr_r;
                            } else {
                                if(is_r && is_r.length) tmp['is'] = is_r;
                                if(onlyOwn_r && onlyOwn_r.length) tmp["onlyOwn"] = onlyOwn_r;
                                if(owner_r && owner_r.length) tmp["owner"] = owner_r;
                                if(accessAttr_r && accessAttr_r.length) tmp["accessAttr"] = accessAttr_r;
                            }

                            current_policies[i][k] = tmp;
                        }
                    }

                }
                callback(current_policies);
            }).bind(this));
        }).bind(this));
    },

    getPolicy: function (req, res) {
        this._getPolicyArray(function(result) {
            res.ok(result);
        });
    },

    getRolesList: function (req, res) {
        res.ok(sails.config.AVAILABLE_USER_TYPES);
    },

    getRulesList: function (req, res) {
        res.ok(['accessAttr', 'is', 'onlyOwn', 'or', 'owner']);
    },

    updatePolicy: function (req, res) {
        var updates = {},
            controllers = req.allParams(),
            _ = require('lodash');

        for(var controller in controllers) {
            if(typeof controllers[controller] === 'object' ||
                controllers[controller] == "*" ||
                (typeof controllers[controller] === 'boolean' &&
                    ['defaultSchema', 'id', 'updatedAt', 'createdAt'].indexOf(controller) === -1
                )
            ) {
                updates[controller] = {};
                for(var action in controllers[controller]) {
                    if(typeof controllers[controller][action] === 'object') {
                        for(var policy in controllers[controller][action]) {
                            if(policy == 'or') {
                                updates[controller][action] = 'or(';
                                for(var childPolicy in controllers[controller][action][policy]) {
                                    for(var o in controllers[controller][action][policy][childPolicy]) {
                                        if(typeof controllers[controller][action][policy][childPolicy][o] == 'object') {
                                            updates[controller][action] += childPolicy+'(\''+JSON.stringify(controllers[controller][action][policy][childPolicy][o])+'\'),'
                                        } else {
                                            if(sails.config.AVAILABLE_USER_TYPES.indexOf(controllers[controller][action][policy][childPolicy][o]) == -1) return res.badRequest({"message": "User type '"+controllers[controller][action][policy][childPolicy][o]+"' doesn't permit."})
                                            updates[controller][action] += childPolicy+'(\"'+controllers[controller][action][policy][childPolicy][o]+'\"),'
                                        }

                                    }
                                }
                                updates[controller][action] = updates[controller][action].substr(0, updates[controller][action].length-1); // remove last coma
                                updates[controller][action] += ')';
                            } else {
                                updates[controller][action] = policy+'(\"'+controllers[controller][action][policy][0]+'\")';
                            }
                        }
                    } else {
                        updates[controller][action] = controllers[controller][action];
                    }
                }
            }
        }

        Policies.destroy({"defaultSchema": true}).then((function() {
            Policies.create(_.merge({}, updates, {"defaultSchema": true})).exec((function(err, created) {
                if(err) return res.badRequest(err);
                this._generateNewJson(function() {
                    res.ok({"message":"Ok"});
                });
            }).bind(this));
        }).bind(this));
    },

    _generateNewJson: function(callback) {
        var fs = require('fs'),
            shell = require('shelljs');

        switch (process.env.NODE_ENV) {
            case 'production':
                callback();
                // generate policies.json & restart all instances
                shell.exec('sh /home/ec2-user/bin/node.sh "cd /home/smooth.style; sudo NODE_ENV=production node policies.js; sleep 5; sudo NODE_ENV=production /usr/lib/node_modules/pm2/bin/pm2 restart app.js --name api"');
                break;
            case 'development':
                callback();
                // generate policies.json & restart all instances
                shell.exec('NODE_ENV=development node policies.js; sleep 5; sudo NODE_ENV=development /usr/lib/node_modules/pm2/bin/pm2 restart app.js --name api');
                break;
            case 'localhost':
            default:
                callback();
                // generate policies.json & restart server manually
                shell.exec('npm run local_policies');
                break;
        }
    },

    setDefault: function (req, res) {
        this._cleanDefault((function(err) {
            if(err) return res.badRequest(err);
            Policies.update({"id": req.param('id')}, {"defaultSchema": true}).exec((function(err, updated) {
                this._generateNewJson(function() {
                    res.ok({"message":"Ok"});
                });
            }).bind(this));
        }).bind(this))
    },

    unsetDefault: function (req, res) {
        this._cleanDefault(function(err) {
            if(err) return res.badRequest(err);
            res.ok({"message":"Ok"});
        })
    },

    _cleanDefault: function(callback) {
        Policies.native(function(err, collection) {
            collection.update(
                {},
                {$unset: {defaultSchema: ""}},
                function (err, object) {
                    callback(err);
                });
        });
    },

    _clean: function(policies) {
        delete policies.createdAt;
        delete policies.updatedAt;
        delete policies.defaultSchema;
        delete policies._id;
        delete policies.id;
        delete policies.inspect;
        return policies;
    },

    _checkRole: function(roles_permitted, role_to_check) {
        var roles_permitted_extended = [];

        for(var r in roles_permitted) {
            if(sails.config.USER_TYPES_HIERARCHY[roles_permitted[r]] && sails.config.USER_TYPES_HIERARCHY[roles_permitted[r]].length > 0) {
                roles_permitted_extended = roles_permitted_extended.concat(sails.config.USER_TYPES_HIERARCHY[roles_permitted[r]]);
            } else {
                roles_permitted_extended = roles_permitted_extended.concat([roles_permitted[r]]);
            }
        }

        return roles_permitted_extended.indexOf(role_to_check) !== -1;
    },

    getRights: function(req, res) {
        var passport = require('passport');
        var result = {};

        passport.authenticate('any_user-bearer', {session: false}, (function (err, user, info) {
            if(err) return res.badRequest(err);
            var role = 'guest';
            if(user) {
                var User = require('../baseModels/User.js');
                role = User.getSubRole(user);
            }

            if(sails.config.AVAILABLE_USER_TYPES.indexOf(role) === -1) return res.ok(result);

            this._getPolicyArray((function(policy, err) {
                if(err) return res.badRequest({'message': err});
                for(var controller in policy) {
                    for(var operation in policy[controller]) {
                        if(typeof policy[controller][operation] === 'object' && Object.keys(policy[controller][operation])[0] == 'or') {
                            for (var i in policy[controller][operation]['or']) {
                                switch (i) {
                                    case 'accessAttr':
                                        for(var i in policy[controller][operation]['or']['accessAttr']) {
                                            if(this._checkRole(policy[controller][operation]['or']['accessAttr'][i]['roles'], role))
                                                result[controller+'_'+operation] = {'items': 'all', 'fields': policy[controller][operation]['or']['accessAttr'][i]['permitAttr']};
                                        }
                                        break;
                                    case 'is':
                                        if(this._checkRole(policy[controller][operation]['or']['is'], role))
                                            result[controller+'_'+operation] = {'items': 'all', 'fields': 'all'};
                                        break;
                                    case 'onlyOwn':
                                        if(this._checkRole(policy[controller][operation]['or']['onlyOwn'], role))
                                            result[controller+'_'+operation] = {'items': 'own', 'fields': 'all'};
                                        break;
                                    case 'owner':
                                        if(this._checkRole(policy[controller][operation]['or']['owner'], role))
                                            result[controller+'_'+operation] = {'items': 'is_owner', 'fields': 'all'};
                                        break;
                                }
                            }
                        } else if(typeof policy[controller][operation] === 'boolean') {
                            if(policy[controller][operation] === true)
                                result[controller+'_'+operation] = {'items': 'all', 'fields': 'all'};
                        } else {
                            switch (Object.keys(policy[controller][operation])[0]) {
                                case 'accessAttr':
                                    for(var i in policy[controller][operation]['accessAttr']) {
                                        if(this._checkRole(policy[controller][operation]['accessAttr'][i]['roles'], role))
                                            result[controller+'_'+operation] = {'items': 'all', 'fields': policy[controller][operation]['accessAttr'][i]['permitAttr']};
                                    }
                                    break;
                                case 'is':
                                    if(this._checkRole(policy[controller][operation]['is'], role))
                                        result[controller+'_'+operation] = {'items': 'all', 'fields': 'all'};
                                    break;
                                case 'onlyOwn':
                                    if(this._checkRole(policy[controller][operation]['onlyOwn'], role))
                                        result[controller+'_'+operation] = {'items': 'own', 'fields': 'all'};
                                    break;
                                case 'owner':
                                    if(this._checkRole(policy[controller][operation]['owner'], role))
                                        result[controller+'_'+operation] = {'items': 'is_owner', 'fields': 'all'};
                                    break;
                            }
                        }
                    }
                }

                res.ok(result);
            }).bind(this));
        }).bind(this))(req, res);


    },

    getAttributes: function(req, res) {
        var results = {};
        var testFolder = __dirname;
        var fs = require('fs');
        var controllerToModel = {
            Member: 'Model'
        };

        fs.readdir(testFolder, function(err, files) {
            for(var model in files) {
                try {
                    var modelName = files[model].substr(0, files[model].length-13);
                    var attributes = eval(controllerToModel[modelName] ? controllerToModel[modelName]  : modelName).attributes;
                } catch (e) {
                    continue;
                }

                results[modelName.toLowerCase()] = [];
                for(var a in attributes) {
                    results[modelName.toLowerCase()].push(a);
                }
            }

            res.ok(results);

        });
    }
};

