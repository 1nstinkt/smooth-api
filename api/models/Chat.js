/**
 * Chat.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
    schema: true,
    tableName: 'chats',
    attributes: {
        from: {
            model: 'model',
            required: true
        },
        to: {
            model: 'model',
            required: true
        },
        chat_id: {
            type: 'string',
            required: true
        },
        status: {
            type: 'string',
            in: ['unread', 'read'],
            defaultsTo: 'unread',
            required: true
        },
        context: {
            type: 'text'
        },
        readAt: {
            type: 'datetime'
        }
    }
};

