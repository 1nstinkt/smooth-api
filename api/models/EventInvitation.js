/**
 * EventInvitation.js
 *
 * @description :: Invitation of event
 * @docs        :: https://docs.google.com/document/d/1t5U0SXju__9KX8W9TdRnRFfr44Btd8IA8V7E5qribNQ/edit#heading=h.vqzijjawfp5y
 */
var statuses = ["invited", "viewed", "accepted", "declined", "participated", "proposed"];

module.exports = {
    schema: true,
    tableName: 'event_invitations',
    attributes: {
        status: {
            type: 'string',
            in: statuses,
            defaultsTo: 'accepted'
        },
        model: {
            model: 'model',
            required: true
        },
        event: {
            model: 'event',
            required: true
        },
        send: {
            type: 'boolean',
            defaultsTo: false
        }
    },
    indexes: [
        {
            attributes: {
                model: 1,
                event: 1
            },
            options: {
                unique: true
            }
        }
    ],

    afterCreate: function(values, cb) {
        var Promise = require('q');
        Promise.all([
            Model.findOne({id: values.model}),
            Event.findOne({id: values.event}).populate('owner')
        ])
            .spread(function(model, event){
                if (!model) return cb(new Error('Selected user not found.'));
                if (!event) return cb(new Error('Selected event not found.'));

                if(event.owner && (event.owner.notification_email || event.owner.email)) {
                    sails.hooks.email.send("eventNotificationEmail",
                        {
                            event_title: event.title,
                            model: {
                                name: model.nickname,
                                city: model.city,
                                country: model.country,
                                email: model.email,
                                phone: model.phone,
                                profile_link: sails.config.custom_config.site_url+'/models/'+model.id

                            }
                        },
                        {
                            to: event.owner.notification_email ? event.owner.notification_email : event.owner.email,
                            //to: 'yura.test.2014@gmail.com',
                            subject: "Event \"" + event.title + "\" notification"
                        },
                        function (error, result) {
                            if (error) {
                                console.log(error.message);
                                return cb(error);
                            }
                            EventInvitation.update({model: values.model, event: values.event}, {send: true}).exec(function(err, result) {
                                if(err) return cb(err);
                                cb();
                            });
                        });
                } else {
                    cb();
                }
            })
            .catch(function(error){
                console.log(error.message);
                cb(error);
            });
    }
};

