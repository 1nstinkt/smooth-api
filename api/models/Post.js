/**
 * Wall.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
    schema: true,
    tableName: 'posts',
    types: {
        is_repost: function(value) {
            var success = true;
            if(this.type !== Post.attributes.type.in[1]) success = false;

            return success;
        },
        is_comment: function(value) {
            var success = true;
            if(this.type !== Post.attributes.type.in[2]) success = false;

            return success;
        }
    },

    validationMessages: {
        original_post: {
            is_repost: 'Field \"original_post\" relates for Post with field \"type\" = \"repost\".'
        },
        parent_post: {
            is_comment: 'Field \"parent_post\" relates for Post with field \"type\" = \"comment\".'
        }
    },

    attributes: {
        content: {
            type: 'text'
        },
        /**
         * Whom the post relates to
         */
        user_id: {
            model: 'model',
            required: true
        },
        /**
         * Who was created the post
         */
        sender_id: {
            model: 'model',
            required: true
        },
        images: {
            type: 'array'
        },
        youtube_link: {
            type: 'string'
        },
        map: {
            type: 'array'
        },
        status: {
            type: 'string',
            in: ['active', 'removed', 'hidden'],
            defaultsTo: "active"
        },
        type: {
            type: 'text',
            in: ['post', 'repost', 'comment'],
            defaultsTo: "post"
        },
        // parent post if type is repost
        original_post: {
            collection: 'post',
            is_repost: true,
            via: 'reposts'
        },
        // child reposts
        reposts: {
            collection: 'post',
            via: 'original_post',
            dominant: true
        },
        // in case if type is comment this field must be fulfilled
        parent_post: {
            model: 'post',
            is_comment: true
        },
        likes: {
            collection: 'model',
            via: 'posts_liked'
        },
        comments: {
            collection: 'post',
            via: 'parent_post'
        }
    }
};

