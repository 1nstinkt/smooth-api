/**
 * Event.js
 *
 * @description :: Meeting events
 * @docs        :: https://docs.google.com/document/d/1t5U0SXju__9KX8W9TdRnRFfr44Btd8IA8V7E5qribNQ/edit#heading=h.qkcmnb6to96d
 */
var News = require('../baseModels/News.js');
var _ = require('lodash');

module.exports = _.merge({}, News, {
    tableName: 'events',
    attributes: {
        contact_info: {
            type: 'text',
            required: true
        },
        invitations: {
            collection: 'eventinvitation',
            via: 'event'
        },
        rating: {
            collection: 'vote',
            via: 'event'
        },
        likes: {
            collection: 'like',
            via: 'event'
        },
        comments: {
            collection: 'comment',
            via: 'event'
        }
    }
});

