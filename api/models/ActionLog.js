/**
 * ActionLog.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
    attributes: {
        type: {
            type: 'string',
            required: true,
            in: ['add_friend', 'remove_friend', 'reject_invitation', 'rm_all_friends', 'reject_all_invitations', 'create_child_registration']
        },
        owner: {
            model: 'model'
        },
        details: {
            type: 'json'
        }
    }
};

