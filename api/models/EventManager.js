/**
 * EventManager.js
 *
 * @description :: Model of event manager
 * @docs        :: https://docs.google.com/document/d/1t5U0SXju__9KX8W9TdRnRFfr44Btd8IA8V7E5qribNQ/edit#heading=h.gzpo1n3n42yh
 */

var User = require('../baseModels/User.js');
var _ = require('lodash');

module.exports = _.merge({}, User, {
    schema: true,
    tableName: 'event_managers',
    role: 'event_manager',
    attributes: {
        is_agent: {
            type: 'boolean',
            defaultsTo: false,
            required: true
        },
        phone: {
            type: 'string'
        },
        country: {
            type: 'string'
        },
        city: {
            type: 'string'
        },
        address: {
            type: 'string'
        },
        avatar: {
            type: 'string'
        },
        events: {
            collection: 'event',
            via: 'owner'
        },
        castings: {
            collection: 'casting',
            via: 'owner'
        },
        // related cities
        related_cities: {
            collection: 'city',
            via: 'event_managers',
            dominant: true
        },
        children: {
            collection: 'model',
            via: 'parent'
        },
        notification_email: {
            type: 'string'
        }
    },
    indexes: [
        {
            attributes: {
                notification_email: 1
            },
            options: {
                unique: true,
                sparse: true
            }
        }
    ]
});

