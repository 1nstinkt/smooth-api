/**
* Admin.js
*
* @description :: Admin model
* @docs        :: https://docs.google.com/document/d/1t5U0SXju__9KX8W9TdRnRFfr44Btd8IA8V7E5qribNQ/edit#heading=h.eg2geig5gkw
*/
var User = require('../baseModels/User.js');
var _ = require('lodash');

module.exports = _.merge({}, User, {
    tableName: 'admins',
    role: 'admin'
});

