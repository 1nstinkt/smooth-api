/**
 * News.js
 *
 * @description :: News
 * @docs        :: https://docs.google.com/document/d/1t5U0SXju__9KX8W9TdRnRFfr44Btd8IA8V7E5qribNQ/edit#heading=h.8nym0l37griz
 */
var News = require('../baseModels/News.js');
var _ = require('lodash');

module.exports = _.merge({}, News, {
    tableName: 'news',
    attributes: {
        rating: {
            collection: 'vote',
            via: 'news'
        },
        likes: {
            collection: 'like',
            via: 'news'
        },
        comments: {
            collection: 'comment',
            via: 'news'
        }
    }
});

