/**
 * Vote.js
 *
 * @description :: Votes for news (Castings, News, Events)
 * @docs        :: https://docs.google.com/document/d/1t5U0SXju__9KX8W9TdRnRFfr44Btd8IA8V7E5qribNQ/edit#heading=h.up0b23fxh4zo
 */

module.exports = {
    tableName: 'votes',
    attributes: {
        mark: {
            type: 'integer',
            in: [0, 1, 2, 3, 4, 5],
            required: true
        },
        news: {
            model: 'news'
        },
        casting: {
            model: 'casting'
        },
        event: {
            model: 'event'
        },
        user: {
            model: 'model'
        }
    },
    indexes: [
        {
            attributes: {
                event: 1
            },
            options: {
                sparse: true
            }
        },
        {
            attributes: {
                casting: 1
            },
            options: {
                sparse: true
            }
        },
        {
            attributes: {
                news: 1
            },
            options: {
                sparse: true
            }
        }
    ]
};

