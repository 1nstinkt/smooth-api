/**
 * CastingInvitation.js
 *
 * @description :: Invitation on casting
 * @docs        :: https://docs.google.com/document/d/1t5U0SXju__9KX8W9TdRnRFfr44Btd8IA8V7E5qribNQ/edit#heading=h.kmnsaahh1g5v
 */
var statuses = ["invited", "viewed", "accepted", "declined", "participated", "proposed"];

module.exports = {
    schema: true,
    tableName: 'casting_invitations',
    attributes: {
        status: {
            type: 'string',
            in: statuses,
            defaultsTo: 'accepted'
        },
        model: {
            model: 'model',
            required: true
        },
        casting: {
            model: 'casting',
            required: true
        },
        send: {
            type: 'boolean',
            defaultsTo: false
        }
    },
    indexes: [
        {
            attributes: {
                model: 1,
                casting: 1
            },
            options: {
                unique: true
            }
        }
    ],

    afterCreate: function (values, cb) {
        var Promise = require('q');
        Promise.all([
            Model.findOne({id: values.model}),
            Casting.findOne({id: values.casting}).populate('owner')
        ])
            .spread(function (model, casting) {
                if (!model) return cb(new Error('Selected user not found.'));
                if (!casting) return cb(new Error('Selected casting not found.'));

                if(casting.owner && (casting.owner.notification_email || casting.owner.email)) {
                    sails.hooks.email.send("castingNotificationEmail",
                        {
                            casting_title: casting.title,
                            model: {
                                name: model.nickname,
                                city: model.city,
                                country: model.country,
                                email: model.email,
                                phone: model.phone,
                                profile_link: sails.config.custom_config.site_url+'/models/'+model.id

                            }
                        },
                        {
                            to: casting.owner.notification_email ? casting.owner.notification_email : casting.owner.email,
                            //to: 'yura.test.2014@gmail.com',
                            subject: "Casting \"" + casting.title + "\" notification"
                        },
                        function (error, result) {
                            if (error) {
                                console.log(error.message);
                                return cb(error);
                            }
                            CastingInvitation.update({
                                model: values.model,
                                casting: values.casting
                            }, {send: true}).exec(function (err, result) {
                                if (err) return cb(err);
                                cb();
                            });
                        });
                } else {
                    cb();
                }
            })
            .catch(function (error) {
                console.log(error.message);
                cb(error);
            });
    }
};

