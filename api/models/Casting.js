/**
 * Casting.js
 *
 * @description :: Casting events
 * @docs        :: https://docs.google.com/document/d/1t5U0SXju__9KX8W9TdRnRFfr44Btd8IA8V7E5qribNQ/edit#heading=h.2sx593fkjn64
 */
var News = require('../baseModels/News.js');
var _ = require('lodash');

module.exports = _.merge({}, News, {
    tableName: 'castings',
    attributes: {
        contact_info: {
            type: 'text',
            required: true
        },
        invitations: {
            collection: 'castinginvitation',
            via: 'casting'
        },
        rating: {
            collection: 'vote',
            via: 'casting'
        },
        likes: {
            collection: 'like',
            via: 'casting'
        },
        comments: {
            collection: 'comment',
            via: 'casting'
        }
    }
});

