/**
 * City.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
    schema: true,
    tableName: 'cities',
    attributes: {
        name: {
            type: 'string',
            required: true,
            unique: true
        },
        event_managers: {
            collection: 'eventmanager',
            via: 'related_cities'
        }
    }
};

