/**
 * Album.js
 *
 * @description :: https://docs.google.com/document/d/1t5U0SXju__9KX8W9TdRnRFfr44Btd8IA8V7E5qribNQ/edit#heading=h.grx3ggew5dxh
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
    schema: true,
    tableName: 'albums',
    attributes: {
        name: {
            type: 'string',
            required: true
        },
        description: {
            type: 'text'
        },
        images: {
            type: 'array'
        },
        videos: {
            type: 'array'
        },
        owner: {
            model: 'model',
            required: true
        },
        default: {
            type: 'boolean',
            defaultsTo: false
        }
    },
    indexes: [
        {
            attributes: {
                name: 1,
                owner: 1
            },
            options: {
                unique: true
            }
        }
    ],

    beforeCreate: function(values, cb) {
        if(values.default && values.permitCode !== sails.config.custom_config.permit_code) values.default = this.attributes.default.defaultsTo;

        cb();
    },

    beforeUpdate: function(values, cb) {
        if(values.default && values.permitCode !== sails.config.custom_config.permit_code) values.default = this.attributes.default.defaultsTo;

        cb();
    }
};

