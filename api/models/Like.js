/**
 * Like.js
 *
 * @description :: User likes to news, castings, events, comments to news entities
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
    tableName: 'likes',
    attributes: {
        news: {
            model: 'news'
        },
        casting: {
            model: 'casting'
        },
        event: {
            model: 'event'
        },
        comment: {
            model: 'comment'
        },
        user: {
            model: 'model',
            required: true
        }
    },
    indexes: [
        {
            attributes: {
                event: 1
            },
            options: {
                sparse: true
            }
        },
        {
            attributes: {
                casting: 1
            },
            options: {
                sparse: true
            }
        },
        {
            attributes: {
                news: 1
            },
            options: {
                sparse: true
            }
        },
        {
            attributes: {
                comment: 1
            },
            options: {
                sparse: true
            }
        }
    ]
};

