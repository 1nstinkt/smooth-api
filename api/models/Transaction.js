/**
 * Transaction.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {
    schema: true,
    tableName: 'transactions',
    attributes: {
        txn_id: {
            type: 'string',
            unique: true
        },
        txn_type: {
            type: 'string'
        },
        mc_gross: {
            type: 'float'
        },
        mc_currency: {
            type: 'string'
        },
        payment_date: {
            type: 'datetime'
        },
        payment_status: {
            type: 'string'
        },
        business: {
            type: 'string'
        },
        receiver_email: {
            type: 'string'
        },
        payer_id: {
            type: 'string'
        },
        payer_email: {
            type: 'string'
        },
        subscr_id: {
            type: 'string'
        },
        custom: {
            type: 'string'
        },
        gateway_type: {
            type: 'string',
            in: ['paypal', 'qiwi'],
            defaultsTo: 'paypal'
        },
        // related user
        user_id: {
            model: 'model'
        }
    }
};

