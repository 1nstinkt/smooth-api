/**
 * Policies.js
 *
 * @description :: Policies
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
    tableName: 'policies',
    schema: false,
    attributes: {
        "defaultSchema": {
            type: 'boolean'
        }
    },
    indexes: [
        {
            attributes: {
                defaultSchema: 1
            },
            options: {
                unique: true,
                sparse: true
            }
        }
    ]
};

