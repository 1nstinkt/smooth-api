/**
 * Comment.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
    tableName: 'comments',
    attributes: {
        user: {
            model: 'model',
            required: true
        },
        news: {
            model: 'news'
        },
        casting: {
            model: 'casting'
        },
        event: {
            model: 'event'
        },
        likes: {
            collection: 'like',
            via: 'comment'
        },
        content: {
            type: 'text',
            required: true
        }
    },

    afterDestroy(destroyedRecords, cb) {
        if(destroyedRecords.length) {
            // destroying all comment's likes
            Like.destroy({comment: destroyedRecords.map(record => { return record.id })}).then(() => {
                cb();
            });
        } else {
            cb();
        }
    }
};

