/**
 * Tariff.js
 *
 * @description :: Available tariffs
 * @docs        :: https://docs.google.com/document/d/1t5U0SXju__9KX8W9TdRnRFfr44Btd8IA8V7E5qribNQ/edit#heading=h.xg4qn2tyg4s0
 */

module.exports = {
    schema: true,
    tableName: 'tariffs',
    attributes: {
        alias: {
            type: 'string',
            required: true,
            unique: true
        },
        name: {
            type: 'json'
        },
        price: {
            type: 'float',
            defaultsTo: 0.00
        },
        details: {
            type: 'json'
        },
        config: {
            type: 'json'
        },
        status: {
            type: 'string',
            in: ['active', 'inactive'],
            defaultsTo: 'inactive'
        }
    }
};
