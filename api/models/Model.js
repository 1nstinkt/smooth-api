/**
* Model.js
*
* @description :: Model of model
* @docs        :: https://docs.google.com/document/d/1t5U0SXju__9KX8W9TdRnRFfr44Btd8IA8V7E5qribNQ/edit#heading=h.29za0vguy1ij
*/
var User = require('../baseModels/User.js');
var _ = require('lodash');

module.exports = _.merge({}, User, {
    tableName: 'models',
    role: 'model',

    attributes: {
        surname: {
            type: 'string',
            required: true
        },
        nickname: {
            type: 'string'
        },
// ========== CONTACTS ============
        phone: {
            type: 'string'
        },
        address: {
            type: 'string'
        },
        zip: {
            type: 'string'
        },
//=========== FIRST PAGE ================
        country: {
            type: 'string'
        },
        city: {
            type: 'string',
            required: true
        },
        gender: {
            type: 'string',
            in: ['male', 'female']
        },
        avatar: {
            type: 'string'
        },
//============= PROFILE =================
        dob: {
            type: 'string'
        },
        marital_status: {
            type: 'string',
            in: ['married', 'single', 'divorced'],
            defaultsTo: "single"
        },
        education: {
            type: 'string'
        },
        curr_work: {
            type: 'string'
        },
        experience: {
            type: 'string'
        },
        photosessions_count: {
            type: 'integer'
        },
        fb_link: {
            type: 'string'
        },
        vk_link: {
            type: 'string'
        },
        ok_link: {
            type: 'string'
        },
        twitter_link: {
            type: 'string'
        },
        interests: {
            type: 'string'
        },
        shortDesc: {
            type: 'string'
        },
//=========== LIST OF VISITED EVENTS ==========
        event_invitations: {
            collection: 'eventinvitation',
            via: 'model'
        },
//=========== LIST OF VISITED CASTINGS ==========
        casting_invitations: {
            collection: 'castinginvitation',
            via: 'model'
        },
        expiration: {
            type: 'datetime',
            defaultsTo: null
        },
        // user marked as model
        is_model: {
            type: 'boolean',
            defaultsTo: false,
            required: true
        },
        // user marked as paid member
        payment_status: {
            type: 'string',
            in: ['unpaid', 'paid'],
            defaultsTo: 'unpaid'
        },
        /**
         * ID of Paypal subscription
         */
        subscr_id: {
            type: 'string'
        },
        albums: {
            collection: 'album',
            via: 'owner'
        },
        // parent event_manager
        parent: {
            model: 'eventmanager'
        },
        posts: {
            collection: 'post',
            via: 'user_id'
        },
        posts_liked: {
            collection: 'post',
            via: 'likes'
        },
        likes: {
            collection: 'like',
            via: 'user'
        },
        // users that are friends
        friends: {
            collection: 'model',
            via: 'as_friend',
            dominant: true
        },
        // friend of another users
        as_friend: {
            collection: 'model',
            via: 'friends'
        },
        // related chat messages
        send_messages: {
            collection: 'chat',
            via: 'from'
        },
        received_messages: {
            collection: 'chat',
            via: 'to'
        },
        payment_transactions: {
            collection: 'transaction',
            via: 'user_id'
        },
        votes: {
            collection: 'vote',
            via: 'user'
        },
        worksheet: {
            collection:'worksheet',
            via: 'user'
        }
    },
    indexes: [
        {
            attributes: {
                subscr_id: 1
            },
            options: {
                unique: true,
                sparse: true
            }
        },
        {
            attributes: {
                nickname: 1
            },
            options: {
                unique: true,
                sparse: true
            }
        }
    ],

    beforeValidate: function(values, cb) {
        if (typeof dirtyDataValidate == 'undefined') {
            dirtyDataValidate = true;

            var Errors = {},
                _ = require('lodash');

            if (values.dob) {
                if (new Date(values.dob) == 'Invalid Date') {
                    Errors = _.merge({}, Errors, {
                        Errors: {dob: [{rule: 'Date in YYYY-MM-DD format need.'}]},
                        code: 'E_VALIDATION',
                        invalidAttributes: {dob: [{rule: 'Date in YYYY-MM-DD format need.'}]}
                    });
                }
            }

            if (values.phone) {
                if (!/\d|[\(\)\+]/.test(values.phone)) {
                    Errors = _.merge({}, Errors, {
                        Errors: {phone: [{rule: 'phone_num'}]},
                        code: 'E_VALIDATION',
                        invalidAttributes: {phone: [{rule: 'phone_num'}]}
                    });
                }
            }

            if(typeof Errors['Errors'] !== 'undefined') return cb(Errors);
            cb();
        } else {
            dirtyDataValidate = true;
            cb();
        }
    },

    /**
     * Build search criteria for searching users with different sub_role
     *
     * @param sub_role
     * @returns {*}
     */
    buildSearchCriteriaFor: function(sub_role) {
        if(!sub_role) sub_role = 'model';

        switch (sub_role) {
            case 'any_member':
                return {"or":[
                    {payment_status: Model.attributes.payment_status.in[0]}, // manually expired user
                    {expiration: null}, // new user
                    {payment_status: Model.attributes.payment_status.in[1], expiration: {'<': new Date()}}, // expired user
                    {payment_status: Model.attributes.payment_status.in[1], expiration: {'>': new Date()}, is_model: false} // member pro
                ]};
                break;
            case 'member_pro':
                return {payment_status: Model.attributes.payment_status.in[1], expiration: {'>': new Date()}, is_model: false}; // member pro
                break;
            case 'model':
                return {
                    is_model: true,
                    payment_status: sails.config.AVAILABLE_PAYMENT_STATUSES[1],
                    expiration: {'>': new Date()}
                };
                break;
        }
    }
});

