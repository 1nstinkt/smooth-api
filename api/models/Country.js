/**
 * Country.js
 *
 * @description :: Country
 * @docs        ::
 */

module.exports = {
    schema: true,
    tableName: 'countries',
    attributes: {
        name: {
            type: 'text',
            required: true
        },
        code: {
            type: 'text',
            required: true
        }
    }
};

