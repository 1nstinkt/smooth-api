/**
 * UserManager.js
 *
 * @description :: https://docs.google.com/document/d/1t5U0SXju__9KX8W9TdRnRFfr44Btd8IA8V7E5qribNQ/edit#heading=h.niourwe1181e
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

var User = require('../baseModels/User.js');
var _ = require('lodash');

module.exports = _.merge({}, User, {
  tableName: 'user_managers',
  role: 'user_manager',
  attributes: {

  }
});