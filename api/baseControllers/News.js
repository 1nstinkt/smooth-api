module.exports = {
    /**
     * Method for creating of all entities, related to News - News, Castings, Events
     *
     * @param req
     * @param res
     * @param Model
     */
    create: function(req, res, Model) {
        delete req.body.rating; // deny set rating manually

        Model.create(req.allParams()).then(function (entity) {
            req.file('images').upload(sails.config.custom_config.POST_LIMIT_MB ? {maxBytes: sails.config.custom_config.POST_LIMIT_MB*1000000} : {}, function (err, uploadedFiles) {
                if (err) return res.serverError(err.message);
                if (uploadedFiles.length > 0) {
                    var request = require("request"),
                        path = require('path'),
                        fs = require('fs'),
                        Q = require('q'),
                        waitFor = [];

                    for(var i = 0; i < uploadedFiles.length; i++) {
                        waitFor.push((function(i) {
                            var deferred = Q.defer();

                            var r = request.post({
                                url: sails.config.custom_config.CDN_URL + "/gallery/" + entity.id,
                                timeout: 60 * 1000
                            }, function (err, response, body) {
                                if (fs.existsSync(sails.config.custom_config.tmp_file_storage + path.basename(uploadedFiles[i].fd))) {
                                    fs.unlinkSync(sails.config.custom_config.tmp_file_storage + path.basename(uploadedFiles[i].fd));
                                }
                                if (err) return deferred.reject(err.message);

                                if(response.statusCode == 200) {
                                    deferred.resolve(JSON.parse(body).filepath);
                                } else {
                                    deferred.reject(JSON.parse(body).errors[0]);
                                }
                            });

                            var form = r.form();
                            form.append(uploadedFiles[i].filename, fs.createReadStream(sails.config.custom_config.tmp_file_storage + path.basename(uploadedFiles[i].fd)), {filename: path.basename(uploadedFiles[i].fd)});
                            return deferred.promise;
                        })(i))
                    }

                    Q.allSettled(waitFor).then(function(data) {
                        var images = [];
                        for(var k = 0; k < data.length; k++) {
                            if(data[k].state == 'rejected') return res.badRequest({message: data[k].reason});
                            images.push(data[k].value);
                        }
                        // update image to news
                        Model.update({id: entity.id}, {images: images}).exec(function (err, updated_news) {
                            if (err) return res.badRequest(err.message);
                            res.ok(updated_news[0]);
                        });
                    });
                } else {
                    res.ok(entity);
                }
            });
        }).catch(function (err) {
            res.badRequest(err);
        });
    },

    /**
     * Update entity
     *
     * @param req
     * @param res
     * @param Model
     */
    update(req, res, Model) {
        delete req.body.rating; // deny set rating manually

        Model.update({id: req.param('id')}, req.allParams()).then(function (entity) {
            if (!entity.length) throw 'No record found with the specified `id`.';
            entity = entity[0];

            req.file('images').upload(sails.config.custom_config.POST_LIMIT_MB ? {maxBytes: sails.config.custom_config.POST_LIMIT_MB*1000000} : {}, function (err, uploadedFiles) {
                if (err) return res.serverError(err.message);
                if (uploadedFiles.length > 0) {
                    var request = require("request"),
                        path = require('path'),
                        fs = require('fs'),
                        Q = require('q'),
                        waitFor = [];

                    for(var i = 0; i < uploadedFiles.length; i++) {
                        waitFor.push((function(i) {
                            var deferred2 = Q.defer();

                            var r = request.post({
                                url: sails.config.custom_config.CDN_URL + "/gallery/" + entity.id,
                                timeout: 60 * 1000
                            }, function (err, response, body) {
                                if (fs.existsSync(sails.config.custom_config.tmp_file_storage + path.basename(uploadedFiles[i].fd))) {
                                    fs.unlinkSync(sails.config.custom_config.tmp_file_storage + path.basename(uploadedFiles[i].fd));
                                }

                                if (err) return deferred2.reject(err.message);

                                if(response.statusCode == 200) {
                                    deferred2.resolve(JSON.parse(body).filepath)
                                } else {
                                    deferred2.reject(JSON.parse(body).errors[0]);
                                }

                            });

                            var form = r.form();
                            form.append(uploadedFiles[i].filename, fs.createReadStream(sails.config.custom_config.tmp_file_storage + path.basename(uploadedFiles[i].fd)), {filename: path.basename(uploadedFiles[i].fd)});

                            return deferred2.promise;
                        })(i))
                    }

                    Q.allSettled(waitFor).then(function(data) {
                        for(var k = 0; k < data.length; k++) {
                            if(data[k].state == 'rejected') return res.badRequest({message: data[k].reason});
                            if(typeof entity.images == 'undefined') entity.images = [];
                            entity.images.push(data[k].value);
                        }

                        Model.update({id: entity.id}, {images: entity.images}).exec(function (err, updated_entity) {
                            if(err) return res.badRequest(err.message);
                            res.ok(updated_entity[0]);
                        });
                    });
                } else {
                    res.ok(entity);
                }
            });
        }).catch(function (err) {
            res.badRequest(err);
        });
    },

    /**
     * Remove, related to News, entity and it's images
     *
     * @param req
     * @param res
     * @param Model
     */
    destroy(req, res, Model) {
        Model.findOne({ id: req.param('id') }).then(entity => {
            if (!entity) throw 'No record found with the specified `id`.';
            var Q = require('q'),
                request = require("request");

            (function() {
                var deferred = Q.defer();

                if(entity.images && entity.images.length > 0) {
                    request.delete({
                        url: sails.config.custom_config.CDN_URL + "/clear_gallery/" + entity.id,
                        timeout: 60 * 1000
                    }, function (err, response, body) {
                        if (err) return deferred.reject(err.message);

                        if(response.statusCode == 200) {
                            deferred.resolve(true)
                        } else {
                            try {
                                deferred.reject(JSON.parse(body).errors[0]);
                            } catch (e) {
                                deferred.reject(response.statusMessage);
                            }
                        }
                    });
                } else {
                    deferred.resolve(true);
                }

                return deferred.promise;
            })().then(function() {
                Model.destroy({ id: req.param('id') }).then(function(removed) {
                    res.ok(removed[0]);
                });
            },function(error) {
                res.badRequest({message: error});
            });
        }).catch(function (err) {
            res.badRequest(err);
        });
    },

    /**
     * Change rating for news
     * @param req
     * @param res
     * @param Model
     */
    vote(req, res, Model) {
        if(typeof req.param('rate') == 'undefined') return res.badRequest({ message: 'Rating is not defined.' });
        var rate = parseFloat(req.param('rate'));
        if([0, 1, 2, 3, 4, 5].indexOf(rate) === -1) return res.badRequest({ message: 'Rating is out of range.' });

        Model.findOne({ id: req.param('id') }).populate('rating').then(found => {
            if(!found) throw 'No record found with the specified `id`.';

            for(var i = 0; i < found.rating.length; i++) {
                if(found.rating[i].user == req.headers.uid) return Vote.update({ id: found.rating[i].id }, {mark: rate});
            }

            const insert = {
                user: req.headers.uid,
                mark: rate
            };
            insert[req.param('type')] = req.param('id');

            return Vote.create(insert);
        }).then(inserted => {
            res.ok({message: 'Success'});
        }).catch(err => {
            res.badRequest(err);
        });
    },

    /**
     * Like entity by user
     *
     * @param req
     * @param res
     * @param Model
     */
    like(req, res, Model) {
        Model.findOne({ id: req.param('id') }).populate('likes').then(found => {
            if(!found) throw 'No record found with the specified `id`.';

            for(var i = 0; i < found.likes.length; i++) {
                if(found.likes[i].user == req.headers.uid) return Like.destroy({ id: found.likes[i].id });
            }

            const insert = {
                user: req.headers.uid
            };
            insert[req.param('type')] = req.param('id');

            return Like.create(insert);
        }).then(() => {
            res.ok({message: 'Success'});
        }).catch(err => {
            res.badRequest(err);
        });
    },

    // remove image from entity
    destroyImg: function(req, res, Model) {
        Model.findOne({ id: req.param('id') }).then(entity => {
            if(!entity) throw 'No record found with the specified `id`.';

            return new Promise((resolve, reject) => {
                var request = require("request");

                request.delete({
                    url: `${sails.config.custom_config.CDN_URL}/${entity.id}/gallery/${req.param('img_name')}`,
                    timeout: 60 * 1000
                }, (err, response, body) => {
                    if(err) return reject(err.message);

                    var images = entity.images;
                    if(entity.images.indexOf(`images/${entity.id}/gallery/${req.param('img_name')}`) !== -1) {
                        entity.images.splice(entity.images.indexOf(`images/${entity.id}/gallery/${req.param('img_name')}`), 1);
                        resolve(Model.update({ id: entity.id }, { images: images }));
                    } else {
                        resolve([entity])
                    }

                });
            });
        }).then((updated) => {
            res.ok(updated[0]);
        }).catch(err => {
            res.badRequest(err);
        });
    },

    /**
     * Custom List of News
     *
     * @param req
     * @param res
     * @param Model
     * @returns {Promise.<TResult>}
     */
    customFind: function (req, res, Model) {
        return customFind.find(req, Model).then(result => {
            if(!result.length) return res.ok([]);
            // additional fields for sender_id subrole in comment and original_post of post
            if (req.query.populateNested) {
                req.query.populateNested = JSON.parse(req.query.populateNested);
                for (var f in sails.config.SUB_ROLE_REQUIRED_FIELDS) {
                    if (req.query.populateNested.comments.user.indexOf(sails.config.SUB_ROLE_REQUIRED_FIELDS[f]) === -1)
                        req.query.populateNested.comments.user.push(sails.config.SUB_ROLE_REQUIRED_FIELDS[f]);
                }
                req.query.populateNested = JSON.stringify(req.query.populateNested);
            }

            let populateNestedPromises = customFind.populateNestedFields(req, result, Model);

            Promise.all(populateNestedPromises).then(data => {
                result.map( res => {
                    res.comments ? res.comments.map( comments => {
                        if(!comments.user) return;
                        // generating subrole for sender_id in comment
                        comments.user.sub_role = require('../baseModels/User.js').getSubRole(comments.user);
                        for (var f in sails.config.SUB_ROLE_REQUIRED_FIELDS) {
                            if (typeof comments.user[sails.config.SUB_ROLE_REQUIRED_FIELDS[f]] !== 'undefined')
                                delete comments.user[sails.config.SUB_ROLE_REQUIRED_FIELDS[f]]; // remove secure fields
                        }
                    }) : null;
                });

                res.ok(result);
            }).catch(error => {
                res.serverError(error);
            });
        }).catch(error => {
            res.serverError(error);
        });
    },

    /**
     * Custom find one item of News
     *
     * @param req
     * @param res
     * @param Model
     * @returns {Promise.<TResult>}
     */
    customFindOne(req, res, Model) {
        let User = require('../baseModels/User.js');

        req.query.where = JSON.stringify(Object.assign({}, req.query.where ? JSON.parse(req.query.where) : {}, {id: req.param('id')}));

        return customFind.find(req, Model).then(result => {
            if(!result.length) return res.ok([]);
            // additional fields for sender_id subrole in comment and original_post of post
            if (req.query.populateNested) {
                req.query.populateNested = JSON.parse(req.query.populateNested);
                for (var f in sails.config.SUB_ROLE_REQUIRED_FIELDS) {
                    if (req.query.populateNested.comments.user.indexOf(sails.config.SUB_ROLE_REQUIRED_FIELDS[f]) === -1)
                        req.query.populateNested.comments.user.push(sails.config.SUB_ROLE_REQUIRED_FIELDS[f]);
                }
                req.query.populateNested = JSON.stringify(req.query.populateNested);
            }

            let populateNestedPromises = customFind.populateNestedFields(req, result, Model);

            Promise.all(populateNestedPromises).then(() => {
                result[0].comments ? result[0].comments.map( comment => {
                    if(!comment.user) return;
                    // generating subrole for sender_id in comment
                    comment.user.sub_role = User.getSubRole(comment.user);
                    for (var f in sails.config.SUB_ROLE_REQUIRED_FIELDS) {
                        if (typeof comment.user[sails.config.SUB_ROLE_REQUIRED_FIELDS[f]] !== 'undefined')
                            delete comment.user[sails.config.SUB_ROLE_REQUIRED_FIELDS[f]]; // remove secure fields
                    }
                }) : null;
                res.ok(result[0]);
            }).catch((error) => {
                res.serverError(error);
            });
        }).catch(function(error) {
            res.serverError(error);
        });
    }
};