/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

    /***************************************************************************
     *                                                                          *
     * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
     * etc. depending on your default view engine) your home page.              *
     *                                                                          *
     * (Alternatively, remove this and add an `index.html` file in your         *
     * `assets` directory)                                                      *
     *                                                                          *
     ***************************************************************************/

    /*'/': {
     view: 'homepage'
     }*/

    /***************************************************************************
     *                                                                          *
     * Custom routes here...                                                    *
     *                                                                          *
     * If a request to a URL doesn't match any of the custom routes above, it   *
     * is matched against Sails route blueprints. See `config/blueprints.js`    *
     * for configuration options and examples.                                  *
     *                                                                          *
     ***************************************************************************/
    '/*': function(req, res, next) {sails.log.verbose(req.method, req.url, req.headers.uid ? req.headers.uid : 'guest', req.body ? JSON.stringify(req.body) : ''); next();},
    'POST /login': 'AuthController.login',
    'POST /logout': 'AuthController.logout',
    'GET /event': 'EventController.customFind',
    'GET /model': 'ModelController.customFind',
    'GET /model/:id': {
        controller: 'ModelController',
        action: 'customFindOne',
        associations: null // remove associations for correct custom population in controller
    },
    'GET /casting': 'CastingController.customFind',
    'GET /casting_cities': 'CastingController.getCities',
    'GET /casting_countries': 'CastingController.getCountries',
    'GET /event_cities': 'EventController.getCities',
    'GET /event_countries': 'EventController.getCountries',
    'GET /news': 'NewsController.customFind',
    'GET /news_cities': 'NewsController.getCities',
    'GET /news_countries': 'NewsController.getCountries',
    'POST /ipn': 'AccountController.ipn',
    'POST /qw_ipn': 'AccountController.qw_ipn',
    'POST /qw_activate': 'AccountController.qw_activate',
    'GET /get_costs': 'AccountController.get_costs',
    'POST /image': 'ImageController.create',
    'DELETE /image/:name': 'ImageController.destroy',
    'DELETE /album/clear/:id': 'ImageController.clearAll',
    'PUT /image/:name': 'ImageController.update',
    'GET /policy': 'PoliciesController.getPolicy',
    'GET /rules': 'PoliciesController.getRulesList',
    'GET /roles': 'PoliciesController.getRolesList',
    'GET /rights': 'PoliciesController.getRights',
    'PUT /policy': 'PoliciesController.updatePolicy',
    'PUT /policy/set_default/:id': 'PoliciesController.setDefault',
    'PUT /policy/unset_default': 'PoliciesController.unsetDefault',
    'GET /eventmanager': 'EventManagerController.customFind',
    'GET /city': 'CityController.customFind',
    'GET /member': 'MemberController.customFind',
    'GET /member/:id': 'MemberController.customFindOne',
    'GET /attributes': 'PoliciesController.getAttributes',
    'GET /available': 'ServiceController.checkAvailable',
    'GET /eventinvitation': 'EventInvitationController.customFind',
    'GET /castinginvitation': 'CastingInvitationController.customFind',
    'GET /any_user': 'ModelController.anyUserCustomFind',
    'GET /post': 'PostController.customFind',
    'GET /timeline/:owner_id': 'PostController.timeline',
    'PUT /like/:id': 'PostController.like',
    'PUT /as_friend/:friend_id': 'ModelController.add_removeFriend',
    'PUT /remove_invitation/:friend_id': 'ModelController.removeInvitation',
    'GET /chat_users': 'ChatController.getChatUsers',
    'GET /unread_msgs_count': 'ChatController.getUnreadMsgs',
    'PUT /set_as_read/:id': 'ChatController.setAsRead',
    'GET /chat': 'ChatController.customFind',
    'PUT /prepay/:id': 'ModelController.prepay',
    'PUT /reset_pwd/:email': 'AccountController.reset_pwd',
    'GET /profile/:id': 'ModelController.getProfileURL',
    'GET /statistic/summary': 'StatisticController.summary',
    'GET /statistic/by_managers': 'StatisticController.by_managers',
    'PUT /news/:id/rating': 'NewsController.vote',
    'PUT /casting/:id/rating': 'CastingController.vote',
    'PUT /event/:id/rating': 'EventController.vote',
    'PUT /news/:id/like': 'NewsController.like',
    'PUT /casting/:id/like': 'CastingController.like',
    'PUT /event/:id/like': 'EventController.like',
    'PUT /comment/:id/like': 'CommentController.like',
    'DELETE /news/:id/:img_name': 'NewsController.destroyImg',
    'DELETE /casting/:id/:img_name': 'CastingController.destroyImg',
    'DELETE /event/:id/:img_name': 'EventController.destroyImg',
    'GET /news/:id': 'NewsController.customFindOne',
    'GET /casting/:id': 'CastingController.customFindOne',
    'GET /event/:id': 'EventController.customFindOne',
    'PUT /member/:id': 'ModelController.update',
    'POST /worksheet': 'WorksheetController.createOrUpdate',
    'GET /worksheet/:uid': 'WorksheetController.customFindOne',
    'DELETE /video/:name': 'VideoController.destroy',
    'PUT /video/:name': 'VideoController.update',
    'DELETE /album/clear_videos/:id': 'VideoController.clearAll'
};
