/**
 * Production environment settings
 *
 * This file can include shared settings for a production environment,
 * such as API keys or remote database passwords.  If you're using
 * a version control solution for your Sails app, this file will
 * be committed to your repository unless you add it to your .gitignore
 * file.  If your repository will be publicly viewable, don't add
 * any private information to this file!
 *
 */
var nodemailer = require('nodemailer'),
    smtpTransport = require('nodemailer-smtp-transport');

module.exports = {

    port: 1337,

    /***************************************************************************
     * Set the default database connection for models in the development       *
     * environment (see config/connections.js and config/models.js )           *
     ***************************************************************************/

    // models: {
    //   connection: 'someMongodbServer'
    // }

    connections: {
        mongo: {
            adapter: 'sails-mongo',
            url: 'mongodb://mongo-3.smoothstyle.8295.mongodbdns.com:27017,mongo-4.smoothstyle.8295.mongodbdns.com:27017,mongo-5.smoothstyle.8295.mongodbdns.com:27017/smooth_revert?replicaSet=SMOOTH'
        }
    },
    
    cors: {
        origin: 'http://localhost:8090,http://smooth.style,http://admin.smooth.style' // development localhost web part
    },

    custom_config: {
        CDN_URL: 'http://cdn.smooth.style',
        tmp_file_storage: '.tmp/uploads/',
        receiver_email: "payments@smooth.style",
        permit_code: 'Q1w2e3r4', // code for secure operations
        business_pro_membership_cost: 0.01,
        site_url: 'http://smooth.style',
        PAYPAL: {
            API_URL: 'https://www.paypal.com/cgi-bin/websc',
            NVP_URL: 'https://api-3t.paypal.com/nvp',
            NVP_USER: 'payments_api1.smooth.style',
            NVP_PWD: 'VK3VCUXQKF65Z3Y3',
            NVP_SIGNATURE: 'AFcWxV21C7fd0v3bYYYRCpSSRl31AB29zE0xHbH-jgZ4STenPeb077pr',
            member_pro_membership_cost: 1.50
        },
        QIWI: {
            API_URL: 'https://api.qiwi.com/api/v2/prv/',
            API_ID: 53701453,
            SHOP_ID: 493610,
            NOTIFICATION_PWD: 'V6zFqy2QSwEF8cEZFsYX',
            API_PWD: 'w8aGydYcrfu8Yvx50eFt',
            CURRENCY: 'RUB',
            member_pro_membership_cost: 90.00
        },
        pwd_length: 6,
        POST_LIMIT_MB: 1000,
        PORTFOLIO_ALBUM_NAME: 'Portfolio'
    },

    email: {
        transporter: nodemailer.createTransport(smtpTransport({
            host: 'mail.inweb24.us',
            port: 25,
            auth: {
                user: 'noreply@smooth.style',
                pass: 'exveS=iH.TIK'
            }
        })),
        testMode: false
    },

    AVAILABLE_PAYMENT_STATUSES: [
        'unpaid',
        'paid'
    ],

    AVAILABLE_USER_TYPES: [
        "guest", // prospect user
        "member", // not paid member
        "member_pro", // paid member
        "model", // paid member which was marked as model in admin
        "model_any", // not paid or paid model
        "event_manager",
        "event_manager_pro", // not marked as agent
        "event_manager_agent", // marked as agent
        "business_lite", // not paid business user
        "business", // paid business user
        "business_any", // not paid or paid business user
        "content_manager", // content manager
        "user_manager", // user manager
        "admin" // super admin
    ],
    USER_TYPES_HIERARCHY: {
        'guest' : [],
        'model_any': ['member', 'member_pro', 'model'],
        'event_manager': ['event_manager_pro', 'event_manager_agent'],
        'business_any': ['business_lite', 'business'],
        'content_manager': [],
        'user_manager': [],
        'admin': []
    },
    AVAILABLE_USER_ROLES: ["admin", "event_manager", "model", "content_manager", "user_manager"],
    AVAILABLE_USER_STATUSES: [
        "inactive",
        "active"
    ],
    // secure fields for user entity, that shouldn't be responded to frontend
    SECURE_FIELDS: ['password'],
    // fields of user, that take part in calculation of subrole
    SUB_ROLE_REQUIRED_FIELDS: ['role', 'is_model', 'payment_status', 'expiration'],
    // subscribe on posts in timeline from listed users
    AUTO_SUBCRIBE_ON_USERS: [],
    mongodb_module_path: 'sails-mongo/node_modules/mongodb'

};
