/**
 * Localhost environment settings
 *
 * This file can include shared settings for a development team,
 * such as API keys or remote database passwords.  If you're using
 * a version control solution for your Sails app, this file will
 * be committed to your repository unless you add it to your .gitignore
 * file.  If your repository will be publicly viewable, don't add
 * any private information to this file!
 *
 */

var _ = require('lodash'),
    config = require('./production');

module.exports = _.merge({}, config, {

    port: 1338,

    /***************************************************************************
     * Set the default database connection for models in the development       *
     * environment (see config/connections.js and config/models.js )           *
     ***************************************************************************/

    // models: {
    //   connection: 'someMongodbServer'
    // }

    connections: {
        mongo: {
            adapter: 'sails-mongo',
            host: 'localhost',
            port: 27017,
            user: '',
            password: '',
            database: 'smooth',
            url: null
        }
    },

    log: {
        level: "info"
    },
    // email server config
    email: {
        transporter: null,
        service: 'Gmail',
        auth: {
            user: 'yura.test.2014@gmail.com',
            pass: 'Yura2014'
        },
        testMode: false
    },
    cors: {
        origin: 'http://smooth.style,http://smooth_web,http://localhost:8090,http://localhost:8100,http://localhost:7171' // development localhost web part
    },

    custom_config: {
        CDN_URL: 'http://localhost:7070',
        tmp_file_storage: '.tmp/uploads/',
        site_url: 'http://localhost:8090',
        PAYPAL: {
            API_URL: 'https://www.sandbox.paypal.com/cgi-bin/websc',
            NVP_URL: 'https://api-3t.sandbox.paypal.com/nvp',
            NVP_USER: 'payments-facilitator_api1.smooth.style',
            NVP_PWD: 'U2MJ6SSUPUD4DPSB',
            NVP_SIGNATURE: 'AToGeGD6i7PLEDQGpyI7BJ6YauxaApa.VA0S2SiyNoF15MLwjCv62lJx',
            member_pro_membership_cost: 0.01
        },
        QIWI: {
            CURRENCY: 'RUB',
            member_pro_membership_cost: 1.00
        },
        receiver_email: "payments-facilitator@smooth.style"
    }

});
