/**
 * Development environment settings
 *
 * This file can include shared settings for a development team,
 * such as API keys or remote database passwords.  If you're using
 * a version control solution for your Sails app, this file will
 * be committed to your repository unless you add it to your .gitignore
 * file.  If your repository will be publicly viewable, don't add
 * any private information to this file!
 *
 */
var nodemailer = require('nodemailer'),
    smtpTransport = require('nodemailer-smtp-transport');

var _ = require('lodash'),
    config = require('./production');

module.exports = _.merge({}, config, {
    /***************************************************************************
     * Set the default database connection for models in the development       *
     * environment (see config/connections.js and config/models.js )           *
     ***************************************************************************/

    // models: {
    //   connection: 'someMongodbServer'
    // }

    connections: {
        mongo: {
            adapter: 'sails-mongo',
            host: '54.171.18.48',
            port: 27017,
            user: '',
            password: '',
            database: 'smooth',
            url: null
        }
    },

    log: {
        level: "info"
    },

    cors: {
        origin: 'http://dev.smooth.style,http://smooth_web,http://localhost:8090,http://localhost:7171,http://localhost:3000,http://admin.smooth.style,http://adm-dev.smooth.style' // development localhost web part
    },

    custom_config: {
        CDN_URL: 'http://cdn-dev.smooth.style',
        tmp_file_storage: '.tmp/uploads/',
        PAYPAL: {
            API_URL: 'https://www.sandbox.paypal.com/cgi-bin/websc',
            NVP_URL: 'https://api-3t.sandbox.paypal.com/nvp',
            NVP_USER: 'payments-facilitator_api1.smooth.style',
            NVP_PWD: 'U2MJ6SSUPUD4DPSB',
            NVP_SIGNATURE: 'AToGeGD6i7PLEDQGpyI7BJ6YauxaApa.VA0S2SiyNoF15MLwjCv62lJx',
            member_pro_membership_cost: 0.01
        },
        QIWI: {
            CURRENCY: 'RUB',
            member_pro_membership_cost: 1.00
        },
        receiver_email: "payments-facilitator@smooth.style",
        site_url: 'http://dev.smooth.style'
    },

    email: {
        transporter: nodemailer.createTransport(smtpTransport({
            host: 'mail.inweb24.us',
            port: 25,
            auth: {
                user: 'noreply2@smooth.style',
                pass: 'SPbPPD2nqx'
            }
        })),
        testMode: false
    }

});