var MongoClient = require('sails-mongo/node_modules/mongodb').MongoClient;
var config = require('./config/env/'+process.env.NODE_ENV+'.js');
var fs = require('fs');

// Build A Mongo Connection String
var connectionString = 'mongodb://';

// If auth is used, append it to the connection string
if(config.connections.mongo.user && config.connections.mongo.password) {

    // Ensure a database was set if auth in enabled
    if(!config.connections.mongo.database) {
        throw new Error('The MongoDB Adapter requires a database config.connections.mongo option if authentication is used.');
    }

    connectionString += config.connections.mongo.user + ':' + config.connections.mongo.password + '@';
}

// Append the host and port
connectionString += config.connections.mongo.host + ':' + config.connections.mongo.port + '/';

if(config.connections.mongo.database) {
    connectionString += config.connections.mongo.database;
}

// Use config.connections.mongo connection string if available
if(config.connections.mongo.url) connectionString = config.connections.mongo.url;


MongoClient.connect(connectionString, function(err, db) {
    db.collection('policies').find({"defaultSchema": true}).toArray(function(err, docs) {
        db.close();
        if(docs.length > 0) {
            delete docs[0].createdAt;
            delete docs[0].updatedAt;
            delete docs[0].defaultSchema;
            delete docs[0]._id;

            fs.writeFile("policies.json", JSON.stringify(docs[0]), function(err) {
                if(err) {
                    return console.log(err);
                }

                console.log("The file was saved.");
            });
        } else {
            console.log("No default policy!");
        }
    });
});